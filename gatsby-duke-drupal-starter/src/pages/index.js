/**
 *  Renders Page content type template
 */
import { graphql } from "gatsby"
import React from "react"
import LayoutHomepage from "../components/Organisms/Layout/LayoutHomepage"
import { getParagraph } from "../components/Molecules/Paragraphs/ParagraphsHelper.js"
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    console.log('all is bad')
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return this.props.children;
  }
}

export const HomepageTemplate = ({ data }) => {

  const homepage = data.site.nodes ? data.site.nodes[0].relationships.field_site_homepage : ''

  const hero = (homepage.relationships.hero) ? getParagraph(homepage.relationships.hero) : null;
  const paragraphs = homepage.relationships.paragraphs?.map(getParagraph);

  return (
    <ErrorBoundary>
      <LayoutHomepage
        hasSidebar={false}
        title={homepage.title}
        hero={hero ? hero : null}
      >
        {paragraphs}
      </LayoutHomepage>
    </ErrorBoundary>
  )
}

export default HomepageTemplate

export const homepageQuery = graphql`
{
  site: allBlockContentSiteInformation {
    nodes {
      relationships {
        field_site_homepage {
          id
          title
          relationships {
            hero: field_hero {
              type: __typename
              ...ParagraphCarousel
              ...ParagraphHeroImage
              ...ParagraphHeroImageText
              ...ParagraphHeroVideoText
            }
            paragraphs: field_content_blocks {
              type: __typename
              ...ParagraphAccordionSection
              ...ParagraphButton
              ...ParagraphCallout
              ...ParagraphContent
              ...ParagraphContentReference
              ...ParagraphEventList
              ...ParagraphFaqsList
              ...ParagraphIframe
              ...ParagraphIconRow
              ...ParagraphImageCardRow
              ...ParagraphImageText
              ...ParagraphMedia
              ...ParagraphNewsList
              ...ParagraphPullQuote
              ...ParagraphSlideshow
              ...ParagraphSocialMedia
              ...ParagraphSpecialText
              ...ParagraphTwoColumn
              ...ParagraphThreeColumn
            }
          }
        }
      }
    }
  }
}`
