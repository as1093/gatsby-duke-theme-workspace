import React from 'react';
import { graphql } from 'gatsby';
import LayoutBase from "../components/Organisms/Layout/LayoutBase"
import CustomBreadcrumbs from "../components/Molecules/Breadcrumbs/Breadcrumbs"
import DocumentTeaser from "../components/Templates/Document/DocumentTeaser"
import Heading from "../components/Atoms/Heading/Heading"
import PaginatedContent from "../components/Atoms/PaginatedContent/PaginatedContent"
import { Container } from "react-bootstrap"

export const DocumentPage = ({ data }) => {
	const pageTitle = 'Documents'

	const allDocumentArray = []
  data.allNodeDocument.nodes.forEach(document => (
    allDocumentArray.push({
      id: document.id,
      title: document.title,
      description: document.body,
      date: document.field_last_modified_date,
      link: document.path ? document.path.alias : "",
      category: document.relationships.field_categories,
      file:document.relationships.field_document
    })
  ))

  const allDocumentSorted = allDocumentArray.sort(
    function(a,b) {
      var date1 = new Date(a.date)
      var date2 = new Date(b.date)
      return date2 - date1
    }
	)
	
	let ContentToPaginate = []
	allDocumentSorted.forEach(node => (
    ContentToPaginate.push(
      <DocumentTeaser
        title={node.title}
        description={node.body && node.body.processed}
        categories={
          node.category
            ? node.category
            : []
        }
        file={
          node.file
            ? node.file
            : null
        }
        date={
          node.date
            ? node.date
            : ""
        }
      />
    )
	))

	return (
		<LayoutBase pageTitle={pageTitle}>
			<Container>
				<Heading level={1}>{pageTitle}</Heading>
				<CustomBreadcrumbs pageTitle={pageTitle} />
				{ContentToPaginate && 
					<PaginatedContent items={ContentToPaginate} />
				}
			</Container>
		</LayoutBase>
	)
}

export default DocumentPage

export const query = graphql`
{
	allNodeDocument(filter: { status: { eq: true}}) {
		nodes {
			title
      id
      path {
        alias
      }
      body {
        processed
      }
      field_last_modified_date(formatString: "M/D/YY")
      relationships {
        field_categories {
          name
        }
        field_document {
          ...ParagraphDocumentUpload
          ...ParagraphURL
        }
      }
		}
	}
}
`;