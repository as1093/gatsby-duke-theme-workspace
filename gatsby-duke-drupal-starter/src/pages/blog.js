/**
 * Blog landing page, displays Blog Post teasers with pagination
 */
import React from "react"
import { graphql } from "gatsby"
import LayoutBase from "../components/Organisms/Layout/LayoutBase"
import CustomBreadcrumbs from "../components/Molecules/Breadcrumbs/Breadcrumbs"
import BlogPostTeaser from "../components/Templates/BlogPost/BlogPostTeaser"
import Heading from "../components/Atoms/Heading/Heading"
import { Container } from "react-bootstrap"

export const BlogPage = ({ data }) => {
  const pageTitle = "Blog"

  return (
    <LayoutBase pageTitle={pageTitle}>
      <Container>
        <Heading level={1}>{pageTitle}</Heading>

        <CustomBreadcrumbs pageTitle={pageTitle} />

        {data.allNodeBlogPost.nodes?.map(post => (
          <BlogPostTeaser node={post} key={post.id} />
        ))}
      </Container>
    </LayoutBase>
  )
}

export default BlogPage

export const query = graphql`
  {
    allNodeBlogPost(
      filter: { status: { eq: true } }
      sort: { fields: field_date, order: DESC }
    ) {
      nodes {
        id
        title
        path {
          alias
        }
        body {
          summary
          processed
        }
        field_date(formatString: "MMMM D, YYYY")
        relationships {
          field_author_reference {
            title
            field_last_name
            path {
              alias
            }
            field_bio {
              summary
            }
          }
          field_featured_media {
            field_media_image {
              alt
            }
            relationships {
              field_media_image {
                localFile {
                  publicURL
                  childImageSharp {
                    fluid(maxWidth: 300, maxHeight: 200) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
