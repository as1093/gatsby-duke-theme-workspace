import React from 'react';
import { graphql } from 'gatsby';
import LayoutBase from "../components/Organisms/Layout/LayoutBase"
import CustomBreadcrumbs from "../components/Molecules/Breadcrumbs/Breadcrumbs"
import EventTeaser from "../components/Templates/Event/EventTeaser"
import Heading from "../components/Atoms/Heading/Heading"
import PaginatedContent from "../components/Atoms/PaginatedContent/PaginatedContent"
import { Container } from "react-bootstrap"

export const EventPage = ({ data }) => {
	const pageTitle = 'Events'

	const allEventArray = []
  data.allNodeEvent.nodes.forEach(event => (
    allEventArray.push({
      id: event.id,
      title: event.title,
      date: event.field_event_date?.value,
      link: event.path ? event.path.alias : "",
      location_url: event.field_location_link?.uri,
      location_text: event.field_location_text && event.field_location_text,
      status: event.relationships?.field_status?.name
    })
  ))

  const allEventSorted = allEventArray.sort(
    function(a,b) {
      var date1 = new Date(a.date)
      var date2 = new Date(b.date)
      return date2 - date1
    }
	)
	
	let ContentToPaginate = []
	allEventSorted.forEach(node => (
    ContentToPaginate.push(
      <EventTeaser 
				key={node.id} 
				date={node.date}
				link={node.link}
				title={node.title}
				status={node.status}
				location_url={node.location_url}
				location_text={node.location_text}
			/>
    )
	))

	return (
		<LayoutBase pageTitle={pageTitle}>
			<Container>
				<Heading level={1}>{pageTitle}</Heading>
				<CustomBreadcrumbs pageTitle={pageTitle} />
				{ContentToPaginate && 
					<PaginatedContent items={ContentToPaginate} />
				}
			</Container>
		</LayoutBase>
	)
}

export default EventPage

export const query = graphql`
{
	allNodeEvent(filter: { status: { eq: true}}) {
		nodes {
			id
			title
			field_event_date {
				value
				end_value
			}
			field_location_link {
				uri
			}
			field_location_text
			relationships {
				field_status {
					name
				}
			}
			path {
				alias
			}
		}
	}
}
`;