/**
 * Faculty landing page, displays scholar profile teasers with pagination
 */
import React from "react"
import { graphql } from "gatsby"
import LayoutBase from "../components/Organisms/Layout/LayoutBase"
import CustomBreadcrumbs from "../components/Molecules/Breadcrumbs/Breadcrumbs"
import ScholarsProfileTeaser from "../components/Templates/ScholarsProfile/ScholarsProfileTeaser"
import Heading from "../components/Atoms/Heading/Heading"
import { Container, Row } from "react-bootstrap"
import PaginatedContent from "../components/Atoms/PaginatedContent/PaginatedContent"

export const FacultyPage = ({ data }) => {
  const pageTitle = 'Faculty'

  let ContentToPaginate = []
	data.allDukeScholarsProfileDukeScholarsProfile.nodes.forEach(profile => (
    ContentToPaginate.push(
      <ScholarsProfileTeaser node={profile} key={profile.id}/>
    )
	))

  return (
  <LayoutBase pageTitle={pageTitle}>
    <Container>
      <Heading level={1}>{pageTitle}</Heading>

      <CustomBreadcrumbs pageTitle={pageTitle} />

      <Row>
        {ContentToPaginate && 
					<PaginatedContent items={ContentToPaginate} itemsPerPage={24} />
				}
      </Row>

    </Container>
  </LayoutBase>
  )
}

export default FacultyPage

export const query = graphql`
  {
    allDukeScholarsProfileDukeScholarsProfile(sort: {fields: duke_scholars_profile_name___last_name}, filter: {relationships: {duke_scholars_profile_positions: {elemMatch: {title: {ne: "Student"}}}}}) {
      nodes {
        id
        drupal_internal__id
        title
        relationships {
          duke_scholars_profile_positions {
            title
            position_preferred
          }
        }
        duke_scholars_profile_thumbnail {
          alt_text
          thumbnail
        }
      }
    }
  }
`
