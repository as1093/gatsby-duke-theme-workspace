/**
 * News landing page, displays News teasers with pagination
 */
import React from "react"
import { graphql } from "gatsby"
import LayoutBase from "../components/Organisms/Layout/LayoutBase"
import CustomBreadcrumbs from "../components/Molecules/Breadcrumbs/Breadcrumbs"
import NewsTeaser from "../components/Templates/News/NewsTeaser"
import Heading from "../components/Atoms/Heading/Heading"
import PaginatedContent from "../components/Atoms/PaginatedContent/PaginatedContent"
import { Container } from "react-bootstrap"

export const NewsPage = ({ data }) => {
  const pageTitle = 'News'

  const allNewsArray = []
  data.allNodeNews.nodes.forEach(news => (
    allNewsArray.push({
      id: news.id,
      title: news.title,
      date: news.field_date,
      link: news.path?.alias,
      alternate_link: news.field_alternate_link,
      body: news.body,
      source: news.field_news_source,
      featured_image: news.relationships?.field_featured_media,
      feed_image_url: news.field_feed_image_url,
      feed_image_alt: news.field_feed_image_alt_text
    })
  ))

  const allNewsSorted = allNewsArray.sort(
    function(a,b) {
      var date1 = new Date(a.date)
      var date2 = new Date(b.date)
      return date2 - date1
    }
  )

  let ContentToPaginate = []
	allNewsSorted.forEach(node => (
    ContentToPaginate.push(
      <NewsTeaser
        key={node.id} 
        date={node.date}
        link={node.link}
        alternate_link={node.alternate_link}
        title={node.title}
        body={node.body}
        source={node.source}
        featured_image={node.featured_image}
        feed_image_url={node.feed_image_url}
        feed_image_alt={node.feed_image_alt}
      />
    )
	))

  return (
    <LayoutBase pageTitle={pageTitle}>
      <Container>
        <Heading level={1}>{pageTitle}</Heading>
        <CustomBreadcrumbs pageTitle={pageTitle} />
        {ContentToPaginate && 
					<PaginatedContent items={ContentToPaginate} />
				}
      </Container>
    </LayoutBase>
  )
}

export default NewsPage

export const query = graphql`
  {
    allNodeNews(filter: { status: { eq: true } }) {
      nodes {
        id
        title
        path {
          alias
        }
        field_alternate_link {
          uri
        }
        body {
          summary
          processed
        }
        field_news_source
        field_feed_image_url {
          uri
        }
        field_feed_image_alt_text
        field_date(formatString: "MMMM D, YYYY")
        relationships { 
          field_featured_media {
            field_media_image {
              alt
            }
            relationships {
              field_media_image {
                localFile {
                  publicURL
                  childImageSharp {
                    fluid(maxWidth: 300, maxHeight:200) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
