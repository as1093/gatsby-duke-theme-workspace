/**
 * Stories landing page, displays Story teasers with pagination
 */
import React from "react"
import { graphql } from "gatsby"
import LayoutBase from "../components/Organisms/Layout/LayoutBase"
import CustomBreadcrumbs from "../components/Molecules/Breadcrumbs/Breadcrumbs"
import StoryTeaser from "../components/Templates/Story/StoryTeaser"
import Heading from "../components/Atoms/Heading/Heading"
import { Container } from "react-bootstrap"

export const StoriesPage = ({ data }) => {
  const pageTitle = 'Stories'

  return (
  <LayoutBase pageTitle={pageTitle}>
    <Container>
      <Heading level={1}>{pageTitle}</Heading>

      <CustomBreadcrumbs pageTitle={pageTitle} />

      {data.allNodeStory.nodes?.map(story => (
        <StoryTeaser node={story} key={story.id}/>
      ))}

    </Container>
  </LayoutBase>
  )
}

export default StoriesPage

export const query = graphql`
  {
    allNodeStory(filter: { status: { eq: true } }, sort: {fields: field_date, order: DESC} ) {
      nodes {
        id
        title
        path {
          alias
        }
        field_summary
        field_date(formatString: "MMMM D, YYYY")
        relationships {
          field_author_reference {
            title
            field_last_name
            path {
              alias
            }
            field_bio {
              summary
            }
          }
          field_featured_media {
            ... on media__image {
              id
              name
              field_media_image {
                alt
              }
              relationships {
                field_media_image {
                  localFile {
                    publicURL
                    childImageSharp {
                      fluid(maxWidth:300, maxHeight:300) {
                        ...GatsbyImageSharpFluid
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
