/**
 * Staff landing page, displays profile teasers with pagination
 */
import React from "react"
import { graphql } from "gatsby"
import LayoutBase from "../components/Organisms/Layout/LayoutBase"
import CustomBreadcrumbs from "../components/Molecules/Breadcrumbs/Breadcrumbs"
import ProfileTeaser from "../components/Templates/Profile/ProfileTeaser"
import Heading from "../components/Atoms/Heading/Heading"
import { Container, Row, Col } from "react-bootstrap"

export const StaffPage = ({ data }) => {
  const pageTitle = 'Staff'

  return (
  <LayoutBase pageTitle={pageTitle}>
    <Container>
      <Heading level={1}>{pageTitle}</Heading>

      <CustomBreadcrumbs pageTitle={pageTitle} />

      <Row>
        {data.allNodeProfile.nodes?.map(profile => (
          <Col md={3}>
            <ProfileTeaser node={profile} key={profile.id}/>
          </Col>
        ))}
      </Row>

    </Container>
  </LayoutBase>
  )
}

export default StaffPage

export const query = graphql`
  {
    allNodeProfile(filter: { status: { eq: true }, field_external_profile: {ne: true} }, sort: {fields: field_last_name, order: ASC} ) {
      nodes {
        id
        title
        path {
          alias
        }
        field_role
        relationships {
          field_image {
            field_media_image {
              alt
            }
            relationships {
              field_media_image {
                localFile {
                  publicURL
                  childImageSharp {
                    fluid(maxWidth: 300, maxHeight:200) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
