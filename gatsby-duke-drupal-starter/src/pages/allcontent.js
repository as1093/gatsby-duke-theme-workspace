import React from 'react';
import { graphql, Link } from 'gatsby';
import LayoutBase from '../components/Organisms/Layout/LayoutBase';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'

export const IndexPage = ({ data }) => {
  let nodes = data.allSitePage.nodes
  let EventsArr =[]
  let NewsArr = []
  let ScholarsArr = []
  let AnnouncementsArr = []
  let FaqsArr = []

  for (let node of data.allSitePage.nodes) {
    if (node.path.includes("event")) EventsArr.push(node)
    if (node.path.includes("news")) NewsArr.push(node)
    if (node.path.includes("duke-scholars-profile")) ScholarsArr.push(node)
    if (node.path.includes("announcement")) AnnouncementsArr.push(node)
    if (node.path.includes("faq")) FaqsArr.push(node)
  }

  let reducedNodes = nodes.filter(x => (!EventsArr.includes(x) && !NewsArr.includes(x) && !ScholarsArr.includes(x) && !AnnouncementsArr.includes(x) && !FaqsArr.includes(x)));

  return (
    <LayoutBase>
      <Container>
        <h1>Content</h1>
        <Accordion>
          <Card key={1}>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey={1}>
                Events
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey={1}>
              <Card.Body>
                <ul>
                  {EventsArr.map(node => (
                    <li key={node.path}>
                      <Link to={node.path}>{node.path}</Link>
                    </li>
                  ))}
                </ul>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
        <Accordion>
          <Card key={2}>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey={2}>
                News
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey={2}>
              <Card.Body>
                <ul>
                  {NewsArr.map(node => (
                    <li key={node.path}>
                      <Link to={node.path}>{node.path}</Link>
                    </li>
                  ))}
                </ul>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>  
        <Accordion>
          <Card key={3}>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey={3}>
                Scholars Profiles
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey={3}>
              <Card.Body>
                <ul>
                  {ScholarsArr.map(node => (
                    <li key={node.path}>
                      <Link to={node.path}>{node.path}</Link>
                    </li>
                  ))}
                </ul>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>     
        <Accordion>
          <Card key={4}>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey={4}>
                Anouncements
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey={4}>
              <Card.Body>
                <ul>
                  {AnnouncementsArr.map(node => (
                    <li key={node.path}>
                      <Link to={node.path}>{node.path}</Link>
                    </li>
                  ))}
                </ul>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>   
        <Accordion>
          <Card key={5}>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey={5}>
                FAQs
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey={5}>
              <Card.Body>
                <ul>
                  {FaqsArr.map(node => (
                    <li key={node.path}>
                      <Link to={node.path}>{node.path}</Link>
                    </li>
                  ))}
                </ul>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>          
        <br/>                   
        <ul>
          {reducedNodes.map(node => (
            <li key={node.path}>
              <Link to={node.path}>{node.path}</Link>
            </li>
          ))}
        </ul>
      </Container>
    </LayoutBase>
  )
}

export default IndexPage

export const query = graphql`
  {
    allSitePage {
      nodes {
        path
      }
    }
  }`;