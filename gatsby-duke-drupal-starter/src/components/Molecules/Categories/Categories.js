/**
 *  Renders the categories information block for Story (full format):
 *  - Written by
 *  - Author(s) name, photo, short bio, link to profile
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import "./Categories.scss"

const Categories = ({ 
  categories, 
  block = "categories", 
  modifiers
}) => {

  const formattedCategories = categories.map((category, index) => (
    <span key={index} className={bem(block, "name", modifiers)}>
      <a href={category.path.alias}>{category.name}</a>
      {index < categories.length - 1 ? 
        (((index === categories.length - 2) && (categories.length !== 2)) ? ", and " : ", ") 
      : 
        " "
      }
    </span>
  ))

  return categories.length ? (
    <div className={bem(block, "wrapper", modifiers)}>
      <Heading level={3} modifiers={["label"]}>
        Categories
      </Heading>
      {formattedCategories}
    </div>
  ) : (
    ""
  )
}

Categories.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.object),
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Categories
