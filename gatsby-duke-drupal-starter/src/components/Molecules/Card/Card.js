import React from "react"
import PropTypes from "prop-types"
import Image from "../../Atoms/Image/Image"
import { imageTypeProps } from "../../Atoms/Image/Image"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import Button from "../../Atoms/Button/Button"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import "./Card.scss"

const Card = ({
  image,
  imageCrop,
  heading,
  children,
  linkText,
  linkUrl,
  block = "imagecard",
  element,
  modifiers,
}) => {
  const croppedImageStyle = (imageCrop ? imageCrop : 'CENTER')
  // const croppedImage = (image.relationships.field_media_image.localFile.childImageSharp ? image.relationships.field_media_image.localFile.childImageSharp[`${croppedImageStyle}`] : image.relationships.field_media_image.localFile)

  return (
    <div className={bem(block, element, modifiers)}>

      {image && (
        <div className={bem(block, "image")}>
          <Image 
            image={image.relationships.field_media_image.localFile} 
            crop={croppedImageStyle} 
            imageType={'fluid'}
            alt={image.field_media_image.alt} />
        </div>
      )}

      <div className={bem(block, "content")}>
        {/* Heading */}
        {heading && (
          <Heading level={3} block={block} element="heading">
            {heading}
          </Heading>
        )}
        {/* Body */}
        {children && <CKEditorContent content={children} />}
        {/* Button */}
        {linkUrl && (
          <Button link={linkUrl} showIcon={true} modifiers={['plain','white']}>{linkText}</Button>
        )}
      </div>
    </div>
  )
}

Card.propTypes = {
  image: imageTypeProps,
  imageCrop: PropTypes.string,
  heading: PropTypes.string,
  children: PropTypes.string,
  linkText: PropTypes.string,
  linkUrl: PropTypes.string,
}

export default Card
