import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import DateProcessor from "../../Atoms/DateProcessor/DateProcessor"
import TimeProcessor from "../../Atoms/TimeProcessor/TimeProcessor"
import DrupalLink from "../../Atoms/DrupalLink/DrupalLink"
import Eyebrow from "../../Atoms/Eyebrow/Eyebrow"
import "./EventCard.scss"

const EventCard = ({
  date,
  title,
  status,
  link,
  location_uri,
  location_text,
  block = "event-card",
  element,
  modifiers,
}) => {
  return (
    <div className={bem(block, element, modifiers)}>
      <DateProcessor startDate={date} cardView />
      <Eyebrow text={status ? (status !== 'CONFIRMED' ? status : "") : ""} />
      <Heading level={4} className={bem(block, "title")}><a href={link}>{title}</a></Heading>
      <TimeProcessor startTime={date} />
      {location_uri ? (
        <>
          <DrupalLink url={location_uri} children={location_text ? location_text : location_uri} />
        </>
      ) : (
        <>
          {location_text && <p>{location_text}</p>}
        </>
      )}
    </div>
  )
}

EventCard.propTypes = {
  date: PropTypes.string,
  title: PropTypes.string,
  status: PropTypes.string,
  link: PropTypes.string,
  location_uri: PropTypes.string,
  location_text: PropTypes.string,
}

export default EventCard
