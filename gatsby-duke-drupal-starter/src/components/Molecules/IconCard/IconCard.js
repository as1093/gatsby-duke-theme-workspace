import React from "react"
import PropTypes from "prop-types"
import Image from "../../Atoms/Image/Image"
import { imageTypeProps } from "../../Atoms/Image/Image"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import Button from "../../Atoms/Button/Button"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import "./IconCard.scss"

const IconCard = ({
  image,
  heading,
  children,
  linkText,
  linkUrl,
  block = "iconcard",
  element,
  modifiers,
}) => {
  return (
    <div className={bem(block, element, modifiers)}>

      {image && (
        <div className={bem(block, "image")}>
          <Image 
            image={image.relationships.field_media_image.localFile} 
            imageType={'fluid'}
            alt={image.field_media_image.alt} />
        </div>
      )}

      <div className={bem(block, "content")}>
        {/* Heading */}
        {heading && (
          <Heading level={3} block={block} element="heading">
            {heading}
          </Heading>
        )}
        {/* Body */}
        {children && <CKEditorContent content={children} />}
        {/* Button */}
        {linkUrl && (
          <Button link={linkUrl} showIcon={true} modifiers={['plain','blue']}>{linkText}</Button>
        )}
      </div>
    </div>
  )
}

IconCard.propTypes = {
  image: imageTypeProps,
  heading: PropTypes.string,
  children: PropTypes.any,
  linkText: PropTypes.string,
  linkUrl: PropTypes.string,
}

export default IconCard
