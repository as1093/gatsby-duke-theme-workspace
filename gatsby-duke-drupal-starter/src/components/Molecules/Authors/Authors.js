/**
 *  Renders the authors information block for Story (full format):
 *  - Written by
 *  - Author(s) name, photo, short bio, link to profile
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import { Link } from "gatsby"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Image from "../../Atoms/Image/Image"
import HTMLEllipsis from "react-lines-ellipsis/lib/html"
import "./Authors.scss"

const Authors = ({ authors, block = "authors", modifiers }) => {
  return authors.length ? (
    <div className={bem(block, "wrapper", modifiers)}>
      <Heading level={3} modifiers={["label"]}>
        Written by
      </Heading>
      
      {authors.length ? (
        authors.map((author, index) => (
        <Row className={bem("author-card")} key={index}>
          {author.relationships.field_image &&
            <Col md={2}>
              <Image
                image={author.relationships.field_image?.relationships.field_media_image.localFile}
                imageType={'fluid'}
                alt={author.relationships.field_image?.field_media_image?.alt}
                modifiers={['rounded-circle']}
              />
            </Col>
          }
          
          <Col md={10}>
            <p className={bem("author-card","name")}>
              <Link to={author.path.alias}>
                {author.title}
              </Link>
            </p>
            <div className={bem("author-card", "bio")}>
              {author.field_bio?.summary ? (
                <CKEditorContent content={author.field_bio.summary} />
              ) : (
                <HTMLEllipsis
                  unsafeHTML={author.field_bio?.processed}
                  maxLine="3"
                  ellipsis="..."
                  basedOn="words"
                />
              )}
            </div>
          </Col>
        </Row>
        ))
      ) : ''}

    </div>
  ) : (
    ""
  )
}

Authors.propTypes = {
  authors: PropTypes.arrayOf(PropTypes.object),
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Authors
