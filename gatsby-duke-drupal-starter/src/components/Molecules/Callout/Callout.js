import React from "react"
import PropTypes from "prop-types"
import Image from "../../Atoms/Image/Image"
import { imageTypeProps } from "../../Atoms/Image/Image"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import Button from "../../Atoms/Button/Button"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import "./Callout.scss"

const Callout = ({
  image,
  imageAlignment = "left",
  heading,
  children,
  linkText,
  linkUrl,
  block = "callout",
  element,
  modifiers,
  additionalClasses,
}) => {
  let flexAlignment = (imageAlignment === 'right') ? 'flex-row-reverse' : ''
  return (
    <div className={`${bem(block, element, modifiers, additionalClasses)}  d-flex flex-row ${flexAlignment}`}>

      {image && (
        <div className={bem(block, "image")}>
          <Image 
            image={image.relationships.field_media_image.localFile} 
            alt={image.field_media_image.alt} 
            imageType={'fluid'} />
        </div>
      )}

      <div className={bem(block, "content")}>
        {/* Heading */}
        {heading && (
          <Heading level={3} block={block} element="heading">
            {heading}
          </Heading>
        )}
        {/* Body */}
        {children && <CKEditorContent content={children} />}
        {/* Button */}
        {linkUrl && (
          <Button link={linkUrl} showIcon={true}>{linkText}</Button>
        )}
      </div>
    </div>
  )
}

Callout.propTypes = {
  image: imageTypeProps,
  heading: PropTypes.string,
  linkText: PropTypes.string,
  linkUrl: PropTypes.string,
  children: PropTypes.any,
}

export default Callout
