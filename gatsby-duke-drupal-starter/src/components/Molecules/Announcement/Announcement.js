import React from "react"
import bem from "../../_utils/bem"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import { graphql, useStaticQuery } from 'gatsby';

import "./Announcement.scss"

const AnnouncementBlock = () => {
  const data = useStaticQuery(graphql`
    query {
      allNodeAnnouncement(filter: {status: {eq: true}}) {
        nodes {
          id
          title
          body {
            processed
          }
          path {
            alias
          }
          relationships {
            field_priority {
              name
            }
          }
          status
          changed
        }
      }
    }
  `)

  const sorted = data.allNodeAnnouncement.nodes.sort(function(a, b) {
    var date1 = new Date(a.changed)
    var date2 = new Date(b.changed)
    return date2 - date1
  })

  return (
    <>
      <div className={bem("announcement", "wrapper")}>
        {sorted[0] &&
          <>
            <p>{sorted[0].relationships.field_priority.name}</p>
            <div>{sorted[0].body.processed && <CKEditorContent content={sorted[0].body.processed} />}</div>
          </>
        }
      </div>
    </>
  )
}

export default AnnouncementBlock;