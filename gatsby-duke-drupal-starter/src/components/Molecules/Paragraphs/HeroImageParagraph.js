import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import styled from "styled-components"
import Heading from "../../Atoms/Heading/Heading"
import Button from "../../Atoms/Button/Button"
import { Container } from "react-bootstrap"

const HeroImageDiv = styled.div`
  padding: 7rem 0;
  text-align: center;
  margin: 0;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  color: white;
`

const HeroImageParagraph = ({
  node,
  block = "paragraph",
  element = "hero-image",
  modifiers,
}) => {
    let heroImage = null

    // use publicURL for svg or gif images, otherwise use childImageSharp
    if (node.relationships.field_image.relationships.field_media_image.localFile.childImageSharp) {
      heroImage = node.relationships.field_image.relationships.field_media_image.localFile.childImageSharp.fluid.src
    }
    else {
      heroImage = node.relationships.field_image.relationships.field_media_image.localFile.publicURL
    }

    return (
      <section className={bem(block, element, modifiers)}>

        <HeroImageDiv style={{backgroundImage: `url(${heroImage})`}} >

          <Container>

            {node.field_heading ? <Heading level={2} modifiers={['hero']}>{node.field_heading}</Heading> : ''}

            {node.field_link ? <Button link={node.field_link.uri}>{node.field_link.title}</Button> : ''}
          </Container>

        </HeroImageDiv>  

      </section>
    )
}

export const HeroImageParagraphTypeProps = PropTypes.shape({
  field_heading: PropTypes.string,
  field_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_image: PropTypes.object,
  }),
})

HeroImageParagraph.propTypes = {
  node: HeroImageParagraphTypeProps,
}

export default HeroImageParagraph
