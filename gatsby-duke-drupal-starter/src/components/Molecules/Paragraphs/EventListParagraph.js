import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import Button from "../../Atoms/Button/Button"
import EventCard from "../../Molecules/EventCard/EventCard"

const EventListParagraph = ({
  node,
  block = "paragraph",
  element = "eventlist",
  modifiers,
}) => {
  const allEventData = useStaticQuery(graphql`
    query {
      allNodeEvent {
        nodes {
          id
          title
          field_event_date {
            value
            end_value
          }
          field_location_link {
            uri
          }
          field_location_text
          relationships {
            field_status {
              name
            }
          }
          path {
            alias
          }
        }
      }
    }
  `)

  const allEventArray = []
  allEventData.allNodeEvent.nodes.forEach(event => (
    allEventArray.push({
      title: event.title,
      date: event.field_event_date?.value,
      link: event.path ? event.path.alias : "",
      location_url: event.field_location_link?.uri,
      location_text: event.field_location_text && event.field_location_text,
      status: event.relationships?.field_status?.name
    })
  ))

  const allEventSorted = allEventArray.sort(
    function(a,b) {
      var date1 = new Date(a.date)
      var date2 = new Date(b.date)
      return date2 - date1
    }
  )

  if (node.relationships.field_category !== null) {
    if (node.relationships.field_category.relationships.node__event !== null) {
      const sorted = node.relationships.field_category.relationships.node__event.sort(
        function(a, b) {
          var date1 = new Date(a.field_event_date.value)
          var date2 = new Date(b.field_event_date.value)
          return date2 - date1
        }
      )
  
      return (
        <section className={bem(block, element, modifiers)}>
          {node.field_display_title && (
            <Heading level={2}>{node.field_display_title}</Heading>
          )}
  
          {sorted.slice(0,4).map((item,i) => (
            <>
            <EventCard 
              key={"event-teaser-" + i}
              title={item.title}
              date={item.field_event_date ? item.field_event_date.value : ""}
              link={item.path ? item.path.alias : ""}
              status={item.relationships && item.relationships.field_status?.name}
              location_uri={item.field_location_link?.uri}
              location_text={item.field_location_text && item.field_location_text}
            />
            </>
          ))}
          {node.field_link &&
            <Button link={node.field_link?.uri} children={node.field_link?.title} />
          }
        </section>
      )
    }
    else {
      return (
        <section className={bem(block, element, modifiers)}>
          {node.field_display_title && (
            <Heading level={2}>{node.field_display_title}</Heading>
          )}
          <p>There is no news in this category(s).</p>
          {node.field_link &&
            <Button link={node.field_link?.uri} children={node.field_link?.title} />
          }
        </section>
      )
    }
  } else {
    return (
      <section className={bem(block, element, modifiers)}>
        {node.field_display_title && (
          <Heading level={2}>{node.field_display_title}</Heading>
        )}
        {allEventSorted.slice(0,4).map((item, i) => (
          <>
          <EventCard 
            key={"event-teaser-" + i}
            title={item.title}
            date={item.date}
            link={item.link}
            status={item.status}
            location_uri={item.location_url}
            location_text={item.location_text ? item.location_text : ''}
          />
          </>
        ))}
        {node.field_link &&
          <Button link={node.field_link?.uri} children={node.field_link?.title} />
        }
      </section>
    )
  }
}

export default EventListParagraph
