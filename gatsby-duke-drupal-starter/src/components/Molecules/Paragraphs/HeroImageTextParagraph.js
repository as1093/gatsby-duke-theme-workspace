import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import DrupalLink from "../../Atoms/DrupalLink/DrupalLink"
import Image from "../../Atoms/Image/Image"
import Eyebrow from "../../Atoms/Eyebrow/Eyebrow"
import { Container } from "react-bootstrap"

const HeroImageTextParagraph = ({
  node,
  block = "paragraph",
  element = "hero-image-text",
  modifiers,
}) => {

    return (
      <section className={bem(block, element, modifiers)}>
        <Container>

          {/* @TODO add field_hero_display_options */}
          <Image 
            image={node.relationships.field_image.relationships.field_media_image.localFile}
            imageType={'fluid'}
            alt={node.relationships.field_image.field_media_image.alt} 
          />
          
          {/* Tagline */}
          {node.field_tagline ? <Eyebrow text={node.field_tagline} /> : ''}
          
          {/* Title */}
          {node.field_display_title ? <Heading level={2}>{node.field_display_title}</Heading> : ''}

          {/* Subtitle */}
          {node.field_heading ? <Heading level={3}>{node.field_heading}</Heading> : ''}

          {node.field_link ? <DrupalLink url={node.field_link.uri}>{node.field_link.title}</DrupalLink> : ''}
        </Container>
      </section>
    )
}

export const HeroImageTextParagraphTypeProps = PropTypes.shape({
  field_heading: PropTypes.string,
  field_display_title: PropTypes.string,
  field_tagline: PropTypes.string,
  field_link: PropTypes.object,
  field_hero_display_options: PropTypes.string,
  relationships: PropTypes.shape({
    field_image: PropTypes.object,
  }),
})

HeroImageTextParagraph.propTypes = {
  node: HeroImageTextParagraphTypeProps,
}

export default HeroImageTextParagraph
