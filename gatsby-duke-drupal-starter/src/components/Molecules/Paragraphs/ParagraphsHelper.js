import React from "react"
import AccordionSectionParagraph from "./AccordionSectionParagraph"
import BlogListParagraph from "./BlogListParagraph"
import ButtonParagraph from "./ButtonParagraph"
import CalloutParagraph from "./CalloutParagraph"
import CarouselParagraph from "./CarouselParagraph"
import ContentParagraph from "./ContentParagraph"
import ContentReferenceParagraph from "./ContentReferenceParagraph"
import EventListParagraph from "./EventListParagraph"
import FaqsListParagraph from "./FaqsListParagraph"
import HeroImageParagraph from "./HeroImageParagraph"
import HeroImageTextParagraph from "./HeroImageTextParagraph"
import HeroVideoTextParagraph from "./HeroVideoTextParagraph"
import IframeParagraph from "./IframeParagraph"
import IconItemParagraph from "./IconItemParagraph"
import IconRowParagraph from "./IconRowParagraph"
import ImageCardParagraph from "./ImageCardParagraph"
import ImageCardRowParagraph from "./ImageCardRowParagraph"
import ImageTextParagraph from "./ImageTextParagraph"
import MediaParagraph from "./MediaParagraph"
import NewsListParagraph from "./NewsListParagraph"
import PullQuoteParagraph from "./PullQuoteParagraph"
import ScholarsListParagraph from "./ScholarsListParagraph"
import SlideshowParagraph from "./SlideshowParagraph"
import SocialMediaParagraph from "./SocialMediaParagraph"
import SpecialTextParagraph from "./SpecialTextParagraph"
import StoryListParagraph from "./StoryListParagraph"
import TwoColumnParagraph from "./TwoColumnParagraph"
import ThreeColumnParagraph from "./ThreeColumnParagraph"

const components = {
  paragraph__accordion_section: AccordionSectionParagraph,
  paragraph__blog_listing: BlogListParagraph,
  paragraph__button: ButtonParagraph,
  paragraph__callout: CalloutParagraph,
  paragraph__carousel: CarouselParagraph,
  paragraph__content: ContentParagraph,
  paragraph__content_reference: ContentReferenceParagraph,
  paragraph__event_listing: EventListParagraph,
  paragraph__faqs_list: FaqsListParagraph,
  paragraph__hero_image: HeroImageParagraph,
  paragraph__hero_image_text: HeroImageTextParagraph,
  paragraph__hero_video_text: HeroVideoTextParagraph,
  paragraph__iframe: IframeParagraph,
  paragraph__icon_item: IconItemParagraph,
  paragraph__icon_row: IconRowParagraph,
  paragraph__image_card: ImageCardParagraph,
  paragraph__image_card_row: ImageCardRowParagraph,
  paragraph__image_text: ImageTextParagraph,
  paragraph__media: MediaParagraph,
  paragraph__news_list: NewsListParagraph,
  paragraph__pull_quote: PullQuoteParagraph,
  paragraph__scholars_profile_list: ScholarsListParagraph,
  paragraph__slideshow: SlideshowParagraph,
  paragraph__social_media: SocialMediaParagraph,
  paragraph__special_text: SpecialTextParagraph,
  paragraph__story_list: StoryListParagraph,
  paragraph__two_column: TwoColumnParagraph,
  paragraph__three_column: ThreeColumnParagraph,
}

export const getParagraph = node => {
  if (components.hasOwnProperty(node.type)) {
    const ParagraphComponent = components[node.type]
    return <ParagraphComponent key={node.id} node={node} />
  }
  return <p key={node.id}>Unknown type paragraph {node.__typename}</p>
}
