import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import { getParagraph } from "./ParagraphsHelper.js"

const ThreeColumnParagraph = ({
  node,
  columnRight,
  columnMiddle,
  columnLeft,
  block = "paragraph",
  element = "threecolumn",
  modifiers,
}) => {
  const left_column_content =
    columnLeft !== undefined
      ? columnLeft
      : node.relationships !== undefined
      ? node.relationships.field_left_column.map(getParagraph)
      : ""
  const right_column_content =
    columnRight !== undefined
      ? columnRight
      : node.relationships !== undefined
      ? node.relationships.field_right_column.map(getParagraph)
      : ""
  const middle_column_content =
    columnMiddle !== undefined
      ? columnMiddle
      : node.relationships !== undefined
      ? node.relationships.field_middle_column.map(getParagraph)
      : ""

  // const right_column_content = (columnRight) ? columnRight : node.relationships.field_right_column.map(getParagraph);
  // const left_column_content = (columnLeft) ? columnLeft :  node.relationships.field_left_column.map(getParagraph);
  // const middle_column_content = (columnMiddle) ? columnMiddle : node.relationships.field_middle_column.map(getParagraph);

  return (
    <section className={bem(block, element, modifiers)}>
      <h2>{node.field_display_title}</h2>
      <Row>
        <Col>{left_column_content}</Col>
        <Col>{middle_column_content}</Col>
        <Col>{right_column_content}</Col>
      </Row>
    </section>
  )
}

export const ThreeColumnParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_display_title: PropTypes.string,
  relationships: PropTypes.shape({
    field_left_column: PropTypes.arrayOf(PropTypes.any),
    field_middle_column: PropTypes.arrayOf(PropTypes.any),
    field_right_column: PropTypes.arrayOf(PropTypes.any),
  }),
  type: PropTypes.string,
})

ThreeColumnParagraph.propTypes = {
  node: ThreeColumnParagraphTypeProps,
  columnRight: PropTypes.string,
  columnMiddle: PropTypes.string,
  columnLeft: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default ThreeColumnParagraph
