import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import { getNodeReference } from "../ContentReferences/ContentReferenceHelper"
import ScholarsProfileNodeReference from "../ContentReferences/ScholarsProfileNodeReference"

const ContentReferenceParagraph = ({
  node,
  block = "paragraph",
  element = "content-reference",
  modifiers,
}) => {
    const nodeReferences = node.relationships.nodeReferences?.map(getNodeReference)

    const scholarsProfiles = node.relationships.scholarsReferences?.map((scholarsProfile, index) => {
      return <ScholarsProfileNodeReference node={scholarsProfile} key={index} />
    })

    return (
      <section className={bem(block, element, modifiers)}>

        {/* Title */}
        {node.field_display_title ? <Heading level={2}>{node.field_display_title}</Heading> : ''}
        
        {nodeReferences}
        
        {scholarsProfiles}

      </section>
    )
}

ContentReferenceParagraph.propTypes = {
  node: PropTypes.shape({
    field_display_title: PropTypes.string,
    relationships: PropTypes.object,
  })
}

export default ContentReferenceParagraph
