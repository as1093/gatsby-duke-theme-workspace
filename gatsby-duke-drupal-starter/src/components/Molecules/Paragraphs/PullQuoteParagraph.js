import React from "react"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import bem from "../../_utils/bem"

const PullQuoteParagraph = ({ 
  node,
  block = "paragraph",
  element = "pullquote",
  modifiers, 
  }) => {
  return (
    <section className={bem(block, element, modifiers)}>
      <CKEditorContent content={node.field_quote.processed} />
      <p className="pullquote-attribution">{node.field_attribution !== null && node.field_attribution}</p>
    </section>
  )
}

export default PullQuoteParagraph
