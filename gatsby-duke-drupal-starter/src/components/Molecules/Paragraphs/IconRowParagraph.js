import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import IconItemParagraph from "./IconItemParagraph"
import Heading from "../../Atoms/Heading/Heading"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

const IconRowParagraph = ({
  node,
  image,
  block = "paragraph",
  element = "iconrow",
  modifiers,
}) => {

    return (
      <section className={bem(block, element, modifiers)}>
        <Heading level={3}>{node.field_display_title ? node.field_display_title : ""}</Heading>

        <Row>
          {node.relationships.field_paragraph_items?.map((icon, index) => (
            <Col key={index} md={6} lg={3}>
              <IconItemParagraph node={icon} />
            </Col>
          ))}
        </Row>
      </section>
    )
}

export const IconRowParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_heading: PropTypes.string,
  field_textarea: PropTypes.string,
  field_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_paragraph_items: PropTypes.array,
  }),
  type: PropTypes.string,
})

IconRowParagraph.propTypes = {
  node: IconRowParagraphTypeProps,
}

export default IconRowParagraph
