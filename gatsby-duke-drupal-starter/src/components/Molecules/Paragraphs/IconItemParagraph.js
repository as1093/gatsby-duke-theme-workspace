import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import IconCard from "../IconCard/IconCard"
import "./Paragraph.scss"

const IconItemParagraph = ({
  node,
  image,
  block = "paragraph",
  element = "iconitem",
  modifiers,
}) => {
  // console.log(node)
    return (
      <section className={bem(block, element, modifiers)}>
        
        <IconCard
          image={node.relationships.field_image ? node.relationships.field_image : ''} 
          heading={node.field_heading ? node.field_heading : ""} 
          linkText={node.field_link ? node.field_link.title : ""} 
          linkUrl={node.field_link ? node.field_link.uri : ""}
        >
          {node.field_body ? node.field_body.processed : ''}
        </IconCard>
      </section>
    )
}

export const IconItemParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_heading: PropTypes.string,
  field_body: PropTypes.object,
  field_link: PropTypes.object,
  field_image_alignment: PropTypes.object,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
  type: PropTypes.string,
})

IconItemParagraph.propTypes = {
  node: IconItemParagraphTypeProps,
  image: PropTypes.object,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default IconItemParagraph
