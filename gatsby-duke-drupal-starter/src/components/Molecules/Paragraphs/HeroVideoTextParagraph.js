import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import DrupalLink from "../../Atoms/DrupalLink/DrupalLink"
import Eyebrow from "../../Atoms/Eyebrow/Eyebrow"
import ReactPlayer from "react-player/lazy"
import { Container } from "react-bootstrap"

const HeroVideoTextParagraph = ({
  node,
  block = "paragraph",
  element = "hero-video-text",
  modifiers,
}) => {
  return (
    <section className={bem(block, element, modifiers)}>
      <Container>

        <div className="video video-responsive">
          <ReactPlayer
            url={node.relationships.field_media_item.field_media_oembed_video}
            controls={false}
            loop={true}
            volume={0}
            playing={true}
          />
        </div>

        {/* Tagline */}
        {node.field_tagline ? <Eyebrow text={node.field_tagline} /> : ""}

        {/* Title */}
        {node.field_display_title ? (
          <Heading level={2}>{node.field_display_title}</Heading>
        ) : (
          ""
        )}

        {/* Subtitle */}
        {node.field_heading ? (
          <Heading level={3}>{node.field_heading}</Heading>
        ) : (
          ""
        )}

        {node.field_link ? (
          <DrupalLink url={node.field_link.uri}>{node.field_link.title}</DrupalLink>
        ) : (
          ""
        )}
      </Container>
    </section>
  )
}

export const HeroVideoTextParagraphTypeProps = PropTypes.shape({
  field_heading: PropTypes.string,
  field_display_title: PropTypes.string,
  field_tagline: PropTypes.string,
  field_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
})

HeroVideoTextParagraph.propTypes = {
  node: HeroVideoTextParagraphTypeProps,
}

export default HeroVideoTextParagraph
