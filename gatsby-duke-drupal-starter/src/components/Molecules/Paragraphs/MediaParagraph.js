import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Image, { imageTypeProps } from "../../Atoms/Image/Image"
import DrupalLink from "../../Atoms/DrupalLink/DrupalLink"
import ReactPlayer from "react-player/lazy"
import Heading from "../../Atoms/Heading/Heading"
import Eyebrow from "../../Atoms/Eyebrow/Eyebrow"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"

const MediaParagraph = ({
  node,
  image,
  block = "paragraph",
  element = "media",
  modifiers,
}) => {
  if (node.relationships.field_media_item !== null) {
    let mediaItem = node.relationships.field_media_item
    let renderedMediaItem = null
    switch (mediaItem.type) {
      case "media__image":       
        renderedMediaItem = (
          <Image 
            image={mediaItem.relationships.field_media_image.localFile}
            imageType={'fluid'}
            alt={mediaItem.relationships.field_media_image.alt}
          />
        )
        break
      case "media__video":
        const { host } = new URL(mediaItem.field_media_oembed_video)
        const providerWarpwire = [
          "warpwire",
          "warpwire.duke.edu",
        ].includes(host)
        if (providerWarpwire) {
          renderedMediaItem = (
            <div className="video video-responsive">
              <iframe id="wwvideo" title={mediaItem.name} data-ww-id="apiTestFrame" src={mediaItem.field_media_oembed_video} frameBorder="0" scrolling="0" allowFullScreen></iframe>
            </div>
          )
        }
        else {
          renderedMediaItem = (
            <div className="video video-responsive">
              <ReactPlayer
                url={mediaItem.field_media_oembed_video}
                controls={true}
              />
            </div>
          )
        }  
        break
      case "media__audio":
        renderedMediaItem = (
          <div className="audio audio-responsive">
            <ReactPlayer url={mediaItem.field_media_soundcloud} />
          </div>
        )
        break
      case "media__document":
        renderedMediaItem = (
          <DrupalLink
            url={mediaItem.relationships.field_document.localFile.publicURL}
          >
            {mediaItem.name}
          </DrupalLink>
        )
        break
      default:
        renderedMediaItem = ""
        break
    }

    return (
      <section className={bem(block, element, modifiers)}>
        <figure role="figure">
          {node.field_tagline ? <Eyebrow text={node.field_tagline} /> : ""}
          {node.field_display_title ? (
            <Heading level={2}>{node.field_display_title}</Heading>
          ) : (
            ""
          )}
          {renderedMediaItem}
          {node.field_media_caption ? (
            <figcaption>
              <CKEditorContent content={node.field_media_caption.processed} />
            </figcaption>
          ) : (
            ""
          )}
        </figure>
      </section>
    )
  } else {
    return ""
  }
}

export const MediaParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_display_title: PropTypes.string,
  field_tagline: PropTypes.string,
  field_media_caption: PropTypes.object,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
  type: PropTypes.string,
})

MediaParagraph.propTypes = {
  node: MediaParagraphTypeProps,
  image: imageTypeProps,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default MediaParagraph
