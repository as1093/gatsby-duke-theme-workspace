import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import Slider from "../Slider/Slider"

const SlideshowParagraph = ({
  node,
  block = "paragraph",
  element = "slideshow",
  modifiers,
}) => {

    return (
      <section className={bem(block, element, modifiers)}>
        <Heading level={3}>{node.field_display_title ? node.field_display_title : ""}</Heading>

        <Slider slides={node.relationships?.field_paragraph_items} />
      </section>
    )
}

export const SlideshowParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_display_title: PropTypes.string,
  relationships: PropTypes.shape({
    field_paragraph_items: PropTypes.array,
  }),
  type: PropTypes.string,
})

SlideshowParagraph.propTypes = {
  node: SlideshowParagraphTypeProps,
}

export default SlideshowParagraph
