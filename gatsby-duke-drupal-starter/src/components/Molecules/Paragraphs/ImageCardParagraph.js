import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Card from "../Card/Card"
import "./Paragraph.scss"

const ImageCardParagraph = ({
  node,
  image,
  block = "paragraph",
  element = "imagecard",
  modifiers,
}) => {
    
    const croppedImageStyle = (node.relationships?.field_image?.field_focal_point_area ? node.relationships.field_image.field_focal_point_area : 'ATTENTION');

    return (
      <section className={bem(block, element, modifiers)}>
        
        <Card
          image={node.relationships?.field_image ? node.relationships.field_image : ''} 
          imageCrop={croppedImageStyle}
          heading={node.field_heading ? node.field_heading : ""} 
          linkText={node.field_link ? node.field_link.title : ""} 
          linkUrl={node.field_link ? node.field_link.uri : ""}
          modifiers={['image-card',`${modifiers}`]}>
            {node.field_body ? node.field_body.processed : ''}
        </Card>
      </section>
    )
}

export const ImageCardParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_heading: PropTypes.string,
  field_body: PropTypes.object,
  field_link: PropTypes.object,
  field_image_alignment: PropTypes.object,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
  type: PropTypes.string,
})

ImageCardParagraph.propTypes = {
  node: ImageCardParagraphTypeProps,
  image: PropTypes.object,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default ImageCardParagraph
