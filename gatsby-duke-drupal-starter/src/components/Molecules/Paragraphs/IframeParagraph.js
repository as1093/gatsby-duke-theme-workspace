/**
 *  Queries and displays content from the Content Paragraph Bundle
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import Iframe from "../../Atoms/Iframe/Iframe"

const IframeParagraph = ({
  node,
  url,
  block = "paragraph",
  element = "iframe",
  modifiers,
}) => {
  const link = url ? url : node.field_link.uri
  return (
    <section className={bem(block, element, modifiers)}>
      <Heading level={3}>
        {node.field_display_title ? node.field_display_title : ""}
      </Heading>

      <Iframe
        src={link}
        title={node.field_link.title}
        width={node.field_width}
        height={node.field_height}
      />
    </section>
  )
}

export const IframeParagraphTypeProps = PropTypes.shape({
  field_display_title: PropTypes.string,
  field_width: PropTypes.string,
  field_height: PropTypes.string,
  field_link: PropTypes.object,
  id: PropTypes.string,
  type: PropTypes.string,
})

IframeParagraph.propTypes = {
  node: IframeParagraphTypeProps,
  url: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default IframeParagraph
