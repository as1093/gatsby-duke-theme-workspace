import React from "react"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import bem from "../../_utils/bem"
import PropTypes from "prop-types"
import Heading from "../../Atoms/Heading/Heading"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Image from "../../Atoms/Image/Image"
import Button from "../../Atoms/Button/Button"

const CalloutParagraph = ({
  node,
  block = "paragraph",
  element = "callout",
  modifiers,
}) => {
  let croppedImage 
  let croppedImageStyle
  if (node.relationships.field_image) {
    croppedImageStyle = node.relationships.field_image
    .field_focal_point_area
    ? node.relationships.field_image.field_focal_point_area
    : "CENTER"
    croppedImage =
      node.relationships.field_image.relationships.field_media_image.localFile
  }

  return (
    <section className={bem(block, element, modifiers)}>
      <Row>
        <Col>
          {croppedImage ? (
            <Image
              image={croppedImage}
              crop={croppedImageStyle}
              imageType={`fluid`}
              alt={node.relationships.field_image.field_media_image.alt}
            />
          ) : (
            ""
          )}
        </Col>
        <Col>
          {node.field_display_title ? (
            <Heading level={2}>{node.field_display_title}</Heading>
          ) : (
            ""
          )}

          <CKEditorContent
            content={node.field_body ? node.field_body.processed : ""}
          />

          {node.field_link.uri ? (
            <Button link={node.field_link.uri}>{node.field_link.title}</Button>
          ) : (
            ""
          )}
        </Col>
      </Row>
    </section>
  )
}

export const CalloutParagraphTypeProps = PropTypes.shape({
  field_display_title: PropTypes.string,
  field_body: PropTypes.object,
  field_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_image: PropTypes.object,
  }),
})

CalloutParagraph.propTypes = {
  node: CalloutParagraphTypeProps,
  image: PropTypes.object,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default CalloutParagraph
