import React from "react"
import { graphql, StaticQuery } from "gatsby"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"

// for accordion custom styling: https://react-bootstrap.github.io/components/accordion/
import Accordion from 'react-bootstrap/Accordion'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'

const FaqsListParagraph = ({ 
  node,
  block = "paragraph",
  element = "faqlist",
  modifiers, 
  }) => {
  return (
    <section className={bem(block, element, modifiers)}>
      <Heading level={2}>{node.field_display_title}</Heading>
      <Accordion>
        {node.relationships.field_category !== null ? node.relationships.field_category.relationships.node__faq.map((element,i) => (
          <Card key={i+1}>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey={i+1}>
                {element.field_question}
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey={i+1}>
              <Card.Body>
                <CKEditorContent content={element.field_answer.processed} />
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        )) : <StaticQuery 
          query={graphql`
            {
              allNodeFaq {
                nodes {
                  id
                  title
                  field_question
                  field_answer {
                    processed
                  }
                }
              }
            }
          `}
          render={data => (
            <>
              {data.allNodeFaq.nodes.map((node,i) => (
                <Card key={i+1}>
                  <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey={i+1}>
                      {node.field_question}
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey={i+1}>
                    <Card.Body><CKEditorContent content={node.field_answer.processed} /></Card.Body>
                  </Accordion.Collapse>
                </Card>
              ))}
            </>
          )}
        /> }
      </Accordion>
    </section>
  )
}

export default FaqsListParagraph
