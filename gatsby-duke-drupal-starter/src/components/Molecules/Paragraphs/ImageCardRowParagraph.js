import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import ImageCardParagraph from "../Paragraphs/ImageCardParagraph"
import Heading from "../../Atoms/Heading/Heading"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

const ImageCardRowParagraph = ({
  node,
  image,
  block = "paragraph",
  element = "imagecardrow",
  modifiers,
}) => {

    return (
      <section className={bem(block, element, modifiers)}>
        <Heading level={3}>{node.field_display_title ? node.field_display_title : ""}</Heading>

        <Row>
          {node.relationships.field_image_card?.map((card, index) => (
            <Col key={index} lg>
              <ImageCardParagraph node={card} 
                modifiers={(index%2) ? ['even'] : []} 
              />
            </Col>
          ))}
        </Row>
      </section>
    )
}

export const ImageCardRowParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_heading: PropTypes.string,
  field_textarea: PropTypes.string,
  field_link: PropTypes.object,
  field_image_alignment: PropTypes.object,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
  type: PropTypes.string,
})

ImageCardRowParagraph.propTypes = {
  node: ImageCardRowParagraphTypeProps,
}

export default ImageCardRowParagraph
