import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Callout from "../Callout/Callout"

const ImageTextParagraph = ({ node, modifiers }) => {
  return (
    <section className={bem("paragraph", "imagetext", modifiers)}>
      <Callout
        image={
          node.relationships.field_image ? node.relationships.field_image : ""
        }
        imageAlignment={
          node.field_image_alignment ? node.field_image_alignment : "left"
        }
        heading={node.field_heading ? node.field_heading : ""}
        linkText={node.field_link ? node.field_link.title : ""}
        linkUrl={node.field_link ? node.field_link.uri : ""}
        modifiers={["image-text"]}
      >
        {node.field_textarea ? node.field_textarea.processed : ""}
      </Callout>
    </section>
  )
}

export const ImageTextParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_heading: PropTypes.string,
  field_textarea: PropTypes.object,
  field_link: PropTypes.object,
  field_image_alignment: PropTypes.string,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
  type: PropTypes.string,
})

ImageTextParagraph.propTypes = {
  node: ImageTextParagraphTypeProps,
  image: PropTypes.object,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default ImageTextParagraph
