import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import { Instagram, Facebook } from "reactjs-social-embed"
import Twitter from "../../Atoms/Social/Twitter"
import Heading from "../../Atoms/Heading/Heading"

const SocialMediaParagraph = ({
  node,
  block = "paragraph",
  element = "social-media",
  modifiers,
}) => {
  if (node.field_url !== null) {
    let renderedSocialMediaItem = ""
    const url = node.field_url.uri
    const { host } = new URL(url)

    const providerInstagram = [
      "instagr.am",
      "www.instagr.am",
      "instagram.com",
      "www.instagram.com",
    ].includes(host)

    const providerFacebook = [
      "facebook",
      "www.facebook",
      "facebook.com",
      "www.facebook.com",
    ].includes(host)

    const providerTwitter = [
      "twitter",
      "twitter.com",
      "www.twitter.com",
    ].includes(host)

    if (providerInstagram) renderedSocialMediaItem = <Instagram url={url} />

    if (providerFacebook) renderedSocialMediaItem = <Facebook url={url} />

    if (providerTwitter) renderedSocialMediaItem = <Twitter url={url} />

    return (
      <section className={bem(block, element, modifiers)}>
        {node.field_display_title ? (
          <Heading level={2}>{node.field_display_title}</Heading>
        ) : (
          ""
        )}

        {renderedSocialMediaItem}
      </section>
    )
  } else {
    return ""
  }
}

SocialMediaParagraph.propTypes = {
  node: PropTypes.shape({
    field_display_title: PropTypes.string,
    field_url: PropTypes.shape({
      uri: PropTypes.string,
    }),
  }),
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default SocialMediaParagraph
