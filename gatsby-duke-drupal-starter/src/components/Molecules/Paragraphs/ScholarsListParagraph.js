import React from "react"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"

const ScholarsListParagraph = ({
  node,
  block = "paragraph",
  element = "scholarslist",
  modifiers,
}) => {
  return (
    <section className={bem(block, element, modifiers)}>
      {/* Title */}
      {node.field_display_title ? (
        <Heading level={2}>{node.field_display_title}</Heading>
      ) : (
        ""
      )}

      <p>Scholars Profile listing will go here.</p>
    </section>
  )
}

export default ScholarsListParagraph
