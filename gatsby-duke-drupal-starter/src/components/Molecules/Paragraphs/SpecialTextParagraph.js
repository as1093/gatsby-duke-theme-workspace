import React from "react"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import bem from "../../_utils/bem"
// import Heading from "../../Atoms/Heading/Heading"

const SpecialTextParagraph = ({ 
  node,
  block = "paragraph",
  element = "specialtext",
  modifiers, 
  }) => {
  return (
    <section className={bem(block, element, modifiers)}>
      <CKEditorContent content={node.field_special_text ? node.field_special_text.processed: ""} />
    </section>
  )
}

export default SpecialTextParagraph
