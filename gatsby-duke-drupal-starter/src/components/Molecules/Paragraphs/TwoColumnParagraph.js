import React from 'react'
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { getParagraph } from './ParagraphsHelper.js'

const TwoColumnParagraph = ({ 
  node, 
  columnRight,
  columnLeft,
  block = 'paragraph',
  element = 'twocolumn',
  modifiers
}) => {
  const right_column_content = (columnRight) ? columnRight : node.relationships.field_right_column.map(getParagraph);
  const left_column_content = (columnLeft) ? columnLeft :  node.relationships.field_left_column.map(getParagraph);

  // Set up the column ratio (options: 1-1, 1-2, 2-1)
  let left_column_ratio = 6; // default is equal columns
  let right_column_ratio = 6;
  if (node.field_pb_column_layout !== null) {
    switch (node.field_pb_column_layout) {
      case '1-2':
        left_column_ratio = 4;
        right_column_ratio = 8;
        break;
      case '2-1':
        left_column_ratio = 8;
        right_column_ratio = 4;
        break; 
      default:
        left_column_ratio = 6;
        right_column_ratio = 6;
        break;                 
    }
  }

  return (
    <section className={bem(block, element, modifiers)}>
      <h2>{node.field_display_title}</h2>
      <Row>
        <Col md={left_column_ratio}>
          {left_column_content}
        </Col>
        <Col md={right_column_ratio}>
          {right_column_content}
        </Col>
      </Row>
    </section>
  );
};

export const TwoColumnParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_pb_column_layout: PropTypes.string,
  field_display_title: PropTypes.string,
  relationships: PropTypes.shape({
    field_left_column: PropTypes.arrayOf(PropTypes.any),
    field_right_column: PropTypes.arrayOf(PropTypes.any),
  }),
  type: PropTypes.string
});

TwoColumnParagraph.propTypes = {
  node: TwoColumnParagraphTypeProps,
  columnRight: PropTypes.string,
  columnLeft: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
};

export default TwoColumnParagraph
