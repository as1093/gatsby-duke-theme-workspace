import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import Button from "../../Atoms/Button/Button"
import NewsCard from "../../Molecules/NewsCard/NewsCard"

const NewsListParagraph = ({
  node,
  block = "paragraph",
  element = "newslist",
  modifiers,
}) => {
  const allNewsData = useStaticQuery(graphql`
    query {
      allNodeNews {
        nodes {
          id
          title
          field_date
          path {
            alias
          }
          field_alternate_link {
            uri
          }
          body {
            processed
            summary
          }
          relationships {
            field_featured_media {
              field_media_image {
                alt
              }
              relationships {
                field_media_image {
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 1025) {
                        ...GatsbyImageSharpFluid
                      }
                    }
                  }
                }
              }
            }
          }
          field_feed_image_url {
            uri
          }
          field_feed_image_alt_text
          field_news_source
        }
      }
    }
  `)

  const allNewsArray = []
  allNewsData.allNodeNews.nodes.forEach(news => (
    allNewsArray.push({
      title: news.title,
      date: news.field_date && news.field_date,
      link: news.path?.alias,
      alternate_link: news.field_alternate_link,
      body: news.body ? (news.body.summary ? news.body.summary : news.body.processed) : "",
      source: news.field_news_source && news.field_news_source,
      featured_image: news.relationships?.field_featured_media,
      feed_image_url: news.field_feed_image_url && news.field_feed_image_url,
      feed_image_alt: news.field_feed_image_alt_text && news.field_feed_image_alt_text
    })
  ))

  const allNewsSorted = allNewsArray.sort(
    function(a,b) {
      var date1 = new Date(a.date)
      var date2 = new Date(b.date)
      return date2 - date1
    }
  )

  /* news teaser list with category(s) */
  if (node.relationships.field_category !== null) {
    if (node.relationships.field_category.relationships.node__news !== null) {
      const sorted = node.relationships.field_category.relationships.node__news.sort(
        function(a, b) {
          var date1 = new Date(a.field_date)
          var date2 = new Date(b.field_date)
          return date2 - date1
        }
      )
  
      return (
        <section className={bem(block, element, modifiers)}>
          {node.field_display_title && (
            <Heading level={2}>{node.field_display_title}</Heading>
          )}
  
          {sorted.slice(0,1).map((item,i) => (
            <NewsCard
              key={"news-card-" + i}
              alternate_link={item.field_alternate_link}
              date={item.field_date ? item.field_date : ""}
              link={item.path ? item.path.alias : ""}
              title={item.title}
              body={item.body ? (item.body.summary ? item.body.summary : item.body.processed) : ''}
              source={item.field_news_source && item.field_news_source}
              featured_image={item.relationships.field_featured_media}
              feed_image_url={item.field_feed_image_url && item.field_feed_image_url}
              feed_image_alt={item.field_feed_image_alt_text && item.field_feed_image_alt_text}
            />
          ))}
          {sorted.slice(1,4).map((item,i) => (
            <NewsCard
              key={"news-card-" + i}
              alternate_link={item.field_alternate_link}
              date={item.field_date ? item.field_date : ""}
              link={item.path ? item.path.alias : ""}
              title={item.title}
            />
          ))}
          {node.field_link &&
            <Button link={node.field_link?.uri} children={node.field_link?.title} />
          }
        </section>
      )
    }
    /* news teaser list with category(s), but no news in that category(s) */
    else {
      return (
        <section className={bem(block, element, modifiers)}>
          {node.field_display_title && (
            <Heading level={2}>{node.field_display_title}</Heading>
          )}
          <p>There is no news in this category(s).</p>
          {node.field_link &&
            <Button link={node.field_link?.uri} children={node.field_link?.title} />
          }
        </section>
      )
    }
  } 
  /* news teaser list with NO category(s) */
  else {
    return (
      <section className={bem(block, element, modifiers)}>
        {node.field_display_title && (
          <Heading level={2}>{node.field_display_title}</Heading>
        )}
        {allNewsSorted.slice(0,1).map((item, i) => (
          <NewsCard
            key={"news-card-" + i}
            date={item.date ? item.date : ""}
            link={item.link}
            alternate_link={item.alternate_link}
            title={item.title}
            body={item.body}
            source={item.source && item.source}
            featured_image={item.featured_image}
            feed_image_url={item.feed_image_url && item.feed_image_url}
            feed_image_alt={item.feed_image_alt_text && item.feed_image_alt_text}
          />
        ))}
        {allNewsSorted.slice(1,4).map((item, i) => (
          <NewsCard
            key={"news-card-" + i}
            date={item.date ? item.date : ""}
            link={item.link}
            alternate_link={item.alternate_link}
            title={item.title}
          />
        ))}
        {node.field_link &&
          <Button link={node.field_link?.uri} children={node.field_link?.title} />
        }
      </section>
    )
  }
}

export default NewsListParagraph
