import React from "react"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"

const BlogListParagraph = ({
  node,
  block = "paragraph",
  element = "bloglist",
  modifiers,
}) => {
  return (
    <section className={bem(block, element, modifiers)}>
      {/* Title */}
      {node.field_display_title ? (
        <Heading level={2}>{node.field_display_title}</Heading>
      ) : (
        ""
      )}

      <p>Blog post listing will go here.</p>
    </section>
  )
}

export default BlogListParagraph
