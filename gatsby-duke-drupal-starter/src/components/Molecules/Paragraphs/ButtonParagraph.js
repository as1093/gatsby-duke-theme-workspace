/**
 *  Queries and displays content from the Button Paragraph Bundle
 */
import React from "react"
import bem from "../../_utils/bem"
import PropTypes from "prop-types"
import Button from "../../Atoms/Button/Button"

const ButtonParagraph = ({
  node,
  block = "paragraph",
  element = "button",
  modifiers,
}) => {
  return (
    <section className={`${bem(block, element, modifiers)} text-${node.field_alignment}`}>
      {node.field_link ? (
        <Button link={node.field_link.uri} showIcon={true}>{node.field_link.title}</Button>
      ) : (
        ""
      )}
    </section>
  )
}

ButtonParagraph.propTypes = {
  node: PropTypes.shape({
    field_alignment: PropTypes.string,
    field_link: PropTypes.object,
  }),
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default ButtonParagraph
