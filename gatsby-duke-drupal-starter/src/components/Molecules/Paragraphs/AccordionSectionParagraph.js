import React from "react"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
// for accordion custom styling: https://react-bootstrap.github.io/components/accordion/
import Accordion from 'react-bootstrap/Accordion'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'

const AccordionSectionParagraph = ({ 
  node,
  block = "paragraph",
  element = "accordion",
  modifiers,
  }) => {
  return (
    <section className={bem(block, element, modifiers)}>
      <Heading level={2}>{node.field_display_title}</Heading>
      <Accordion>
      {node.relationships && (node.relationships.field_accordion_item.map((element, i) => (
        <Card key={i+1}>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey={i+1}>
              {element.field_heading}
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey={i+1}>
            <Card.Body>
              {element.field_content && <CKEditorContent content={element.field_content.processed} /> }
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      )))}
      </Accordion>
    </section>
  )
}

export default AccordionSectionParagraph
