/**
 *  Queries and displays content from the Content Paragraph Bundle
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"

const ContentParagraph = ({
  node,
  text,
  block = "paragraph",
  element = "wysiwyg",
  modifiers,
}) => {
  const processedContent = node.field_textarea
    ? node.field_textarea.processed
    : ""
  const content = text ? text : processedContent
  return (
    <section className={bem(block, element, modifiers)}>
      <CKEditorContent
        content={content}
        className={bem(block, element, modifiers)}
      />
    </section>
  )
}

export const ContentParagraphTypeProps = PropTypes.shape({
  field_textarea: PropTypes.shape({
    processed: PropTypes.string,
  }),
  id: PropTypes.string,
  type: PropTypes.string,
})

ContentParagraph.defaultProps = {
  node: {
    field_textarea: {
      processed: "",
    },
  },
}

ContentParagraph.propTypes = {
  node: ContentParagraphTypeProps,
  text: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default ContentParagraph
