import React from "react"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"

const StoryListParagraph = ({
  node,
  block = "paragraph",
  element = "storylist",
  modifiers,
}) => {
  return (
    <section className={bem(block, element, modifiers)}>
      {/* Title */}
      {node.field_display_title ? (
        <Heading level={2}>{node.field_display_title}</Heading>
      ) : (
        ""
      )}

      <p>Story listing will go here.</p>
    </section>
  )
}

export default StoryListParagraph
