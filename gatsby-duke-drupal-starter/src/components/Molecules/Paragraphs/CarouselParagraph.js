import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import Eyebrow from "../../Atoms/Eyebrow/Eyebrow"
import DrupalLink from "../../Atoms/DrupalLink/DrupalLink"
import Image from "../../Atoms/Image/Image"

const CarouselParagraph = ({
  node,
  block = "paragraph",
  element = "hero-carousel",
  modifiers,
}) => {
  const slides = node.relationships?.field_paragraph_items
  return (
    <section className={bem(block, element, modifiers)}>

      {slides?.map((slide, index) => (
        <div key={index}>
          {slide.field_tagline ? <Eyebrow text={slide.field_tagline} /> : ""}
          {slide.field_heading ? (
            <Heading level={4}>{slide.field_heading}</Heading>
          ) : (
            ""
          )}

          <Image 
            image={slide.relationships.field_image.relationships.field_media_image.localFile}
            crop={'CENTER'}
            imageType={'fixed'}
            alt={slide.relationships.field_image.field_media_image.alt} 
          />

          {slide.field_url ? (
            <DrupalLink url={slide.field_url.uri}>{slide.field_url.title}</DrupalLink>
          ) : (
            ""
          )}
        </div>
      ))}
    </section>
  )
}

export const CarouselParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  relationships: PropTypes.shape({
    field_paragraph_items: PropTypes.array,
  }),
})

CarouselParagraph.propTypes = {
  node: CarouselParagraphTypeProps,
}

export default CarouselParagraph
