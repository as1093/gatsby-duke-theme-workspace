import React from 'react'
import PropTypes from 'prop-types'
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import DateProcessor from "../../Atoms/DateProcessor/DateProcessor"
import Image from "../../Atoms/Image/Image"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"

const NewsCard = ({
  date,
  link,
  alternate_link,
  title,
  body,
  source,
  featured_image,
  feed_image_url,
  feed_image_alt,
  block = 'news-card',
  element,
  modifiers,
}) => {
  return (
    <div className={`${bem(block, element, modifiers)}`}>
      {feed_image_url ? (
        <>
          {featured_image ? (
            <div className="news-featured-image">
              {featured_image.relationships.field_media_image !== null && (
                <Image
                  image={
                    featured_image &&
                    featured_image.relationships.field_media_image
                      .localFile
                  }
                  imageType={"fluid"}
                  alt={featured_image.relationships.field_media_image.alt}
                />
              )}
            </div>
          ) : (
            <div className="news-featured-image">
              <Image
                image={feed_image_url.uri}
                imageType={"fluid"}
                alt={feed_image_alt}
              />
            </div>
          )}
        </>
      ) : (
        <>
          {featured_image && (
            <div className="news-featured-image">
              {featured_image.relationships.field_media_image !== null && (
                <Image
                  image={
                    featured_image &&
                    featured_image.relationships.field_media_image
                      .localFile
                  }
                  imageType={"fluid"}
                  alt={featured_image.relationships.field_media_image.alt}
                />
              )}
            </div>
          )}
        </>
      )}
      {source ? (
        <>
          <DateProcessor startDate={date} /><p>&nbsp;&#183;&nbsp;{source}</p>
        </>
      ) : (
        <>
          <DateProcessor startDate={date} />
        </>
      )}
      {alternate_link ? (
        <>
          <Heading level={4}><a href={alternate_link.uri}>{title}</a></Heading>
        </>
      ) : (
        <>
          <Heading level={4}><a href={link}>{title}</a></Heading>
        </>
      )}
      {<CKEditorContent content={body} />}
    </div>
  )
}

NewsCard.propTypes = {
  date: PropTypes.string,
  link: PropTypes.string,
  alternate_link: PropTypes.object,
  title: PropTypes.string,
  featured_image: PropTypes.string,
  feed_image_url: PropTypes.object,
  feed_image_alt: PropTypes.string,
}

export default NewsCard
