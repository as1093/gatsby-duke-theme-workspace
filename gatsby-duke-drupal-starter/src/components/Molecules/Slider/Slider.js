import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import Carousel from 'react-bootstrap/Carousel'
import "./Slider.scss"
import Image from "../../Atoms/Image/Image"

const Slider = ({
  slides,
  block = "slider",
  element,
  modifiers,
}) => {
  return (
    <div className={bem(block, element, modifiers)}>

      <Carousel>
          {slides?.map((slide, index) => (
            <Carousel.Item key={index}>
              <Image
                image={slide.relationships.field_image.relationships.field_media_image.localFile}
                imageType={'fluid'}
                alt={slide.relationships.field_image.field_media_image.alt} 
              />
              <Carousel.Caption>
                <CKEditorContent content={slide.field_media_caption?.processed} />
              </Carousel.Caption>
            </Carousel.Item>
          ))}
        </Carousel>
        <br className="clearfix" />

    </div>
  )
}

Slider.propTypes = {
  slides: PropTypes.arrayOf(PropTypes.object)
}

export default Slider
