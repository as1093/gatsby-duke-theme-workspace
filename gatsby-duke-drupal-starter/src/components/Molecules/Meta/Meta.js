/**
 *  Renders the meta information for News article, Story, and Blog article:
 *  - By
 *  - Authors (both internal and external)
 *  - //
 *  - Date (Month d, yyyy)
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import "./Meta.scss"
import { Link } from "gatsby"

const Meta = ({ authors, date, modifiers }) => {
  // Separate the authors by a comma
  const formattedAuthors = authors.map((author, index) => (
    <span key={index}>
      <Link to={author.path.alias}>{author.title}</Link>
      {index < authors.length - 1 ? ", " : " "}
    </span>
  ))

  return (
    <div className={bem("meta", "wrapper", modifiers)}>
      {authors.length ? (
        <span className={bem("meta", "authors")}>
          By {formattedAuthors}
          <span className={bem("meta", "separator", modifiers)}>
            &#47;&#47;
          </span>
        </span>
      ) : (
        ""
      )}
      &nbsp;{date}
    </div>
  )
}

Meta.propTypes = {
  authors: PropTypes.arrayOf(PropTypes.object),
  date: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Meta
