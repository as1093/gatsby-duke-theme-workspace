import React from "react"
import { Link } from "gatsby"
import bem from "../../_utils/bem"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Image from "../../Atoms/Image/Image"
import Heading from "../../Atoms/Heading/Heading"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import HTMLEllipsis from "react-lines-ellipsis/lib/html"

const ProfileNodeReference = ({ node }) => {
  return (
    <Row className={bem("profile-teaser")}>
      {node.relationships.field_image.relationships.field_media_image
        .localFile && (
        <Col md={4}>
          <Image
            image={
              node.relationships.field_image.relationships.field_media_image
                .localFile
            }
            imageType={"fixed"}
            alt={node.relationships.field_image.field_media_image.alt}
          />
        </Col>
      )}

      <Col md={8}>
        {/* Name */}
        {node.title && (
          <Heading level={4}>
            <Link to={node.path.alias}>{node.title}</Link>
          </Heading>
        )}

        {/* Role */}
        {node.field_role && (
          <Heading level={5}>
            {node.field_role?.map((role, index) => (
              <div className={bem("profile-teaser", "role")} key={index}>
                {role}
              </div>
            ))}
          </Heading>
        )}

        {/* Email */}
        {node.field_email_address && (
          <div className={bem("profile-teaser", "email")}>
            <a href={`mailto:${node.field_email_address}`}>
              {node.field_email_address}
            </a>
          </div>
        )}

        {/* Phone */}
        {node.field_phone_number && (
          <div className={bem("profile-teaser", "phone")}>
            <a href={`tel:${node.field_phone_number}`}>
              {node.field_phone_number}
            </a>
          </div>
        )}

        <div className={bem("profile-teaser", "bio")}>
          {node.field_bio.summary ? (
            <CKEditorContent content={node.field_bio.summary} />
          ) : (
            <HTMLEllipsis
              unsafeHTML={node.field_bio.processed}
              maxLine="3"
              ellipsis="..."
              basedOn="words"
            />
          )}
        </div>
      </Col>
    </Row>
  )
}

export default ProfileNodeReference
