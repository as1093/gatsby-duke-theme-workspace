import React, { Fragment } from "react"
import { Link } from "gatsby"
import bem from "../../_utils/bem"
import Image from "../../Atoms/Image/Image"
import Heading from "../../Atoms/Heading/Heading"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import { BsDot } from "react-icons/bs"

const StoryNodeReference = ({ 
  node,
  block = 'reference-card',
  modifiers = 'story',
}) => {
  return (
    <section className={`${block} ${bem(block, modifiers)}`}>
      {node.relationships.field_featured_media && (
        <Image
          image={
            node.relationships.field_featured_media.relationships
              .field_media_image.localFile
          }
          imageType={"fluid"}
          alt={node.relationships.field_featured_media.field_media_image.alt}
        />
      )}

      <div className={bem(block, "content", [modifiers])}>
        <div className={bem(block, "meta")}>
          {/* Date and Authors should Eyebrow? */}

          {/* Date */}
          {node.field_date && (
            <span>{node.field_date}</span>
          )}

          {/* Authors, separated by a comma */}
          {node.relationships.field_author_reference ? (
            <Fragment>
              <span>
                <BsDot />
              </span>
              {node.relationships.field_author_reference?.map((author, index) => (
                <span key={index}>
                  {author.title ? author.title : ''}
                  {index < node.relationships.field_author_reference.length - 1 ? ", " : " "}
                </span>
              ))}
            </Fragment>
          ) : (
            ""
          )}
        </div>  

        {/* Title */}
        {node.title && (
          <Heading level={4}>
            <Link to={node.path.alias}>{node.title}</Link>
          </Heading>
        )}

        {/* Blurb */}
        {node.field_summary ? (
          <div>
            <CKEditorContent content={node.field_summary} />
          </div>
        ) : (
          ""
        )}
      </div>

    </section>
  )
}

export default StoryNodeReference
