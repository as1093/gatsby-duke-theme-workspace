import React from "react"
import { Link } from "gatsby"
import bem from "../../_utils/bem"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Image from "../../Atoms/Image/Image"
import Heading from "../../Atoms/Heading/Heading"

const ScholarsProfileNodeReference = ({ node }) => {
  return (
    <Row className={bem("scholars-profile-teaser")}>
      {node.duke_scholars_profile_thumbnail && (
        <Col md={4}>
          <Image
            image={
              node.duke_scholars_profile_thumbnail.thumbnail
            }
            alt={node.duke_scholars_profile_thumbnail.alt_text}
          />
        </Col>
      )}

      <Col md={8}>
        {/* Name */}
        {node.title && (
          <Heading level={4}>
            <Link to={node.duke_scholars_uri}>{node.title}</Link>
          </Heading>
        )}

        {/* Role */}
        {node.relationships.duke_scholars_profile_positions && (
          <Heading level={5}>
            {node.relationships.duke_scholars_profile_positions?.map((role, index) => {
              return (role.position_preferred ? (
                <div className={bem("profile-teaser", "role")} key={index}>
                  {role.title}
                </div>
              )
              : ''
              )
            } 
            )}
          </Heading>
        )}

        {/* Email */}
        {node.duke_scholars_profile_email && (
          <div className={bem("profile-teaser", "email")}>
            <a href={`mailto:${node.duke_scholars_profile_email}`}>
              {node.duke_scholars_profile_email}
            </a>
          </div>
        )}

        {/* Phone */}
        {node.duke_scholars_profile_phone && (
          <div className={bem("profile-teaser", "phone")}>
            <a href={`tel:${node.duke_scholars_profile_phone}`}>
              {node.duke_scholars_profile_phone}
            </a>
          </div>
        )}
      </Col>
    </Row>
  )
}

export default ScholarsProfileNodeReference
