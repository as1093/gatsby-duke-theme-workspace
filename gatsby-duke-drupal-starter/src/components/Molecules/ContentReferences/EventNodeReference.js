import React from "react"
import bem from "../../_utils/bem"
import EventCard from "../EventCard/EventCard"

const EventNodeReference = ({ node }) => {
  return (
    <section className={bem("event-teaser")}>
      <EventCard
        title={node.title}
        status={
          node.relationships.field_status
            ? node.relationships.field_status.name
            : ""
        }
        url={node.path.alias}
        date={node.field_event_date ? node.field_event_date.value : ""}
        location={node.field_location_text ? node.field_location_text : ""}
      />
    </section>
  )
}

export default EventNodeReference
