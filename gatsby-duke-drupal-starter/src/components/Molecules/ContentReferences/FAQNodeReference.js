import React from "react"
import bem from "../../_utils/bem"
import Accordion from "react-bootstrap/Accordion"
import Card from "react-bootstrap/Card"
import Button from "react-bootstrap/Button"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"

const FAQNodeReference = ({ node }) => {
  return (
    <section className={bem("faq-teaser")}>
      <Accordion defaultActiveKey="0">
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              {node.field_question ? node.field_question : ""}
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              {node.field_answer ? (
                <CKEditorContent content={node.field_answer.processed} />
              ) : (
                ""
              )}
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </section>
  )
}

export default FAQNodeReference
