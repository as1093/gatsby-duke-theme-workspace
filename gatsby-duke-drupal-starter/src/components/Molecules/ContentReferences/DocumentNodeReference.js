import React from "react"
import bem from "../../_utils/bem"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import Heading from "../../Atoms/Heading/Heading"

const DocumentNodeReference = ({ node }) => {
  return (
    <section className={bem("document-teaser")}>
      {/* Title */}
      {node.title && <Heading level={5}>{node.title}</Heading>}

      {/* Categories */}
      {node.relationships.field_categories && (
        <div className={bem("document-teaser", "category")}>
          {node.relationships.field_categories?.map((category, index) => (
            <span key={index}>{category.name}</span>
          ))}
        </div>
      )}

      {/* Description */}
      <div className={bem("document-teaser", "description")}>
        {node.body && <CKEditorContent content={node.body.processed} />}
      </div>

      {/* Link to document - Document Upload */}
      {node.relationships.field_document &&
        node.relationships.field_document.relationships && (
          <a
            href={
              node.relationships.field_document.relationships.field_document
                .relationships.field_document.localFile.publicURL
            }
          >
            {
              node.relationships.field_document.relationships.field_document
                .name
            }
          </a>
        )}

      {/* Link to document - Document URL */}
      {node.relationships.field_document &&
        node.relationships.field_document.field_url && (
          <a href={node.relationships.field_document.field_url.uri}>
            {node.title}
          </a>
        )}

      {/* Last Modified Date */}
      {node.field_last_modified_date && (
        <p className={bem("document-teaser", "date")}>
          <i>Revised {node.field_last_modified_date}</i>
        </p>
      )}
    </section>
  )
}

export default DocumentNodeReference
