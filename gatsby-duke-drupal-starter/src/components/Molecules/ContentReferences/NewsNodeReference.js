import React from "react"
import { Link } from "gatsby"
import bem from "../../_utils/bem"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import Image from "../../Atoms/Image/Image"
import Heading from "../../Atoms/Heading/Heading"
import HTMLEllipsis from "react-lines-ellipsis/lib/html"
import { BsDot } from "react-icons/bs"
import "./ContentReference.scss"

const NewsNodeReference = ({ 
  node,
  block = 'reference-card',
  modifiers = 'news',
}) => {
  return (
    <section className={`${block} ${bem(block, modifiers)}`}>
      {/* {node.relationships.field_featured_media.relationships.field_media_image
        .localFile && (
        <Image
          image={
            node.relationships.field_featured_media.relationships
              .field_media_image.localFile
          }
          imageType={"fluid"}
          alt={node.relationships.field_featured_media.field_media_image.alt}
        />
      )} */}

      {node.field_feed_image_url ? (
        <>
          {node.field_featured_media ? (
            <div className="news-featured-image">
              {node.field_featured_media.relationships.field_media_image !==
                null && (
                <Image
                  image={
                    node.field_featured_media &&
                    node.field_featured_media.relationships.field_media_image
                      .localFile
                  }
                  imageType={"fluid"}
                  alt={node.field_featured_media.relationships.field_media_image.alt}
                />
              )}
            </div>
          ) : (
            <div className="news-featured-image">
              <Image
                image={node.field_feed_image_url.uri}
                imageType={"fluid"}
                alt={node.field_feed_image_alt_text}
              />
            </div>
          )}
        </>
      ) : (
        <>
          {node.field_featured_media && (
            <div className="news-featured-image">
              {node.field_featured_media.relationships.field_media_image !==
                null && (
                <Image
                  image={
                    node.field_featured_media &&
                    node.field_featured_media.relationships.field_media_image
                      .localFile
                  }
                  imageType={"fluid"}
                  alt={node.field_featured_media.relationships.field_media_image.alt}
                />
              )}
            </div>
          )}
        </>
      )}

      <div className={bem(block, "content")}>
      
        <div className={bem(block, "meta")}>
          {/* Date and Source should be Eyebrow? */}
          {/* Date */}
          {node.field_date && (
            <span>{node.field_date}</span>
          )}

          {/* Source */}
          {node.field_news_source && (
            <span>
              <span><BsDot /></span>
              {node.field_news_source}
            </span>
          )}
        </div>

        {/* Title */}
        {node.field_title_override ? (
          <Heading level={4}>
            <Link to={node.path.alias}>{node.field_title_override}</Link>
          </Heading>
        ) : (
          <Heading level={4}>
            <Link to={node.path.alias}>{node.title}</Link>
          </Heading>
        )}

        {/* Blurb */}
        <div>
          {node.body.summary ? (
            <CKEditorContent content={node.body.summary} />
          ) : (
            <HTMLEllipsis
              unsafeHTML={node.body.processed}
              maxLine="3"
              ellipsis="..."
              basedOn="words"
            />
          )}
        </div>
      </div>  
    </section>
  )
}

export default NewsNodeReference
