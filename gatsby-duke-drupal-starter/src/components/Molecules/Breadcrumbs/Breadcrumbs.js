/**
 *  Renders breadcrumbs trail
 *  The separator is styled in CSS
 */
import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { FaHome } from "react-icons/fa"
import bem from "../../_utils/bem"
import { BreadcrumbsTrail } from "./BreadcrumbsWalker"

import "./Breadcrumbs.scss"

const blockName = "breadcrumb"

const CustomBreadcrumbs = ({ customParents, pageTitle }) => {
  let trail =[]

  // if customParent is defined, reconstruct the trail using them
  // otherwise, use the menuData
  if (customParents && customParents.length) {
    trail = [
      {
        title: 'Home',
        uri: '/'
      },
    ]

    // merge the homepage item and the parents array
    trail = trail.concat(customParents)

    // add the current page to the end of trail
    trail.push(
    {
      title: pageTitle,
      uri: ''
    })
  }
  else {
    trail = BreadcrumbsTrail()
  }

  if (!trail) return null

  // get the index of the last item to add markup for the current item
  const trailLastIndex = trail.length - 1

  let breadcrumbItems = trail.map((item, index) => {
    switch (index) {
      case 0:
        return (
          <li key={`breadcrumb_item${index}`} className="breadcrumb-item">
            <Link to={item.uri}>
              <FaHome />
            </Link>
          </li>
        )
      case trailLastIndex:
        return (
          <li
            key={`breadcrumb_item${index}`}
            className="breadcrumb-item active"
            aria-current="page"
          >
            {pageTitle}
          </li>
        )
      default:
        return (
          <li key={`breadcrumb_item${index}`} className="breadcrumb-item">
            <Link to={item.uri}>{item.title}</Link>
          </li>
        )
    }
  })

  return (
    <nav
      aria-label="breadcrumb"
      className={bem(blockName, "wrapper")}
      role="navigation"
    >
      <ol className="breadcrumb">{breadcrumbItems}</ol>
    </nav>
  )
}

CustomBreadcrumbs.propTypes = {
  customParents: PropTypes.array,
  pageTitle: PropTypes.string,
}

export default CustomBreadcrumbs
