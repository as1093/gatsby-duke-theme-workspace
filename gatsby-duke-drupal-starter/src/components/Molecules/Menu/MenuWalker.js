import React from "react"
import { Link } from "gatsby"
import PropTypes from "prop-types"
import { globalHistory } from "@reach/router"
import { useMenuData } from "../../../hooks/MenuData"

function createMenuHierarchy(menuData, menuName) {
  let tree = [],
    mappedArr = {},
    arrElem,
    mappedElem

  // First map the nodes of the array to an object -> create a hash table.
  for (let i = 0, len = menuData.length; i < len; i++) {
    arrElem = menuData[i].node
    if (arrElem.menu_name === menuName && arrElem.enabled === true) {
      mappedArr[arrElem.drupal_id] = arrElem
      if (
        arrElem.drupal_parent_menu_item != null &&
        arrElem.drupal_parent_menu_item.includes(arrElem.bundle)
      ) {
        let stripped_drupal_id = arrElem.drupal_parent_menu_item.replace(
          arrElem.bundle + ":",
          ""
        )
        mappedArr[
          arrElem.drupal_id
        ].drupal_parent_menu_item = stripped_drupal_id
      }
      mappedArr[arrElem.drupal_id]["children"] = []
    }
  }

  for (let id in mappedArr) {
    if (mappedArr.hasOwnProperty(id)) {
      mappedElem = mappedArr[id]
      // If the element is not at the root level, add it to its parent array of children.
      if (mappedElem.drupal_parent_menu_item) {
        mappedArr[mappedElem.drupal_parent_menu_item]["children"].push(
          mappedElem
        )
      }
      // If the element is at the root level, add it to first level elements array.
      else {
        tree.push(mappedElem)
      }
    }
  }
  return tree
}

function buildLink(link, classes="") {
  console.log(link)
  if (!link.external && link.link.uri_alias) {
    return (
      <Link activeClassName="active" className={`nav-link ${classes}`} to={link.link.uri_alias}>
        {link.title}
      </Link>
    )
  } else if (!link.external && link.link.uri.includes("internal:")) {
  console.log('hey', link.link.uri.replace("internal:", ""))
    return (
      <Link
        activeClassName="active" className={`nav-link ${classes}`}
        to={`/${link.link.uri.replace("internal:", "")}`}
      >
        {link.title}
      </Link>
    )
  } else {
    return (
      <a href='https://google.com' className={`external nav-link ${classes}`}>
        {link.title}
      </a>
    )
  }
}

function buildMenu(menuArray) {
  if (!menuArray) {
    return
  }
  let menu = []

  for (let item in menuArray) {
    if (menuArray[item].children.length !== 0) {
      menu.push(
        <li key={menuArray[item].drupal_id} className="nav-item has-dropdown" >
          {buildLink(menuArray[item],"dropdown-toggle")}
          <ul className="dropdown" aria-labelledby="navbarDropdown">{buildMenu(menuArray[item].children)}</ul>
        </li>
      )
    } else {
      menu.push(
        <li key={menuArray[item].drupal_id} className="nav-item">{buildLink(menuArray[item])}</li>
      )
    }
  }

  return menu
}

function generateMenu(pageSlug, menuLinks, menuName, startLevel) {
  let menu
  let currentPage = {
    path: pageSlug,
    drupal_id: 0,
    parent_id: 0,
  }
  let menuLinksArray = menuLinks

  // homepage
  if (currentPage.path === "/") {
    currentPage.path = null
  }

  for (let item of menuLinksArray) {
    if (item.node.link.uri_alias === currentPage.path) {
      currentPage.drupal_id = item.node.drupal_id
      currentPage.parent_id = item.node.drupal_parent_menu_item
      break
    }
  }

  function getParent(childID, nodeArray) {
    let parentID = 0

    for (let item of nodeArray) {
      if (
        item.node.drupal_id === childID &&
        item.node.drupal_parent_menu_item
      ) {
        if (item.node.drupal_parent_menu_item.includes("menu_link_content:")) {
          item.node.drupal_parent_menu_item = item.node.drupal_parent_menu_item.replace(
            "menu_link_content:",
            ""
          )
        }
        parentID = item.node.drupal_parent_menu_item
        break
      }
    }
    return parentID
  }

  // Find the top level parent
  function getTopParent(childID, nodeArray) {
    let topParentID = 0
    let nextParentID = getParent(childID, nodeArray)
    if (nextParentID === 0) {
      topParentID = childID
    } else {
      while (nextParentID !== 0) {
        topParentID = nextParentID
        nextParentID = getParent(nextParentID, nodeArray)
      }
    }
    return topParentID
  }

  let topParentID = getTopParent(currentPage.drupal_id, menuLinksArray)
  menu = createMenuHierarchy(menuLinks, menuName)

  if (startLevel === 1 && currentPage.path !== null) {
    let newMenu = menu
    menu.forEach(function(item, index) {
      if (item.drupal_id === topParentID) {
        newMenu = item.children
      }
    })
    menu = newMenu
  }

  menu = buildMenu(menu)
  return menu
}

// class Menu extends Component {
const Menu = ({ storyItems, menuName, startLevel }) => {
  const menuDataDrupal = useMenuData()
  const menuData = storyItems ? storyItems : menuDataDrupal
  const { location } = globalHistory

  return (
    <>
      <ul>{generateMenu(location.pathname, menuData, menuName, startLevel)}</ul>
    </>
  )
}

Menu.propTypes = {
  menuData: PropTypes.array,
  menuName: PropTypes.string,
  startLevel: PropTypes.number,
  storyItems: PropTypes.array,
}

Menu.defaultProps = {
  menuName: `main`,
  startLevel: 0,
}

export default Menu
