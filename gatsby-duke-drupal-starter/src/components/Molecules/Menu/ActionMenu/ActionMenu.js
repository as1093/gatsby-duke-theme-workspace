import React from "react"
import PropTypes from "prop-types"
import Menu from "../MenuWalker"
import bem from "../../../_utils/bem"
import "./Action-menu.scss"

const ActionMenu = ({ block = "actionmenu", element = "", modifiers }) => {
  return (
    <nav className={`nav justify-content-end ${bem(block, element, modifiers)}`} aria-labelledby="actionmenulabel">
      <h2 id="actionmenulabel" className="sr-only">Action navigation</h2>
      <Menu
        menuName={`action`}
        startLevel={0}
      />
    </nav>
  )
}

ActionMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default ActionMenu
