import React from "react"
import PropTypes from "prop-types"
import Menu from "../MenuWalker"
import bem from "../../../_utils/bem"

import "./Footer-menu.scss"

const FooterMenu = ({ block = "footermenu", element = "", modifiers }) => {
  return (
    <nav
      className={`nav ${bem(block, element, modifiers)}`}
      aria-labelledby="footermenulabel"
    >
      <h2 id="footermenulabel" className="sr-only">
        Footer navigation
      </h2>
      <Menu menuName={`footer`} startLevel={0} />
    </nav>
  )
}

FooterMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default FooterMenu
