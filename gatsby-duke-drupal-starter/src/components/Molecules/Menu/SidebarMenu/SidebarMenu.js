import React from "react"
import PropTypes from "prop-types"
import Menu from "../MenuWalker"
import bem from "../../../_utils/bem"
import "./Sidebar-menu.scss"

const SidebarMenu = ({ block = "sidebarmenu", element = "", modifiers }) => {
  return (
    <nav className={`nav ${bem(block, element, modifiers)}`} aria-labelledby="sidemenulabel">
      <h2 id="sidemenulabel" className="visuallyhidden">Sub navigation</h2>
      <Menu menuName={`main`} startLevel={1} /> 
    </nav>  
  )
}

SidebarMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default SidebarMenu
