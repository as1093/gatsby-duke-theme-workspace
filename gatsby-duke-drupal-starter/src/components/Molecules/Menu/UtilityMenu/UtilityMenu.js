import React from "react"
import PropTypes from "prop-types"
import Menu from "../MenuWalker"
import bem from "../../../_utils/bem"
import "./Utility-menu.scss"

const UtilityMenu = ({ block = "utilitymenu", element = "", modifiers }) => {
  return (
    <nav className={`nav justify-content-end ${bem(block, element, modifiers)}`} aria-labelledby="utilitymenulabel">
      <h2 id="utilitymenulabel" className="sr-only">Utility navigation</h2>
      <Menu
        menuName={`utility`}
        startLevel={0}
      />
    </nav>
  )
}

UtilityMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default UtilityMenu
