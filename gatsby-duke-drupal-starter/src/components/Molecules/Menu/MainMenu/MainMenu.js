import React from 'react';
import PropTypes from 'prop-types';
import MegaMenu from '../MegaMenuWalker';
import bem from '../../../_utils/bem';
import './main-menu.scss'

const MainMenu = ({
  block = 'mainmenu',
  element = '',
  modifiers,
}) => {

  return (
    <nav className={`navbar-nav navbar-expand-lg ${bem(block, element, modifiers)}`}>
      {/* <a class="navbar-brand" href="#">Navbar</a> */}
      {/* <button classname="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button> */}
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <MegaMenu menuName={`main`} startLevel={0} />
      </div>
    </nav>  
  );
};

MainMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
};

export default MainMenu;