import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import PropTypes from "prop-types"
import DrupalLink from "../../Atoms/DrupalLink/DrupalLink"
import Heading from "../../Atoms/Heading/Heading"
import bem from "../../_utils/bem" 
import FooterMenu from "../../Molecules/Menu/FooterMenu/FooterMenu"
import Copyright from "../../Atoms/Copyright/Copyright"
import "./Footer.scss"

const Footer = ({ 
  footerContent,
  block = 'footer',
  }) => {

  return (
    <footer>
      <Container>
        <Row>
          <Col>
            <h3 className="site_name">
              <DrupalLink url="/">{footerContent.field_site_name_prefix} {footerContent.field_site_name}</DrupalLink>
            </h3>
          </Col>
        </Row>
        <Row>
          <Col>
            <p className={bem(block, 'address', [])}>
              {footerContent.field_site_address?.map(function(item, index) {
                return <span key={`address_${index}`}>{ item }</span>
                }
              )}
            </p>  

            {footerContent.field_site_email !== null && (
              <p className={bem(block, 'email', [])}><a href={`mailto:${footerContent.field_site_email}`}>{footerContent.field_site_email}</a></p>
            )}

            {footerContent.field_site_phone_number !== null && (
              <p className={bem(block, 'phone', [])}><a href={`tel:${footerContent.field_site_phone_number}`}>{footerContent.field_site_phone_number}</a></p>
            )}  

            {footerContent.field_site_additional_info !== null && (
              <p>{footerContent.field_site_additional_info}</p>
            )}                        
          </Col>
          <Col className="col-6">
            <FooterMenu />
          </Col>
          <Col>
            {/* Primary CTA */}
            {footerContent.field_site_primary_cta !== null && 
              <DrupalLink className={bem(block, 'cta-primary button', [])} url={footerContent.field_site_primary_cta.uri} >{footerContent.field_site_primary_cta.title}</DrupalLink>
            }

            {/* Secondary CTA */}
            {footerContent.field_site_cta_heading !== null && 
              <Heading level={4} className={bem(block, 'cta-heading', [])}>{footerContent.field_site_cta_heading}</Heading>
            } 

            {footerContent.field_site_cta_description !== null && 
              <p>{footerContent.field_site_cta_description}</p>
            } 

            {footerContent.field_site_secondary_cta !== null && 
              <DrupalLink url={footerContent.field_site_secondary_cta.uri} className={bem(block, 'cta-secondary button', [])}>{footerContent.field_site_secondary_cta.title}</DrupalLink>
            }            
          </Col>        
        </Row>
        <Row>
          <Col>
            {/* Social icon-Drupallinks 
              * @TODO - refactor using Icon atom
            */}
            <div className={`${bem(block, 'social-icons-wrapper', [])} text-center`}>
              {footerContent.field_site_facebook !== null && 
                <DrupalLink url={footerContent.field_site_facebook.uri}>Facebook</DrupalLink>
              }
              {footerContent.field_site_twitter !== null && 
                <DrupalLink url={footerContent.field_site_twitter.uri}>Twitter</DrupalLink>
              }
              {footerContent.field_site_instagram !== null && 
                <DrupalLink url={footerContent.field_site_instagram.uri}>Instagram</DrupalLink>
              }
              {footerContent.field_site_youtube !== null && 
                <DrupalLink url={footerContent.field_site_youtube.uri}>YouTube</DrupalLink>
              }
              {footerContent.field_site_linkedin !== null && 
                <DrupalLink url={footerContent.field_site_linkedin.uri}>LinkedIn</DrupalLink>
              }    
            </div>                                              
          </Col>
        </Row>
      </Container>
        <hr className="separator" />
      <Container>
        <Row>
          <Col>
            <Copyright />
          </Col>
          <Col>
              <div className={`${bem(block, 'copyright-Drupallinks-wrapper', [])} text-right`}>
                <DrupalLink url='https://accessibility.duke.edu'>Web Accessibility</DrupalLink>
                <DrupalLink url='https://oarc.duke.edu/privacy'>Privacy Policy</DrupalLink>
                <DrupalLink url='https://oie.duke.edu/we-can-help/complaints-and-concerns/sexual-misconduct-title-ix'>Title IX</DrupalLink>
              </div>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};
Footer.propTypes = {
  siteTitle: PropTypes.string,
  footerContent: PropTypes.object
};

export default Footer;
