import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import HeaderSearch from "../Search/Search"
import MainMenu from "../../Molecules/Menu/MainMenu/MainMenu"
import UtilityMenu from "../../Molecules/Menu/UtilityMenu/UtilityMenu"
import ActionMenu from "../../Molecules/Menu/ActionMenu/ActionMenu"
// import AnnouncementBlock from "../../Molecules/Announcement/Announcement"
import DukeLogoWhite from '../../../images/DukeLogoWhite.svg'

import "./Hero.scss"

const Header = ({ 
  siteTitle,
  siteTitlePrefix,
  }) => {
  
  const block = 'header';
  return (
    <>
    <header>
      {/* <AnnouncementBlock /> */}
      <div className="utility_section">
        <HeaderSearch />

        <div className="utility_menu_wrapper">
            <UtilityMenu />
            <ActionMenu />
        </div>
      </div>
      <div className="header_wrapper">
        
          {siteTitle && (
            <a className="site_branding" href="/">
              <div className="duke-logo p-2">
                <img className={bem(block, 'logo', [])} src={DukeLogoWhite} alt="Duke University" />

                <div className={bem(block, 'sitename', [])}>{siteTitlePrefix !== null && siteTitlePrefix}</div>

                <div className={bem(block, 'sitename', [])}>{siteTitle !== null && siteTitle}</div>
              </div>
            </a>  
          )}
        <div className="header_menu_wrapper">
          <MainMenu />
        </div>
      </div>
    </header>
    </>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

export default Header
