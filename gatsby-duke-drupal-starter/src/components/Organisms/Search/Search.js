import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import "./Search.scss"

const HeaderSearch = ({
  children,
  block = "headersearch",
  element = "",
  modifiers,
}) => {
  return (
    <div className={bem(block, element, modifiers)}>
      <p>Search field will go here.</p>
      {children}
    </div>
  )
}

HeaderSearch.propTypes = {
  children: PropTypes.node,
}

export default HeaderSearch
