import React, { Component } from "react"

class AlertBar extends Component {
  componentDidMount() {
    try {
      const postscribe = require('postscribe')
      postscribe('#alert-bar', '<script src="https://alertbar.oit.duke.edu/alert.html"></script>');
    } catch(e) {
      console.log('Error')
    }
    // document.getElementById('alert-bar').innerHTML = '<script src="https://alertbar.oit.duke.edu/alert.html"></script>'
  }

  render() {
    return (
      <>
        <div id="alert-bar"></div>
      </>
    )
  }
}

export default AlertBar