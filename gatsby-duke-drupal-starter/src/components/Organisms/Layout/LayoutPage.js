import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import bem from "../../_utils/bem"
import PageTitle from "../../Atoms/PageTitle/PageTitle"
import CustomBreadcrumbs from "../../Molecules/Breadcrumbs/Breadcrumbs"
import SidebarMenu from "../../Molecules/Menu/SidebarMenu/SidebarMenu"

const LayoutPage = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  mainContentModifiers = [],
  hasSidebar,
  title,
  hero,
}) => {
  if (hasSidebar) {
    mainModifiers.push("with-sidebar")
    mainContentModifiers.push("with-sidebar")
  }
  return (
    <LayoutBase pageTitle={title}>

      {hero ? hero : ''}

      <Container>

        <PageTitle>{title}</PageTitle>
        {hasSidebar && (
          <aside className="main-sidebar">
            <SidebarMenu />
          </aside>
        )}
        <CustomBreadcrumbs pageTitle={title} />
        <main className={bem(mainBlock, "", mainContentModifiers)}>
          {children}
        </main>
      </Container>
    </LayoutBase>
  )
}

LayoutPage.propTypes = {
  title: PropTypes.string,
  hero: PropTypes.object,
  children: PropTypes.node.isRequired,
}

export default LayoutPage
