import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import CustomBreadcrumbs from "../../Molecules/Breadcrumbs/Breadcrumbs"
import bem from "../../_utils/bem"

const LayoutBlogPost = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  layoutModifiers = [],
  mainContentModifiers = [],
  title,
}) => {
  return (
    <LayoutBase pageTitle={title}>
      <Container>
        <CustomBreadcrumbs pageTitle={title} customParents={[
            {
              title: 'Blog', 
              uri: '/blog'
            }
          ]}    
        />
        <main className={bem(mainBlock, "", mainContentModifiers)}>
          {children}
        </main>
      </Container>
    </LayoutBase>
  )
}

LayoutBlogPost.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
}

export default LayoutBlogPost
