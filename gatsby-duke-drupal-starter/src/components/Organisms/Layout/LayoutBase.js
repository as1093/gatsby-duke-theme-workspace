/**
 * Base layout - includes header, base wrapper, footer
 */

import React from "react"
import PropTypes from "prop-types"
// import { ErrorBoundary }  from "react-error-boundary"
import Helmet from "react-helmet"
import Header from "../Header/Header.js"
import Footer from "../Footer/Footer.js"
import { useSiteInfoData } from "../../../hooks/SiteInfoData"

const LayoutBase = ({ children, pageTitle }) => {
  const siteDataArray = useSiteInfoData()
  const siteData = siteDataArray.length !== 0 ? siteDataArray[0].node : null

  return (
    <>
      <Helmet
        title={pageTitle}
        meta={[{ name: "description", content: pageTitle }]}
      >
        <link
          rel="stylesheet"
          href="https://alertbar.oit.duke.edu/sites/all/themes/blackwell/css/alert.css"
          type="text/css"
          media="Screen"
        />
      </Helmet>
      <Header
        siteTitle={siteData.field_site_name ? siteData.field_site_name : ""}
        siteTitlePrefix={
          siteData.field_site_name_prefix ? siteData.field_site_name_prefix : ""
        }
      />
      {children}
      <Footer footerContent={siteData ? siteData : ""} />
    </>
  )
}

LayoutBase.propTypes = {
  children: PropTypes.node.isRequired,
  pageTitle: PropTypes.string,
}

export default LayoutBase
