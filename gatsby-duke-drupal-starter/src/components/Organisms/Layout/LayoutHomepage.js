import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import bem from "../../_utils/bem"

const LayoutHomepage = ({
  children,
  mainBlock = "main-content",
  mainContentModifiers = [],
  title,
  hero,
}) => {
  return (
    <LayoutBase pageTitle={title}>

      {hero ? hero : ''}

      <Container>

        {/* <PageTitle>{title}</PageTitle>
        <CustomBreadcrumbs pageTitle={title} /> */}
        <main className={bem(mainBlock, "", mainContentModifiers)}>
          {children}
        </main>
      </Container>
    </LayoutBase>
  )
}

LayoutHomepage.propTypes = {
  title: PropTypes.string,
  hero: PropTypes.object,
  children: PropTypes.node.isRequired,
}

export default LayoutHomepage
