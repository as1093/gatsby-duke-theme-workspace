import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "../Layout/LayoutBase"
import Container from "react-bootstrap/Container"
import CustomBreadcrumbs from "../../Molecules/Breadcrumbs/Breadcrumbs"
import bem from "../../_utils/bem"

const LayoutStory = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  layoutModifiers = [],
  mainContentModifiers = [],
  title,
}) => {
  return (
    <LayoutBase pageTitle={title}>
      <Container>
        <CustomBreadcrumbs pageTitle={title} customParents={[
            {
              title: 'Stories', 
              uri: '/stories'
            }
          ]}    
        />
        <main className={bem(mainBlock, "", mainContentModifiers)}>
          {children}
        </main>
      </Container>
    </LayoutBase>
  )
}

LayoutStory.propTypes = {
  children: PropTypes.node.isRequired,
}

export default LayoutStory
