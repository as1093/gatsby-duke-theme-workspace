import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import PageTitle from "../../Atoms/PageTitle/PageTitle"
import CustomBreadcrumbs from "../../Molecules/Breadcrumbs/Breadcrumbs"
import bem from "../../_utils/bem"

const LayoutProfile = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  layoutModifiers = [],
  mainContentModifiers = [],
  title,
}) => {
  return (
    <LayoutBase>
      <Container>
        <PageTitle>{title}</PageTitle>
        
        <CustomBreadcrumbs pageTitle={title} customParents={[
            {
              title: 'Staff', 
              uri: '/staff'
            }            
          ]}    
        />

        <main className={bem(mainBlock, "", mainContentModifiers)}>
          {children}
        </main>
      </Container>
    </LayoutBase>
  )
}

LayoutProfile.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
}

export default LayoutProfile
