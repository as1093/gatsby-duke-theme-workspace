import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import bem from "../../_utils/bem"
import CustomBreadcrumbs from "../../Molecules/Breadcrumbs/Breadcrumbs"

const LayoutEvent = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  mainContentModifiers = [],
  hasSidebar,
  title,
}) => {
  if (hasSidebar) {
    mainModifiers.push("with-sidebar")
    mainContentModifiers.push("with-sidebar")
  }
  return (
    <LayoutBase>
      <Container>
        {/* {hasSidebar && (
          <aside className="main-sidebar">
            <p>Menu will go here</p>
          </aside>
        )} */}
        <CustomBreadcrumbs pageTitle={title} customParents={[
            {
              title: 'Event', 
              uri: '/event'
            }
          ]}    
        />

        <main className={bem(mainBlock, "", mainContentModifiers)}>
          {children}
        </main>
      </Container>
    </LayoutBase>
  )
}

LayoutEvent.propTypes = {
  title: PropTypes.string,
  hero: PropTypes.object,
  children: PropTypes.node.isRequired,
}

export default LayoutEvent
