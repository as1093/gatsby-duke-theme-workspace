import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "../Layout/LayoutBase"
import Container from "react-bootstrap/Container"
import CustomBreadcrumbs from "../../Molecules/Breadcrumbs/Breadcrumbs"
import bem from "../../_utils/bem"

const LayoutNews = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  layoutModifiers = [],
  mainContentModifiers = [],
  hasSidebar,
  title,
}) => {
  if (hasSidebar) {
    mainModifiers.push("with-sidebar")
    mainContentModifiers.push("with-sidebar")
  }

  return (
    <LayoutBase>
      <Container>
        {hasSidebar && (
          <aside className="main-sidebar">
            <p>Menu will go here</p>
          </aside>
        )}
        <CustomBreadcrumbs pageTitle={title} customParents={[
            {
              title: 'News', 
              uri: '/news'
            }
          ]}    
        />

        <main className={bem(mainBlock, "", mainContentModifiers)}>
          {children}
        </main>
      </Container>
    </LayoutBase>
  )
}

LayoutNews.propTypes = {
  children: PropTypes.node.isRequired,
}

export default LayoutNews
