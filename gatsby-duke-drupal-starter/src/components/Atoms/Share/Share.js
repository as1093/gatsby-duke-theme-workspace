/**
 *  Renders the social icons for sharing on
 *  Email, Facebook, Twitter, LinkedIn
 */
import React from "react"
import PropTypes from "prop-types"
import { globalHistory } from "@reach/router"
import Heading from "../Heading/Heading"
import bem from "../../_utils/bem"
import {
  EmailShareButton,
  EmailIcon,
  FacebookShareButton,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon,
} from "react-share"
import "./Share.scss"

const Share = ({ 
  modifiers, 
  shareTitle,
  pageTitle,
  pageSummary
}) => {
  // construct the full URL of the current page
  const url = `${globalHistory.location.origin}${globalHistory.location.pathname}`

  return (
    <div className={bem("share", "wrapper", modifiers)}>
      <Heading level={3} modifiers={['label']}>{shareTitle}</Heading>

      <EmailShareButton url={url} subject={pageTitle} body={pageSummary}>
        <EmailIcon size={50} round={true} />
      </EmailShareButton>

      <FacebookShareButton url={url} quote={pageTitle}>
        <FacebookIcon size={50} round={true} />
      </FacebookShareButton>

      <TwitterShareButton url={url} title={pageTitle}>
        <TwitterIcon size={50} round={true} />
      </TwitterShareButton>

      <LinkedinShareButton url={url} title={pageTitle} summary={pageSummary}>
        <LinkedinIcon size={50} round={true} />
      </LinkedinShareButton>
    </div>
  )
}
Share.propTypes = {
  pageTitle: PropTypes.string,
  pageSummary:  PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
  shareTitle: PropTypes.string,
}

export default Share
