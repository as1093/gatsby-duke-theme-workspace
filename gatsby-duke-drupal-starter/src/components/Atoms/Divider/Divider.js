import React from "react";
import bem from "../../_utils/bem"
import PropTypes from "prop-types"
import "./Divider.scss"

const Divider = ({
  modifiers
}) => {
  const Divider = `hr`
  return <Divider className={bem("divider","", modifiers)} />
}

Divider.propTypes = {
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Divider