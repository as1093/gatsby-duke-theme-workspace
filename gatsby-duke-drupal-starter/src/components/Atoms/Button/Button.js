/**
 * Exports a button component.
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import DrupalLink from "../../Atoms/DrupalLink/DrupalLink"
import { MdKeyboardBackspace } from "react-icons/md"
import "./Button.scss"

const Button = ({
  link,
  block = "button",
  element = "",
  modifiers,
  children,
  showIcon = "0",
}) => {
  return (
    <DrupalLink url={link} className={bem(block, element, modifiers)}>
      <span className={bem(block, "text", modifiers)}>{children}</span>
      {showIcon ? <MdKeyboardBackspace className={bem(block, "icon")} /> : ""}
    </DrupalLink>
  )
}

Button.propTypes = {
  link: PropTypes.string,
  showIcon: PropTypes.bool,
  children: PropTypes.node,
  onClick: PropTypes.func,
}

Button.defaultProps = {
  children: null,
}

export default Button
