import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Moment from 'react-moment'

const Time = ({
  startTime,
  endTime,
  block = 'time',
  element,
}) => {
  return (
    <div className={bem(block, element)}>
      {startTime && (
        <>
          {endTime ? (
            <p className={bem(block, "time")}>
              {/* start time */}
              <Moment format="h:mm a">
                {startTime} 
              </Moment>
              <span>&nbsp;-&nbsp;</span>
              {/* end time */}
              <Moment format="h:mm a">
                {endTime} 
              </Moment>
            </p>
          ) : (
            <p className={bem(block, "time")}>
              <Moment format="h:mm a">
                {startTime} 
              </Moment>
            </p>
          )}
        </>
      )}
    </div>
  )
}

Time.propTypes = {
  startTime: PropTypes.string,
  endTime: PropTypes.string,
}

export default Time