import React from "react"
import PropTypes from "prop-types"
import Img from "gatsby-image"
import withModifiers from "../../_utils/withModifiers"
import "./Image.scss"

const Image = ({
  modifiers,
  image,
  imageType = "fluid",
  crop = "CENTER",
  alt = "",
}) => {
  let imageRender = ""

  if (image.childImageSharp) {
    if (imageType === "fluid") {
      imageRender = (
        <Img
          fluid={
            image.childImageSharp[`${crop}`]
              ? image.childImageSharp[`${crop}`]
              : image.childImageSharp.fluid
          }
          alt={alt}
          className={withModifiers("image")(modifiers)}
        />
      )
    }

    if (imageType === "fixed") {
      imageRender = (
        <Img
          fixed={
            image.childImageSharp[`${crop}`]
              ? image.childImageSharp[`${crop}`]
              : image.childImageSharp.fixed
          }
          alt={alt}
          className={withModifiers("image")(modifiers)}
        />
      )
    }
  } else {
    // static image, svg, or gif
    imageRender = (
      <img
        src={image.publicURL ? image.publicURL : image}
        alt={alt}
        className={withModifiers("image")(modifiers)}
      />
    )
  }

  return <>{imageRender}</>
}

export const imageTypeProps = PropTypes.shape({
  src: PropTypes.string,
  srcset: PropTypes.string,
  sizes: PropTypes.string,
})

Image.propTypes = {
  modifiers: PropTypes.arrayOf(PropTypes.string),
  image: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
  crop: PropTypes.string,
  imageType: PropTypes.string,
  alt: PropTypes.string,
}

export default Image
