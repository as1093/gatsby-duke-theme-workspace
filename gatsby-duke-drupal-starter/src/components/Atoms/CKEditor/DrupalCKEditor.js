/**
 *  Renders content coming from CKEditor
 */
import React from "react"
import PropTypes from "prop-types"
import DrupalImage from "../CKEditor/DrupalImage"
import DrupalDocument from "../CKEditor/DrupalDocument"
import ReactHtmlParser from "react-html-parser"
import ReactPlayer from "react-player/lazy"
import "./CKEditor.scss"

const CKEditorContent = ( props ) => {

  let textarea_content_elements = new ReactHtmlParser(props.content, {
    transform: function transform(node) {
      // search the body content for all article tags with an attribute data-media-source
      if (node.type === 'tag' && node.name === 'article' && node.attribs["data-media-source"]) {

        // Media - images
        if (node.attribs["data-media-source"] === 'image') {
          let fid = parseInt(node.attribs["data-media-source-value"]);
          return <DrupalImage key={fid} fid={fid} />
        } 

        // Media - documents
        if (node.attribs["data-media-source"] === 'document') {
          let fid = parseInt(node.attribs["data-media-source-value"]);
          return <DrupalDocument key={fid} fid={fid} />
        } 

        // Media - local video embed - not implementing
        // if (node.attribs["data-media-source"] === 'video_file') {
        //   let fid = parseInt(node.attribs["data-media-source-value"]);
        //   return <DrupalVideoFile key={fid} fid={fid} />
        // }   

        // Media - video embed
        if (node.attribs["data-media-source"] === 'video') {
          let fid = node.attribs["data-media-source-value"];
          const { host } = new URL(fid)
          const providerWarpwire = [
            "warpwire",
            "warpwire.duke.edu",
          ].includes(host)
          if (providerWarpwire) {
            return (
              <>
                <div className="video video-responsive">
                  <iframe id="wwvideo" title="Warpwire embed" data-ww-id="apiTestFrame" src={fid} frameBorder="0" scrolling="0" allowFullScreen></iframe>
                </div>
              </>
            )
          }
          else {
            return (
              <>
                <div className="video video-responsive">
                  <ReactPlayer url={fid} controls={true} />
                </div>
              </>
            )
          }   
        } 

        // Media - audio embed
        if (node.attribs["data-media-source"] === 'audio') {
          let fid = node.attribs["data-media-source-value"];
          return (
            <>
            <div className="audio audio-responsive">
              <ReactPlayer url={fid} controls={true} />
            </div>
            </>
          )
        } 

      }
      return undefined;
    }
  });

  return (
    <>
      {textarea_content_elements}
    </>
  )
}

CKEditorContent.propTypes = {
  content: PropTypes.string,
}

export default CKEditorContent
