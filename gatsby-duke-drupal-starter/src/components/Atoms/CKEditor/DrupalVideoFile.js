/**
 * Renders local video file embeds from CKEditor
 * @todo Add captions file (.vvt) 
 */
import React, { useMemo } from "react"
import { graphql, useStaticQuery } from "gatsby"

const DrupalVideoFile = ({ fid }) => {
  const data = useStaticQuery(graphql`
    query {
      allFileFile {
        edges {
          node {
            drupal_internal__fid
            localFile {
              publicURL
            }
          }
        }
      }  
    }
  `)

  const match = useMemo(() => (
    data.allFileFile.edges.find(({ node }) => fid === node.drupal_internal__fid)
  ), [ data, fid ])

  return (
    <video controls preload="metadata">
      <source src={match.node.localFile.publicURL} type="video/mp4" />
      <track src="captions_en.vtt" kind="captions" srcLang="en" label="english_captions" />
    </video>
  )
}

export default DrupalVideoFile
