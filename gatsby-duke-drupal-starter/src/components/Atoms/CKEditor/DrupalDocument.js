/**
 *  Renders document embeds from CKEditor
 */
import React, { useMemo } from "react"
import { graphql, useStaticQuery } from "gatsby"

const DrupalDocument = ({ fid }) => {
  const dataDocs = useStaticQuery(graphql`
    query {
      allFileFile {
        edges {
          node {
            drupal_internal__fid
            filename
            localFile {
              publicURL
            }
          }
        }
      }  
    }
  `)

  const match = useMemo(() => (
    dataDocs.allFileFile.edges.find(({ node }) => fid === node.drupal_internal__fid)
  ), [ dataDocs, fid ])

  return <a href={match.node.localFile.publicURL}>{match.node.filename}</a>
}

export default DrupalDocument
