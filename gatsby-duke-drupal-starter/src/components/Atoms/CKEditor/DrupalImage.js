/**
 *  Renders image embeds from CKEditor
 */
import React, { useMemo } from "react"
import { graphql, useStaticQuery } from "gatsby"
import Image from "../Image/Image"

const DrupalImg = ({ fid }) => {
  const dataImg = useStaticQuery(graphql`
    query {
      allFileFile {
        edges {
          node {
            drupal_internal__fid
            localFile {
              publicURL
              childImageSharp {
                fluid(maxWidth: 1025) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            relationships {
              media__image {
                field_media_image {
                  alt
                }
              }
            }
          }
        }
      }
    }
  `)

  const match = useMemo(
    () =>
      dataImg.allFileFile.edges.find(
        ({ node }) => fid === node.drupal_internal__fid
      ),
    [dataImg, fid]
  )

  return (
    <Image
      image={match.node.localFile}
      imageType={"fluid"}
      alt={match.node.relationships.media__image[0].field_media_image.alt}
    />
  )
}

export default DrupalImg
