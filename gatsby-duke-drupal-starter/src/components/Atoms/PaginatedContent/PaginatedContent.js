import React, { Fragment } from "react"
import Pagination from "./Pagination.js"
import PropTypes from "prop-types"
import Divider from "../Divider/Divider.js"

class PaginatedContent extends React.Component {
  constructor(props) {
    super(props)
    const items = props.items
    const itemsPerPage = props.itemsPerPage ? props.itemsPerPage : 10
    this.state = {
      items: items,
      itemsPerPage: itemsPerPage,
      pageOfItems: [],
    }

    this.onChangePage = this.onChangePage.bind(this)
  }

  onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems })
  }

  render() {
    return (
      <>
        {this.state.pageOfItems.map((item, index) => (
          <Fragment key={index}>{item}</Fragment>
        ))}
        <Divider modifiers={['long']} />
        <Pagination
          items={this.state.items}
          pageSize={this.state.itemsPerPage}
          onChangePage={this.onChangePage}
        />
      </>
    )
  }
}

PaginatedContent.propTypes = {
  items: PropTypes.array.isRequired,
  itemsPerPage: PropTypes.number,
}
export default PaginatedContent
