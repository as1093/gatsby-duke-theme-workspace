import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Moment from 'react-moment'

const DateProcessor = ({
  startDate,
  endDate,
  block = 'date',
  element,
  cardView,
}) => {
  return (
    <div className={bem(block, element)}>
      {startDate && (
        <>
          {endDate ? (
            <>
              {cardView ? (
                <>
                  {/* start date */}
                  <div className={bem(block, "date-cardview")}>
                    <Moment format="MMM" className={bem(block, "date-cardview-month")}>
                      {startDate} 
                    </Moment>
                    <Moment format="DD" className={bem(block, "date-cardview-day")}>
                      {startDate} 
                    </Moment>
                    <span>&nbsp;-&nbsp;</span>
                    {/* end date */}
                    <Moment format="MMM" className={bem(block, "date-cardview-month")}>
                      {endDate} 
                    </Moment>
                    <Moment format="DD" className={bem(block, "date-cardview-day")}>
                      {endDate} 
                    </Moment>
                  </div>
                </>
              ) : (
                <>
                  <div className={bem(block, "date-lineview")}>
                    {/* start date */}
                    <Moment format="MMMM" className={bem(block, "date-lineview-month")}>
                      {startDate} 
                    </Moment>
                    <span>&nbsp;</span>
                    <Moment format="DD" className={bem(block, "date-lineview-day")}>
                      {startDate} 
                    </Moment>
                    <span>,&nbsp;</span>
                    <Moment format="YYYY" className={bem(block, "date-lineview-year")}>
                      {startDate}
                    </Moment>
                    <span>&nbsp;-&nbsp;</span>
                    {/* end date */}
                    <Moment format="MMMM" className={bem(block, "date-lineview-month")}>
                      {endDate} 
                    </Moment>
                    <span>&nbsp;</span>
                    <Moment format="DD" className={bem(block, "date-lineview-day")}>
                      {endDate} 
                    </Moment>
                    <span>,&nbsp;</span>
                    <Moment format="YYYY" className={bem(block, "date-lineview-year")}>
                      {endDate}
                    </Moment>
                  </div>
                </>
              )}
            </>
          ) : (
            <>
              {cardView ? (
                <>
                  <div className={bem(block, "date-cardview")}>
                    <Moment format="MMM" className={bem(block, "date-cardview-month")}>
                      {startDate} 
                    </Moment>
                    <Moment format="DD" className={bem(block, "date-cardview-day")}>
                      {startDate} 
                    </Moment>
                  </div>
                </>
              ) : (
                <>
                  <div className={bem(block, "date-lineview")}>
                    <Moment format="MMMM" className={bem(block, "date-lineview-month")}>
                      {startDate} 
                    </Moment>
                    <span>&nbsp;</span>
                    <Moment format="DD" className={bem(block, "date-lineview-day")}>
                      {startDate} 
                    </Moment>
                    <span>,&nbsp;</span>
                    <Moment format="YYYY" className={bem(block, "date-lineview-year")}>
                      {startDate}
                    </Moment>
                  </div>
                </>
              )}
            </>
          )}
        </>
      )}
    </div>
  )
}

DateProcessor.propTypes = {
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  cardView: PropTypes.bool,
}

export default DateProcessor