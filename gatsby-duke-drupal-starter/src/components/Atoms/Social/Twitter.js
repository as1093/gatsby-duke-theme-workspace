import React, { Fragment, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

var callbacks = [];

const addScript = (src, cb) => {
  if (callbacks.length === 0) {
    callbacks.push(cb)
    var s = document.createElement('script')
    s.setAttribute('src', src)
    s.onload = () => callbacks.forEach((cb) => cb())
    document.body.appendChild(s)
  } else {
    callbacks.push(cb)
  }
}

const twitterRegexp = new RegExp(/https:\/\/twitter.com+/i);
const tweetRegexp = new RegExp(/https:\/\/twitter\.com\/[A-Za-z0-9-_]*\/status\/[0-9]+/i);

const isTwitterLink = url => {
  return tweetRegexp.test(url) || twitterRegexp.test(url)
}

const singleTweet = url => {
  var postRegexp = new RegExp(/twitter\.com\/.*\/status(?:es)?\/([^]+)+/i);
  var match = url.match(postRegexp);
  if (match) {
    return match[1];
  }
  else return null
}

const Twitter = ({ url, parentClass }) => {
    // const videoId = encodeURIComponent(id);    
    const tweetRef = useRef();

    const renderTweet = () => {
        const { current } = tweetRef;

        if (isTwitterLink(url)) {
          
          // single tweet
          if (singleTweet(url) !== null) {
            window.twttr.widgets.createTweet(
              singleTweet(url), current,
            {
              align: 'left'
            })
            .then(function (el) {
              // console.log("Tweet displayed.")
            });
          }

          // timeline
          else {
            window.twttr.widgets.createTimeline(
              {
                sourceType: 'url',
                url: url
              },
              current,
              {
                tweetLimit: 5,
                height: 300
              }
            ); 
          }  
        }  
    }

    useEffect(() => {
        if (!window.twttr) {
            addScript('https://platform.twitter.com/widgets.js', renderTweet)
        } else {
            renderTweet()
        }
    
    });

    return (
    <Fragment>
      <div className={`${parentClass}`} ref={tweetRef}></div>        
    </Fragment>
  );
}

Twitter.propTypes = {
    url: PropTypes.string,
    parentClass: PropTypes.string
};

Twitter.defaultProps = {
    url: "",
    parentClass: "",
};
  

export default Twitter;