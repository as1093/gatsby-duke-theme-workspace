import React from "react";
import PropTypes from "prop-types";
import bem from "../../_utils/bem";

import "./Eyebrow.scss"

const Eyebrow = ({
  text
}) => {
  const Eyebrow = `p`
  return <Eyebrow className={bem("eyebrow")}>{text}</Eyebrow>
}

Eyebrow.propTypes = {
  text: PropTypes.string
}

export default Eyebrow;