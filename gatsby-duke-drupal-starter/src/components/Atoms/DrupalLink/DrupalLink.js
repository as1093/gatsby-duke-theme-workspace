/**
 * Link component
 * Use: <Link></Link>
 */
import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import "./Link.scss"

const DrupalLink = ({
  children,
  block = "link",
  element,
  modifiers,
  url,
  target,
  title,
  className,
}) => {
  let renderedLink
  let cleanedURL = url.replace("internal:", "")
  if (cleanedURL.includes("http")) {
    renderedLink = (
      <a href={url} target={target} title={title} className={className}>
        {children}
      </a>
    )
  } else {
    renderedLink = (
      <Link to={cleanedURL} target={target} title={title} className={className}>
        {children}
      </Link>
    )
  }

  return <>{renderedLink}</>
}

DrupalLink.propTypes = {
  children: PropTypes.node,
  url: PropTypes.string.isRequired,
  target: PropTypes.string,
  title: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default DrupalLink
