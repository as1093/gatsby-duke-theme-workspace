/**
  * Iframe component (responsive)
  * Use: <Iframe src={} title={} width={} height={} />
  * @TODO: need to add logic/script for resizing the iFrame based on its content
  * @TODO: need to add responsiveness for fixed-width iFrames (100% width on smaller screens, fixed width on larger screens)
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import "./Iframe.scss"

const IframeStyle = {
  border: 0,
}

const Iframe = ({
  block = "iframe",
  element,
  modifiers,
  src,
  width,
  height,
  title,
}) => {

  return (

    <div className={bem(block, element, modifiers)}>
    {/* <div className="embed-responsive embed-responsive-16by9"> */}
      <iframe
        id={title}
        title={title}
        width={width ? width : '100%'}
        height={height ? height : '400'}
        src={src}
        allow="fullscreen"
        loading="lazy"
        style={IframeStyle}
        className="embed-responsive-item"
      ></iframe>
      {/* </div> */}
    </div>
  )
}

Iframe.propTypes = {
  src: PropTypes.string.isRequired,
  width: PropTypes.string,
  height: PropTypes.string,
  title: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Iframe
