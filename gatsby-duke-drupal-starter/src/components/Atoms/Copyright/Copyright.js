import React from "react";
import bem from "../../_utils/bem"
import PropTypes from "prop-types"
import DukeLogoWhite from '../../../images/DukeLogoWhite.svg'
import "./Copyright.scss"

const Copyright = ({
  block = "copyright",
  modifiers,
}) => {
  const Copyright = `div`
  return (
    <>
    <img className={bem(block, 'logo', [])} src={DukeLogoWhite} alt="Duke University" />
    
    <Copyright className={bem(block, "", modifiers)}>Copyright &copy; 2020 Duke University</Copyright>
    </>
  )
}

Copyright.propTypes = {
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Copyright