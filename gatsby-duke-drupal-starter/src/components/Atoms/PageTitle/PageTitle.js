import React from 'react';
import PropTypes from 'prop-types';
import Header from '../Heading/Heading';
import bem from '../../_utils/bem';
import './PageTitle.scss'

const PageTitle = ({ modifiers, children }) => (
  <div className={bem('page-title', 'wrapper', modifiers)}>
    <Header level={1} block="page-title">
      {children}
    </Header>
  </div>
);

PageTitle.propTypes = {
  Modifiers: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.string,
};

export default PageTitle;