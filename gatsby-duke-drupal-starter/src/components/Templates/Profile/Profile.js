import React from "react"
import Image from "../../Atoms/Image/Image"
import Heading from "../../Atoms/Heading/Heading"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import PaginatedContent from "../../Atoms/PaginatedContent/PaginatedContent"
import bem from "../../_utils/bem"
import ProfileRelatedContentTeaser from "./ProfileRelatedContentTeaser"

import './Profile.scss'
import { Row, Col } from "react-bootstrap"

export const Profile = ({
  title,
  roles,
  bio,
  email_address,
  phone_number,
  address_1,
  address_2,
  address_3,
  website_url,
  photo,
  related_content,
  block = "profile",
}) => {

  let sortedRelatedContentData = related_content
  
  // sort related content array by date DESC
  sortedRelatedContentData && sortedRelatedContentData.sort(function(a, b) {
    var date1 = new Date(a.field_date)
    var date2 = new Date(b.field_date)
    return date2 - date1
  })

  let ContentToPaginate = []
  for (let item in sortedRelatedContentData) {
    ContentToPaginate.push(
      <ProfileRelatedContentTeaser 
      key={item}
      title={sortedRelatedContentData[item].title}
      url={sortedRelatedContentData[item].path.alias}
      date={sortedRelatedContentData[item].field_date}
      type={sortedRelatedContentData[item].type.replace('node__', '').replace('_post', '')}
      />
    )
  }

  return (
    <section className={bem(block, '')}>
      <Row>
        <Col md={9}>
          {roles && 
            <Heading level={2}>
              {roles.map((role, index)  => 
                <span key={index}>{role}<br/></span>
              )}
            </Heading>  
          }  

          {bio && (
            <>
              <Heading level={3}>Bio</Heading> 
              <CKEditorContent content={bio} />
            </>  
          )}

          {ContentToPaginate && (ContentToPaginate.length > 0) &&
            <section className='related-content'>
              <Heading level={3}>Authored posts</Heading>  
              <PaginatedContent items={ContentToPaginate} />
            </section>  
          }
        
        </Col>
        <Col md={3}>
          {photo && (
            <div className={bem(block, 'image')}>
              {photo.relationships.field_media_image !== null && (
                <Image
                  image={
                    photo &&
                    photo.relationships.field_media_image.localFile}
                  imageType={'fixed'}  
                  alt={photo.relationships.field_media_image.alt}
                />
              )}
            </div>
          )}
          
          <Heading level={3}>Contact Info</Heading> 
          {email_address && (
            <p><a href={`mailto:${email_address}`}>{email_address}</a></p>
          )}  
          {phone_number && (
            <p><a href={`phone:${phone_number}`}>{phone_number}</a></p>
          )} 
          
          <p className={bem(block, 'address')}>
            {address_1 && (<span>{address_1}</span>)}
            {address_2 && (<span>{address_2}</span>)}
            {address_3 && (<span>{address_3}</span>)}
          </p>
          
          {website_url && (
            <>
              <Heading level={4}>Personal Link</Heading> 
              <a href={website_url}>{website_url}</a>
            </>  
          )}  
        </Col>
      </Row>
    </section>                 
  )
}  

Profile.propTypes = {
}

export default Profile
