/**
 * Profile teaser display template
 */
import React from "react"
import { Link } from "gatsby"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Image from "../../Atoms/Image/Image"
import "./Profile.scss"

export const ProfileTeaser = ({
  node,
  block = "profile-teaser",
  modifiers,
}) => {
  return (
    <article className={`${bem(block, modifiers)}`}>
      {node.relationships.field_image && (
        <div className={bem(block, "image")}>
          {node.relationships.field_image.relationships.field_media_image !==
            null && (
            <Image
              image={
                node.relationships.field_image.relationships.field_media_image
                  .localFile
              }
              imageType={"fluid"}
              alt={node.relationships.field_image.alt}
            />
          )}
        </div>
      )}

      {/* Name */}
      {node.title && (
        <p className={`${bem(block, "name")}`}>
          <Link to={node.path.alias}>{node.title}</Link>
        </p>
      )}

      {/* Roles */}
      {node.field_role ? (
        <p className={`${bem(block, "role")}`}>
          {node.field_role.map((role, index) => (
            <span key={index}>
              {role}
              <br />
            </span>
          ))}
        </p>
      ) : (
        ""
      )}
    </article>
  )
}

ProfileTeaser.propTypes = {
  node: PropTypes.object,
}

export default ProfileTeaser
