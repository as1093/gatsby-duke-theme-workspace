import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Eyebrow from "../../Atoms/Eyebrow/Eyebrow"
import { Link } from "gatsby"
import Moment from "react-moment"

const ProfileRelatedContentTeaser = ({
  title,
  date,
  url,
  type,
  block = "profile",
}) => {
  return (

    <div className={bem(block, "related-content-item")}>
      <Eyebrow text={type} />
      <p>
        <Link to={url}>{title}</Link>
      </p>
      <Moment format="MMMM DD, YYYY">
        {Date(date)}
      </Moment>
    </div>
  )
}

ProfileRelatedContentTeaser.propTypes = {
  title: PropTypes.string,
  date: PropTypes.string,
  url: PropTypes.string,
  type: PropTypes.string,
  block: PropTypes.string,
}

export default ProfileRelatedContentTeaser
