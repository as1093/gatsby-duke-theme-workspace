/**
 * Story teaser display template
 */
import React, { Fragment } from 'react'
import { Link } from 'gatsby';
import PropTypes from 'prop-types'
import { Row, Col } from 'react-bootstrap';
import bem from "../../_utils/bem"
import Image from "../../Atoms/Image/Image"
import Heading from "../../Atoms/Heading/Heading"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import { BsDot } from "react-icons/bs"

export const StoryTeaser = ({
  node,
  block = 'node-teaser',
  modifiers,
}) => {
  return (

    <article className={`${bem(block, modifiers)}`}>
      <Row>
        <Col md={4}>
          {node.relationships.field_featured_media && (
            <Image
              image={
                node.relationships.field_featured_media.relationships
                  .field_media_image.localFile
              }
              imageType={"fluid"}
              alt={node.relationships.field_featured_media.field_media_image.alt}
            />
          )}
        </Col>
        <Col md={8}>  
          <div className={bem(block, "content")}> 

            {/* Title */}
            {node.title && (
              <Heading level={3} modifiers={['teaser']}>
                <Link to={node.path.alias}>{node.title}</Link>
              </Heading>
            )}

            {/* Blurb */}
            {node.field_summary ? (
              <div>
                <CKEditorContent content={node.field_summary} />
              </div>
            ) : (
              ""
            )}

            <div className={bem(block, "meta")}>

              {/* Date */}
              {node.field_date && (
                <span className={bem(block, 'meta', ['date'])}>{node.field_date}</span>
              )}

              {/* Authors, separated by a comma */}
              {node.relationships.field_author_reference && node.relationships.field_author_reference?.length ? (
                <Fragment>
                  <span>
                    <BsDot />
                  </span>
                  {node.relationships.field_author_reference?.map((author, index) => (
                    <span key={index}>
                      {author.title}
                      {index < node.relationships.field_author_reference?.length - 1 ? ", " : " "}
                    </span>
                  ))}
                </Fragment>
              ) : (
                ""
              )}
            </div> 
          </div>
        </Col>
      </Row>

    </article>

  )
}  

StoryTeaser.propTypes = {
  node: PropTypes.object
}

export default StoryTeaser
