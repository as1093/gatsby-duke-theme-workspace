import React from "react"
import PropTypes from "prop-types"
import Image from "../../Atoms/Image/Image"
import Heading from "../../Atoms/Heading/Heading"
import Meta from "../../Molecules/Meta/Meta"
import Divider from "../../Atoms/Divider/Divider"
import Eyebrow from "../../Atoms/Eyebrow/Eyebrow"
import Share from "../../Atoms/Share/Share"
import Authors from "../../Molecules/Authors/Authors"

const Story = props => (
  <>
    {props.featured_media && (
      <div className="story-featured-image">
        {props.featured_media.relationships.field_media_image !== null && (
          <Image
            image={
              props.featured_media &&
              props.featured_media.relationships.field_media_image.localFile}
            imageType={'fluid'}  
            alt={props.featured_media.relationships.field_media_image.alt}
          />
        )}
      </div>
    )}

    <Eyebrow text="Story" />
    <Heading level={1}>{props.title}</Heading>
    <Heading level={2}>{props.subtitle}</Heading>
    <Meta authors={props.authors} date={props.date} />
    <Divider />
    <Share shareTitle="Share this story" pageTitle={props.title} pageSummary={props.summary} />

    {props.paragraphs}
        
    <Share shareTitle="Share this story" pageTitle={props.title} pageSummary={props.summary} />

    <Divider />

    <Authors authors={props.authors} />

  </>
)

Story.propTypes = {
  featured_media: PropTypes.object,
  additional_media: PropTypes.object,
  paragraphs: PropTypes.array,
}

export default Story
