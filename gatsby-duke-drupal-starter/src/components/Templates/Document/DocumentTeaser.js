/**
 * Document teaser display template
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import { Row, Col } from "react-bootstrap"

const DocumentTeaser = ({
  title,
  description,
  file,
  date,
  block = "node-teaser",
  modifiers
}) => (
  <article className={`${bem(block, modifiers)}`}>
    <Row>
      <Col md={12}>
        <section className={bem("document-teaser")}>
          {/* Title */}
          {title && <Heading level={5}>{title}</Heading>}

          {/* Categories */}
          {file?.relationships?.field_categories && (
            <div className={bem("document-teaser", "category")}>
              {file.relationships.field_categories?.map((category, index) => (
                <span key={index}>{category.name}</span>
              ))}
            </div>
          )}

          {/* Description */}
          <div className={bem("document-teaser", "description")}>
            {description && <CKEditorContent content={description} />}
          </div>

          {/* Link to document - Document Upload */}
          {file &&
            file.relationships && (
              <a
                href={
                  file.relationships.field_document.relationships.field_document.localFile.publicURL
                }
              >
                {
                  file.relationships.field_document
                    .name
                }
              </a>
            )}

          {/* Link to document - Document URL */}
          {file &&
            file.field_url && (
              <a href={file.field_url.uri}>
                {title}
              </a>
            )}

          {/* Last Modified Date */}
          {date && (
            <p className={bem("document-teaser", "date")}>
              <i>Revised {date}</i>
            </p>
          )}
        </section>
      </Col>
    </Row>
  </article>
)

DocumentTeaser.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  categories: PropTypes.array,
  file: PropTypes.object,
  date: PropTypes.string,
}

export default DocumentTeaser
