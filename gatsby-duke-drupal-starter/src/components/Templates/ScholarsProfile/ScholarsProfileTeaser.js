/**
 * ScholarsProfile teaser display template
 */
import React from "react"
import { Link } from "gatsby"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import { Col } from "react-bootstrap"
import "../Profile/Profile.scss"

export const ScholarsProfileTeaser = ({
  node,
  block = "profile-teaser",
  modifiers,
}) => {
  return (
    <Col className={`${bem(block, modifiers)}`} md={3}>
      {node.duke_scholars_profile_thumbnail && (
        <div className={bem(block, "image")}>
          <img
            src={node.duke_scholars_profile_thumbnail.thumbnail}
            alt={node.duke_scholars_profile_thumbnail.thumbnail.alt_text}
          />
        </div>
      )}

      {/* Name */}
      {node.title && (
        <p className={`${bem(block, "name")}`}>
          <Link to={`/duke-scholars-profile/${node.drupal_internal__id}`}>
            {node.title}
          </Link>
        </p>
      )}

      {/* Positions */}
      {node.relationships.duke_scholars_profile_positions && (
        <p className={`${bem(block, "role")}`}>
          {node.relationships.duke_scholars_profile_positions.map(
            (role, index) => (
              <>
                {role.position_preferred === true ? (
                  <span key={index}>
                    {role.title}
                  </span>
                ) : ''}
              </>
            )
          )}
        </p>
      )}
    </Col>
  )
}

ScholarsProfileTeaser.propTypes = {
  node: PropTypes.object,
}

export default ScholarsProfileTeaser
