import React from "react"
import PropTypes from "prop-types"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import DateProcessor from "../../Atoms/DateProcessor/DateProcessor"
import TimeProcessor from "../../Atoms/TimeProcessor/TimeProcessor"
import Image from "../../Atoms/Image/Image"
import { imageTypeProps } from "../../Atoms/Image/Image"
import Divider from "../../Atoms/Divider/Divider"
import Share from "../../Atoms/Share/Share"

const Event = props => (
  <>
    <h1>
      {props.event_status &&
        <>
          {props.event_status.name !== 'CONFIRMED' &&
            <>
              <span className="event-status"><i>{props.event_status.name}:&nbsp;</i></span>
            </>
          }
          {props.title_override ? props.title_override : props.title}
        </>
      }
    </h1>
    <aside>
      <div className="aside-top">
        <DateProcessor 
          startDate={props.event_date.start}
          endDate={props.event_date.end}
        />
        <TimeProcessor 
          startTime={props.event_date.start}
          endTime={props.event_date.end}
        />
        {props.location_text && (
          <>
            {props.location_text !== 'None' && 
              <a href={props.location_link && props.location_link.uri}>
                {props.location_text}
              </a>
            }
          </>
        )}
				{props.cost &&
					<>
						<h2>Cost</h2>
						<p>{props.cost}</p>
					</>
				}
        {props.webcast_url && (
          <>
            <h2>Webcast</h2>
            <p>{props.webcast_url.uri}</p>
          </>
        )}
        <Share
          shareTitle="Share this event"
          pageTitle={props.title}
          pageSummary={props.summary}
        />
      </div>
      <div className="aside-bottom">
        {props.event_sponsors &&
          <>
            <h2>Event Sponsored By</h2>
            <p>
              <span className="event-sponsor">{props.event_sponsors.name}</span>
              {props.event_co_sponsors?.length >= 1 &&
                <>
                  &nbsp;&amp;
                  <br />
                  {props.event_co_sponsors.map((cosponsor,i) => (
                    <>
                      <span className="event-co-sponsor">{cosponsor.name}</span>
                      {i+1 < props.event_co_sponsors.length &&
                        <span>&#44;&nbsp;</span>
                      }
                    </>
                  ))}
                </>
              }
            </p>
          </>
        }
				{props.event_series.length >= 1 &&
					<>
						<h2>Series</h2>
						<ul>
							{props.event_series.map((series, i) => (
								<li key={"series-" + i + 1}>{series.name}</li>
							))}
						</ul>
					</>
				}
        {props.contact_name !== 'None' &&
          <>
            <h2>Contact</h2>
            <p><a href={props.contact_email && "mailto:" + props.contact_email}>{props.contact_name}</a></p>
            {props.contact_phone &&
              <p>{props.contact_phone}</p>
            }
          </>
        }
      </div>
    </aside>
    <div className="event-content">
      {props.feed_image_url ? (
        <>
          {props.featured_image ? (
            <div className="events-featured-image">
              {props.featured_image.relationships.field_media_image !==
                null && (
                <Image
                  image={
                    props.featured_image &&
                    props.featured_image.relationships.field_media_image
                      .localFile
                  }
                  imageType={"fluid"}
                  alt={props.featured_image.relationships.field_media_image.alt}
                />
              )}
            </div>
          ) : (
            <div className="events-featured-image">
              <Image
                image={props.feed_image_url.uri}
                imageType={"fluid"}
                alt={props.feed_image_alt}
              />
            </div>
          )}
        </>
      ) : (
        <>
          {props.featured_image && (
            <div className="events-featured-image">
              {props.featured_image.relationships.field_media_image !==
                null && (
                <Image
                  image={
                    props.featured_image &&
                    props.featured_image.relationships.field_media_image
                      .localFile
                  }
                  imageType={"fluid"}
                  alt={props.featured_image.relationships.field_media_image.alt}
                />
              )}
            </div>
          )}
        </>
      )}
			{props.speaker &&
			<>
				<h2>Speaker</h2>
				<p>{props.presenter}</p>
			</>
			}
      <div className="body">
        {props.body && <CKEditorContent content={props.body.processed} />}
      </div>
			<div className="more-event-info">
        {props.more_event_info &&
          <>
            {props.more_event_info.map((more, i) => (
              <p key={"more-" + i+1}>
                <a href={more.uri} key={"more-" + i + 1}>
                  More Event Information
                </a>
              </p>
            ))}
          </>
        }
        {props.additional_links &&
          <>
            {props.additional_links.map((link,i) => (
              <p>
                <a href={link.uri} key={"addl-link-" + i +1}>{link.title}</a>
              </p>
            ))}
          </>
        }
			</div>
			<Divider />
      {props.categories.length >= 1 &&
        <>
          <h2>Categories</h2>
          <ul>
            {props.categories.map((cat, i) => (
              <li key={"cat-" + i + 1}>{cat.name}</li>
            ))}
          </ul>
        </>
      }
    </div>
    {/* <h2>Event Source URL</h2>
    <a href={props.event_source_url}>Event Source</a> */}
  </>
)

Event.propTypes = {
	image: imageTypeProps,
  title: PropTypes.string.isRequired,
  title_override: PropTypes.string,
  alternate_link: PropTypes.string,
  alternate_link_title: PropTypes.string,
  featured_image: PropTypes.object,
  additional_media: PropTypes.array,
  event_date: PropTypes.object,
  location_link: PropTypes.string,
  location_text: PropTypes.string,
  presenter: PropTypes.string,
  cost: PropTypes.string,
  summary: PropTypes.string,
  body: PropTypes.object,
  additional_links: PropTypes.array,
	contact_name: PropTypes.string,
	contact_email: PropTypes.string,
	contact_phone: PropTypes.string,
  more_event_info: PropTypes.array,
  categories: PropTypes.array,
  event_series: PropTypes.array,
  event_sponsors: PropTypes.object,
  event_co_sponsors: PropTypes.array,
  event_source_url: PropTypes.string,
  webcast_url: PropTypes.object,
  feed_image_url: PropTypes.object,
	feed_image_alt: PropTypes.string,
	event_status: PropTypes.object
}

export default Event
