import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import DateProcessor from "../../Atoms/DateProcessor/DateProcessor"
import TimeProcessor from "../../Atoms/TimeProcessor/TimeProcessor"
import DrupalLink from "../../Atoms/DrupalLink/DrupalLink"
import Eyebrow from "../../Atoms/Eyebrow/Eyebrow"
import { Row, Col } from "react-bootstrap"

const EventTeaser = ({
  date,
  title,
  status,
  link,
  location_url,
  location_text,
  block = "node-teaser",
  element,
  modifiers,
}) => {
  return (
    <div className={bem(block, element, modifiers)}>
      <Row>
        <Col>
        <DateProcessor startDate={date} cardView />
        <Eyebrow text={status ? (status !== 'CONFIRMED' ? status : "") : ""} />
        <Heading level={4} className={bem(block, "title")}><a href={link}>{title}</a></Heading>
        <TimeProcessor startTime={date} />
        {location_url ? (
          <>
            <DrupalLink url={location_url} children={location_text ? location_text : location_url} />
          </>
        ) : (
          <>
            {location_text && <p>{location_text}</p>}
          </>
        )}
        </Col>
      </Row>
    </div>
  )
}

EventTeaser.propTypes = {
  date: PropTypes.string,
  title: PropTypes.string,
  status: PropTypes.string,
  link: PropTypes.string,
  location_url: PropTypes.string,
  location_text: PropTypes.string,
}

export default EventTeaser
