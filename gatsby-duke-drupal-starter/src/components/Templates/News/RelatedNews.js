import React from 'react'
// import PropTypes from 'prop-types'
// import Img from 'gatsby-image'
import CKEditorContent from '../../Atoms/CKEditor/DrupalCKEditor'
import DateProcessor from "../../Atoms/DateProcessor/DateProcessor"

function RelatedNews(props) {
  const id_list = []
  const related_list = []

  // get parent news item categories
  props.categories.forEach(category => (
    id_list.push(category.id)
  ))
  
  // create an array with key & value to store necessary information
  props.categories.forEach(category => {
    category.relationships.node__news.forEach(news => (
      related_list.push({
        id: news.id,
        title: news.title,
        date: news.field_date && news.field_date,
        path: news.path.alias,
        summary: news.body && news.body.summary && news.body.summary,
        body: news.body && news.body.value && news.body.value.substr(0,255),
        categories_list: news.relationships.field_categories !== null && news.relationships.field_categories.map(category => (category.id)),
        matching: 0
      })
    ))
  })
  
  // remove duplicate news items & current news item from array
  const unique = related_list.map(e => e['id'])
    .map((e, i, final) => final.indexOf(e) === i && i)
    .filter((e) => related_list[e]).map(e => related_list[e])
    .filter((e) => e.id !== props.id)
  
  // check how many matching category tags each related news item has
  for (var news of unique) {
    for (var cat of id_list) {
      if (news.categories_list.includes(cat)) {
        news.matching += 1
      }
    }
  }

  // sort array by matching category tag count and date
  unique.sort(function(a, b) {
    var date1 = new Date(a.date)
    var date2 = new Date(b.date)
    return date2 - date1
  })
  unique.sort(function(a, b) {
    return b.matching - a.matching
  })
      
  return(
    <>
      <h3>Related News</h3>
      {unique.length === Number(0) ? (<p>There are no related news at the moment</p>) : (
        <ul>
        {unique.slice(0,3).map(news => (
          <li key={news.id}>
            <ul>
              <a href={news.path}>
                {news.date !== null &&
                  <li key={news.id + "-date"}>
                    <DateProcessor startDate={news.date} />
                  </li>
                }
                <li key={news.id + "-title"}>{news.title}</li>
                <li key={news.id + "-summary"}><CKEditorContent content={news.summary !== null ? news.summary : (news.body?.length >= 255 ? news.body?.substr(0,news.body.lastIndexOf(" ")) + " ..." : news.body )} /></li>
              </a>
            </ul>
          </li>
        ))}
        </ul>
      )}
    </>
  )
}

export default RelatedNews