/**
 * News teaser display template
 */
import React, { Fragment } from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import Heading from "../../Atoms/Heading/Heading"
import Image from "../../Atoms/Image/Image"
import { Row, Col } from "react-bootstrap"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import HTMLEllipsis from "react-lines-ellipsis/lib/html"
import { BsDot } from "react-icons/bs"

const NewsTeaser = ({
  date,
  link,
  alternate_link,
  title,
  body,
  source,
  featured_image,
  feed_image_url,
  feed_image_alt,
  block = "node-teaser",
  modifiers,
}) => {
  let hasImage
  if (feed_image_url) {
    hasImage = true
  }
  else {
    if (featured_image) {
      hasImage = true
    }
    else {
      hasImage = false
    }
  }

  return (
    <article className={`${bem(block, modifiers)}`}>
      {hasImage ? (
        <Row>
          <Col md={4}>
            {feed_image_url ? (
              <>
                {featured_image ? (
                  <div className="news-featured-image">
                    {featured_image.relationships.field_media_image !== null && (
                      <Image
                        image={
                          featured_image &&
                          featured_image.relationships.field_media_image.localFile
                        }
                        imageType={"fluid"}
                        alt={featured_image.relationships.field_media_image.alt}
                      />
                    )}
                  </div>
                ) : (
                  <div className="news-featured-image">
                    <Image
                      image={feed_image_url.uri}
                      imageType={"fluid"}
                      alt={feed_image_alt}
                    />
                  </div>
                )}
              </>
            ) : (
              <>
                {featured_image && (
                  <div className="news-featured-image">
                    {featured_image?.relationships.field_media_image !== null && (
                      <Image
                        image={
                          featured_image &&
                          featured_image.relationships.field_media_image.localFile
                        }
                        imageType={"fluid"}
                        alt={featured_image.relationships.field_media_image.alt}
                      />
                    )}
                  </div>
                )}
              </>
            )}
          </Col>
          <Col md={8}>
            {alternate_link ? (
              <Heading level={3} modifiers={["teaser"]}>
                <a href={alternate_link.uri}>{title}</a>
              </Heading>
            ) : (
              <Heading level={3} modifiers={["teaser"]}>
                <a href={link}>{title}</a>
              </Heading>
            )}

            {/* Blurb */}
            <div>
              {body?.summary ? (
                <CKEditorContent content={body?.summary} />
              ) : (
                <HTMLEllipsis
                  unsafeHTML={body?.processed}
                  maxLine="3"
                  ellipsis="..."
                  basedOn="words"
                />
              )}
            </div>

            <div className={bem(block, "meta", ["date"])}>
              {date ? date : ""}
              {source ? (
                <Fragment>
                  <span>
                    <BsDot />
                  </span>
                  {source}
                </Fragment>
              ) : (
                ""
              )}
            </div>
          </Col>
        </Row>
      ) : (
        <Row>
          <Col md={12}>
            {alternate_link ? (
              <Heading level={3} modifiers={["teaser"]}>
                <a href={alternate_link.uri}>{title}</a>
              </Heading>
            ) : (
              <Heading level={3} modifiers={["teaser"]}>
                <a href={link}>{title}</a>
              </Heading>
            )}

            {/* Blurb */}
            <div>
              {body?.summary ? (
                <CKEditorContent content={body?.summary} />
              ) : (
                <HTMLEllipsis
                  unsafeHTML={body?.processed}
                  maxLine="3"
                  ellipsis="..."
                  basedOn="words"
                />
              )}
            </div>

            <div className={bem(block, "meta", ["date"])}>
              {date ? date : ""}
              {source ? (
                <Fragment>
                  <span>
                    <BsDot />
                  </span>
                  {source}
                </Fragment>
              ) : (
                ""
              )}
            </div>
          </Col>
        </Row>
      )}
    </article>
  )
}

NewsTeaser.propTypes = {
  date: PropTypes.string,
  body: PropTypes.object,
  link: PropTypes.string,
  alternate_link: PropTypes.object,
  title: PropTypes.string,
  featured_image: PropTypes.object,
  feed_image_url: PropTypes.object,
  feed_image_alt: PropTypes.string,
}

export default NewsTeaser
