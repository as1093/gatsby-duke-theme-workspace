import React from "react"
import PropTypes from "prop-types"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import Image from "../../Atoms/Image/Image"
import DateProcessor from "../../Atoms/DateProcessor/DateProcessor"
import { imageTypeProps } from "../../Atoms/Image/Image"
import Share from "../../Atoms/Share/Share"

const News = props => (
  <>
    <h1>{props.title_override ? props.title_override : props.title}</h1>
    <h2>{props.subtitle}</h2>
    {props.feed_image_url ? (
      <>
        {props.featured_image ? (
          <div className="news-featured-image">
            {props.featured_image.relationships.field_media_image !==
              null && (
              <Image
                image={
                  props.featured_image &&
                  props.featured_image.relationships.field_media_image
                    .localFile
                }
                imageType={"fluid"}
                alt={props.featured_image.relationships.field_media_image.alt}
              />
            )}
          </div>
        ) : (
          <div className="news-featured-image">
            <Image
              image={props.feed_image_url.uri}
              imageType={"fluid"}
              alt={props.feed_image_alt}
            />
          </div>
        )}
      </>
    ) : (
      <>
        {props.featured_image && (
          <div className="news-featured-image">
            {props.featured_image.relationships.field_media_image !==
              null && (
              <Image
                image={
                  props.featured_image &&
                  props.featured_image.relationships.field_media_image
                    .localFile
                }
                imageType={"fluid"}
                alt={props.featured_image.relationships.field_media_image.alt}
              />
            )}
          </div>
        )}
      </>
    )}
    <aside>
      {props.date &&
        <>
          <h2>Date</h2>
          <DateProcessor startDate={props.date} />
        </>
      }
      {props.authors.length > 0 && 
        <>
          <h2>By</h2>
          <p>
            {props.authors.map((author,i) => (
              <span key={"author-" + i+1}>
                <span>{author.title}</span>
                {i < props.authors.length - 1 &&
                  <span>,&nbsp;</span>
                }
              </span>
            ))}
          </p>
        </>
      }
      <Share
        shareTitle="Share this news"
        pageTitle={props.title}
        pageSummary={props.summary}
      />
    </aside>
    <div className="body">
      {props.body && <CKEditorContent content={props.body.processed} />}
    </div>
    <div className="slideshow">
      {props.slideshow}
    </div>
  </>
)

News.propTypes = {
  image: imageTypeProps,
  title: PropTypes.string.isRequired,
  title_override: PropTypes.string,
  featured_image: PropTypes.object,
  feed_image_url: PropTypes.object,
  feed_image_alt: PropTypes.string,
  author: PropTypes.object,
  slideshow: PropTypes.object,
}

export default News
