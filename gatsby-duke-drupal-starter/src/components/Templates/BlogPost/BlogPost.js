import React from "react"
import PropTypes from "prop-types"
import Heading from "../../Atoms/Heading/Heading"
import Meta from "../../Molecules/Meta/Meta"
import Share from "../../Atoms/Share/Share"
import Image from "../../Atoms/Image/Image"
import CKEditorContent from "../../Atoms/CKEditor/DrupalCKEditor"
import Divider from "../../Atoms/Divider/Divider"
import Eyebrow from "../../Atoms/Eyebrow/Eyebrow"
import Categories from "../../Molecules/Categories/Categories"

const BlogPost = props => (
  <>
    {props.featured_media && (
      <div className="blog-featured-image">
        {props.featured_media.relationships.field_media_image !== null && (
          <Image
            image={
              props.featured_media &&
              props.featured_media.relationships.field_media_image.localFile
            }
            imageType={'fluid'}
            alt={props.featured_media.relationships.field_media_image.alt}
          />
        )}
      </div>
    )}

    <Eyebrow text="Blog" />
    <Heading level={1}>{props.title}</Heading>

    <Meta authors={props.authors} date={props.date} />
    
    <Divider />
    <Share
      shareTitle="Share this post"
      pageTitle={props.title}
      pageSummary={props.summary}
    />

    {props.body &&
      <div className="blog-body">
        <CKEditorContent content={props.body.processed} />
      </div>
    }
    
    {props.slideshow &&
      <div className="blog-slideshow">
        {props.slideshow}
      </div>
    }

    <Divider modifiers={['long']} /> 

    {props.categories &&
      <Categories categories={props.categories} />
    }
  </>
)

BlogPost.propTypes = {
  featured_media: PropTypes.object,
}

export default BlogPost
