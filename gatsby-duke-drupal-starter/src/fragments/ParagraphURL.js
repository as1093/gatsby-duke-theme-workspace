import { graphql } from "gatsby"

export const ParagraphURL = graphql`
  fragment ParagraphURL on paragraph__url {
    id
    field_url {
      uri
      title
    }
  }
`;