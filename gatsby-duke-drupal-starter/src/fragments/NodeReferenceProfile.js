import { graphql } from "gatsby"

export const NodeProfile = graphql`
  fragment NodeProfile on node__profile {
    id
    field_email_address
    field_phone_number
    field_role
    title
    field_bio {
      summary
      processed
    }
    path {
      alias
    }
    relationships {
      field_image {
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              publicURL
              childImageSharp {
                fixed(cropFocus: CENTER, height: 300, width: 300) {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
        }
      }
    }
  }
`;