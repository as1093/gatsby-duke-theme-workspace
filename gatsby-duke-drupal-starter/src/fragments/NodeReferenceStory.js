import { graphql } from "gatsby"

export const NodeStory = graphql`
  fragment NodeStory on node__story {
    id
    title
    field_date(formatString: "MMMM D, YYYY")
    field_summary
    path {
      alias
    }
    relationships {
      field_author_reference {
        title
        field_last_name
        path {
          alias
        }
      } 
      field_featured_media {
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              publicURL
              childImageSharp {
                fluid(maxWidth: 900, maxHeight: 600) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`;