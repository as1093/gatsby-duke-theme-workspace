import { graphql } from "gatsby"

export const ParagraphSlideshow = graphql`
  fragment ParagraphSlideshow on paragraph__slideshow {
    id
    field_display_title
    relationships {
      field_paragraph_items {
        field_media_caption {
          processed
        }
        relationships {
          field_image {
            field_media_image {
              alt
            }
            relationships {
              field_media_image {
                localFile {
                  publicURL
                  childImageSharp {
                    fluid(fit: COVER, maxWidth: 1440, maxHeight: 810) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
