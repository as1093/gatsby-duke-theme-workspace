import { graphql } from "gatsby"

export const ParagraphFaqsList = graphql `
  fragment ParagraphFaqsList on paragraph__faqs_list {
    id
    field_display_title
    relationships {
      field_category {
        relationships {
          node__faq {
            id
            title
            field_question
            field_answer {
              processed
            }
          }
        }
      }
    }
  }
`