import { graphql } from "gatsby"

export const NodeDocument = graphql`
  fragment NodeDocument on node__document {
    id
    title
    field_last_modified_date(formatString: "M/D/YY")
    body {
      processed
    }
    relationships {
      field_categories {
        name
      }
      field_document {
        ...ParagraphDocumentUpload
        ...ParagraphURL
      }
    }
  }
`;