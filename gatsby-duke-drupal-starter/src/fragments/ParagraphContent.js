import { graphql } from "gatsby"

export const ParagraphContent = graphql`
  fragment ParagraphContent on paragraph__content {
    id
    field_textarea {
      processed
    }
  }
`;