import { graphql } from "gatsby"

export const ParagraphHeroImage = graphql`
  fragment ParagraphHeroImage on paragraph__hero_image {
    id
    field_heading
    field_link {
      title
      uri
    }
    relationships {
      field_image {
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              publicURL
              extension
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
