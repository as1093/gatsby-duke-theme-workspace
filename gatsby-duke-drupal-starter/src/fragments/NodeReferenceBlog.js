import { graphql } from "gatsby"

export const NodeBlogPost = graphql`
  fragment NodeBlogPost on node__blog_post {
    id
    field_date(formatString: "MMMM D, YYYY")
    title
    path {
      alias
    }
    relationships {
      field_author_reference {
        title
        field_last_name
        path {
          alias
        }
      }    
      field_featured_media {
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              childImageSharp {
                fluid(maxWidth: 900, maxHeight: 600) {
                  ...GatsbyImageSharpFluid
                }
              }
              publicURL
            }
          }
        }
      }
    }
  }
`;