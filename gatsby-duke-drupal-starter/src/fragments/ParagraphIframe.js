import { graphql } from "gatsby"

export const ParagraphIframe = graphql`
  fragment ParagraphIframe on paragraph__iframe {
    id
    field_display_title
    field_width
    field_height
    field_link {
      title
      uri
    }
  }
`;