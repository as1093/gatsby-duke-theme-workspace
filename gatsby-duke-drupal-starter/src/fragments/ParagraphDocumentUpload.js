import { graphql } from "gatsby"

export const ParagraphDocumentUpload = graphql`
  fragment ParagraphDocumentUpload on paragraph__document_upload {
    id
    relationships {
      field_document {
        name
        relationships {
          field_document {
            localFile {
              publicURL
            }
          }
        }
      }
    }
  }
`;