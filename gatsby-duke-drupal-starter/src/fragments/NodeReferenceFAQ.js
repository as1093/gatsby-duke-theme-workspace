import { graphql } from "gatsby"

export const NodeFAQ = graphql`
  fragment NodeFAQ on node__faq {
    id
    field_question
    field_answer {
      processed
    }
  }
`;