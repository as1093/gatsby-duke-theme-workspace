import { graphql } from "gatsby"

export const ParagraphTwoColumn = graphql`
  fragment ParagraphTwoColumn on paragraph__two_column {
    id
    field_pb_column_layout
    field_display_title
    relationships {
      field_left_column {
        type: __typename
        ...ParagraphAccordionSection
        ...ParagraphBlogList
        ...ParagraphButton
        ...ParagraphCallout
        ...ParagraphContent
        ...ParagraphContentReference
        ...ParagraphEventList
        ...ParagraphFaqsList
        ...ParagraphIframe
        ...ParagraphImageText
        ...ParagraphMedia
        ...ParagraphNewsList
        ...ParagraphPullQuote
        ...ParagraphScholarsList
        ...ParagraphSlideshow
        ...ParagraphSocialMedia
        ...ParagraphSpecialText
        ...ParagraphStoryList
      }
      field_right_column {
        type: __typename
        ...ParagraphAccordionSection
        ...ParagraphBlogList
        ...ParagraphButton
        ...ParagraphCallout
        ...ParagraphContent
        ...ParagraphContentReference
        ...ParagraphEventList
        ...ParagraphFaqsList
        ...ParagraphIframe
        ...ParagraphImageText
        ...ParagraphMedia
        ...ParagraphNewsList
        ...ParagraphPullQuote
        ...ParagraphScholarsList
        ...ParagraphSlideshow
        ...ParagraphSocialMedia
        ...ParagraphSpecialText
        ...ParagraphStoryList
      }
    }
  }
`;