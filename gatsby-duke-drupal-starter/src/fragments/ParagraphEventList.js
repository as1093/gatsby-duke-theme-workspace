import { graphql } from "gatsby"

export const ParagraphEventList = graphql `
  fragment ParagraphEventList on paragraph__event_listing {
    id
    field_display_title
    field_link {
      title
      uri
    }
    relationships {
      field_category {
        relationships {
          node__event {
            id
            title
            field_event_date {
              value
              end_value
            }
            field_location_link {
              uri
            }
            field_location_text
            relationships {
              field_status {
                name
              }
            }
            path {
              alias
            }
          }
        }
      }
    }
  }
`