import { graphql } from "gatsby"

export const ParagraphScholarsList = graphql `
  fragment ParagraphScholarsList on paragraph__scholars_profile_list {
    id
    field_display_title
    relationships {
      field_category {
        relationships {
          duke_scholars_profile__duke_scholars_profile {
            id
            title
          }
        }
      }
    }
  }
`