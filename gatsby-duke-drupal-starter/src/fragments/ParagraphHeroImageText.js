import { graphql } from "gatsby"

export const ParagraphHeroImageText = graphql`
  fragment ParagraphHeroImageText on paragraph__hero_image_text {
    id
    field_display_title
    field_heading
    field_hero_display_options
    field_link {
      uri
      title
    }
    field_tagline
    relationships {
      field_image {
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              childImageSharp {
                fluid(maxWidth: 750, maxHeight: 550, fit: COVER) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
