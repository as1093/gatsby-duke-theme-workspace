import { graphql } from "gatsby"

export const ParagraphImageCard = graphql`
  fragment ParagraphImageCard on paragraph__image_card {
    id
    field_body {
      processed
    }
    field_heading
    field_link {
      title
      uri
    }
    relationships {
      field_image {
        field_focal_point_area
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              publicURL
              childImageSharp {
                ATTENTION: fluid(
                  cropFocus: ATTENTION
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }
                CENTER: fluid(
                  cropFocus: CENTER
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }                
                NORTH: fluid(
                  cropFocus: NORTH
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }
                SOUTH: fluid(
                  cropFocus: SOUTH
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }
                EAST: fluid(
                  cropFocus: EAST
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }
                WEST: fluid(
                  cropFocus: WEST
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }
                NORTHEAST: fluid(
                  cropFocus: NORTHEAST
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }
                NORTHWEST: fluid(
                  cropFocus: NORTHWEST
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }
                SOUTHEAST: fluid(
                  cropFocus: SOUTHEAST
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }
                SOUTHWEST: fluid(
                  cropFocus: SOUTHWEST
                  fit: COVER
                  maxHeight: 800
                  maxWidth: 1200
                ) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`;