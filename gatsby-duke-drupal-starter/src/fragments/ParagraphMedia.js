import { graphql } from "gatsby"

export const ParagraphMedia = graphql`
    fragment ParagraphMedia on paragraph__media {
      id
      field_display_title
      field_tagline
      field_media_caption {
        processed
      }
      relationships {
        field_media_item {
          type: __typename
          ... on media__image {
            field_media_image {
              alt
            }
            relationships {
              field_media_image {
                localFile {
                  publicURL
                  childImageSharp {
                    fluid(maxWidth: 1025) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
          ... on media__video {
            field_media_oembed_video
          }
          ... on media__audio {
            field_media_soundcloud
          }          
          ... on media__document {
            name
            relationships {
              field_document {
                localFile {
                  publicURL
                }
              }
            }
          }
        }
      }
    }
`;