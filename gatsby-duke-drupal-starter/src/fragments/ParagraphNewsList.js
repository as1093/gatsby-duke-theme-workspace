import { graphql } from "gatsby"

export const ParagraphNewsList = graphql `
  fragment ParagraphNewsList on paragraph__news_list {
    id
    field_display_title
    field_link {
      uri
      title
    }
    relationships {
      field_category {
        relationships {
          node__news {
            id
            title
            field_date
            path {
              alias
            }
            field_alternate_link {
              uri
            }
            body {
              processed
              summary
            }
            relationships {
              field_featured_media {
                field_media_image {
                  alt
                }
                relationships {
                  field_media_image {
                    localFile {
                      childImageSharp {
                        fluid(maxWidth: 1025) {
                          ...GatsbyImageSharpFluid
                        }
                      }
                    }
                  }
                }
              }
            }
            field_feed_image_url {
              uri
            }
            field_feed_image_alt_text
            field_news_source
          }
        }
      }
    }
  }
`