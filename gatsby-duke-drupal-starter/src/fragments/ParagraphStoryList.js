import { graphql } from "gatsby"

export const ParagraphStoryList = graphql `
  fragment ParagraphStoryList on paragraph__story_list {
    id
    field_display_title
    field_link {
      title
      uri
    }
    relationships {
      field_category {
        relationships {
          node__story {
            id
            title
          }
        }
      }
    }
  }
`