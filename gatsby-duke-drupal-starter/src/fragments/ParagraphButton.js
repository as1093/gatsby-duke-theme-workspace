import { graphql } from "gatsby"

export const ParagraphButton = graphql`
  fragment ParagraphButton on paragraph__button {
    id
    field_alignment
    field_link {
      title
      uri
    }
  }
`;
