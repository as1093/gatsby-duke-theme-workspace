import { graphql } from "gatsby"

export const NodeEvent = graphql`
  fragment NodeEvent on node__event {
    id
    title
    field_location_link {
      uri
    }
    field_location_text
    field_event_date {
      value
      end_value
    }
    path {
      alias
    }
    relationships {
      field_status {
        name
      }
    }
  }
`;