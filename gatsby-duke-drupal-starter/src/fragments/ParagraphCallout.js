import { graphql } from "gatsby"

export const ParagraphCallout = graphql`
  fragment ParagraphCallout on paragraph__callout {
    id
    field_display_title
    field_body {
      processed
    }
    field_link {
      uri
      title
    }
    relationships {
      field_image {
        field_focal_point_area
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              publicURL
              childImageSharp {
                ATTENTION: fluid(
                  cropFocus: ATTENTION
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }
                CENTER: fluid(
                  cropFocus: CENTER
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }                
                NORTH: fluid(
                  cropFocus: NORTH
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }
                SOUTH: fluid(
                  cropFocus: SOUTH
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }
                EAST: fluid(
                  cropFocus: EAST
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }
                WEST: fluid(
                  cropFocus: WEST
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }
                NORTHEAST: fluid(
                  cropFocus: NORTHEAST
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }
                NORTHWEST: fluid(
                  cropFocus: NORTHWEST
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }
                SOUTHEAST: fluid(
                  cropFocus: SOUTHEAST
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }
                SOUTHWEST: fluid(
                  cropFocus: SOUTHWEST
                  fit: COVER
                  maxHeight: 500
                  maxWidth: 500
                ) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
