import { graphql } from "gatsby"

export const ParagraphSocialMedia = graphql`
  fragment ParagraphSocialMedia on paragraph__social_media {
    id
    field_display_title
    field_url {
      uri
    }
  }
`;
