import { graphql } from "gatsby"

export const ParagraphImageText = graphql`
  fragment ParagraphImageText on paragraph__image_text {
    id
    field_heading
    field_image_alignment
    field_link {
      title
      uri
    }
    field_textarea {
      processed
    }
    relationships {
      field_image {
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              publicURL
              childImageSharp {
                fluid(maxWidth: 1025) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`;