import { graphql } from "gatsby"

export const ParagraphImageCardRow = graphql`
  fragment ParagraphImageCardRow on paragraph__image_card_row {
    id
    field_display_title
    relationships {
      field_image_card {
        field_heading
        field_body {
          processed
        }
        field_link {
          title
          uri
        }
        relationships {
          field_image {
            field_focal_point_area
            field_media_image {
              alt
            }
            relationships {
              field_media_image {
                localFile {
                  publicURL
                  extension
                  childImageSharp {
                    ATTENTION: fluid(
                      cropFocus: ATTENTION
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }
                    CENTER: fluid(
                      cropFocus: CENTER
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }                
                    NORTH: fluid(
                      cropFocus: NORTH
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }
                    SOUTH: fluid(
                      cropFocus: SOUTH
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }
                    EAST: fluid(
                      cropFocus: EAST
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }
                    WEST: fluid(
                      cropFocus: WEST
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }
                    NORTHEAST: fluid(
                      cropFocus: NORTHEAST
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }
                    NORTHWEST: fluid(
                      cropFocus: NORTHWEST
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }
                    SOUTHEAST: fluid(
                      cropFocus: SOUTHEAST
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }
                    SOUTHWEST: fluid(
                      cropFocus: SOUTHWEST
                      fit: COVER
                      maxHeight: 266
                      maxWidth: 400
                    ) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;