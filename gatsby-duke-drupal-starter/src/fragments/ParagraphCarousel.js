import { graphql } from "gatsby"

export const ParagraphCarousel = graphql`
  fragment ParagraphCarousel on paragraph__carousel {
    id
    relationships {
      field_paragraph_items {
        field_heading
        field_tagline
        field_url {
          title
          uri
        }
        relationships {
          field_image {
            field_media_image {
              alt
            }
            relationships {
              field_media_image {
                localFile {
                  publicURL
                  childImageSharp {
                    fixed(cropFocus: CENTER, height: 670, width: 900) {
                      ...GatsbyImageSharpFixed
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
