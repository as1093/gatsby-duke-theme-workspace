import { graphql } from "gatsby"

export const ParagraphIconRow = graphql`
  fragment ParagraphIconRow on paragraph__icon_row {
    id
    field_display_title
    relationships {
      field_paragraph_items {
        relationships {
          field_image {
            field_media_image {
              alt
            }
            relationships {
              field_media_image {
                localFile {
                  publicURL
                  childImageSharp {
                    fluid(maxWidth: 1025) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
        field_body {
          processed
        }
        field_heading
        field_link {
          title
          uri
        }
      }
    }
  }
`;