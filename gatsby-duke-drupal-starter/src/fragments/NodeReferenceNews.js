import { graphql } from "gatsby"

export const NodeNews = graphql`
  fragment NodeNews on node__news {
    id
    field_date(formatString: "MMMM D, YYYY")
    field_news_source
    field_title_override
    title
    path {
      alias
    }
    body {
      processed
      summary
    } 
    field_feed_image_url {
      uri
    }
    field_feed_image_alt_text
    relationships {
      field_author_reference {
        title
        field_last_name
        path {
          alias
        }
      } 
      field_featured_media {
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              publicURL
              childImageSharp {
                fluid(maxWidth: 900, maxHeight: 600) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`;