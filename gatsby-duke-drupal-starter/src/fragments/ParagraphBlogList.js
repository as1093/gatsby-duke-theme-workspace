import { graphql } from "gatsby"

export const ParagraphBlogList = graphql `
  fragment ParagraphBlogList on paragraph__blog_listing {
    id
    field_display_title
    field_link {
      title
      uri
    }
    relationships {
      field_category {
        relationships {
          node__blog_post {
            id
            title
          }
        }
      }
    }
  }
`