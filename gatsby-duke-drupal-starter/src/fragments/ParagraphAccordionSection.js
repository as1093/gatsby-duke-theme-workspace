import { graphql } from "gatsby"

export const ParagraphAccordionSection = graphql `
  fragment ParagraphAccordionSection on paragraph__accordion_section {
    id
    field_administrative_title
    field_display_title
    relationships {
      field_accordion_item {
        field_heading
        field_content {
          processed
        }
      }
    }
  }
`;
