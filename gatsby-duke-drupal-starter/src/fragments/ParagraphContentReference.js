import { graphql } from "gatsby"

export const ParagraphContentReference = graphql`
  fragment ParagraphContentReference on paragraph__content_reference {
    id
    field_display_title
    relationships {
      nodeReferences : field_content_item {
        type: __typename
        ...NodeEvent
        ...NodeProfile
        ...NodeNews
        ...NodeBlogPost
        ...NodeDocument
        ...NodeFAQ
        ...NodeStory
      }
      scholarsReferences : field_scholars_profile {
        duke_scholars_uri
        title
        duke_scholars_profile_email
        duke_scholars_profile_phone
        duke_scholars_profile_thumbnail {
          alt_text
          thumbnail
        }
        relationships {
          duke_scholars_profile_positions {
            position_preferred
            title
          }
        }
      }
    }
  }
`;