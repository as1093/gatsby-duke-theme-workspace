import { useStaticQuery, graphql } from "gatsby"

export const useSiteInfoData = () => {
  const { allBlockContentSiteInformation } = useStaticQuery(
    graphql`
      {
        allBlockContentSiteInformation {
          edges {
            node {
              field_site_name
              field_site_name_prefix
              field_site_additional_info
              field_site_address
              field_site_phone_number
              field_site_email
              field_site_facebook {
                uri
              }
              field_site_instagram {
                uri
              }
              field_site_linkedin {
                uri
              }
              field_site_twitter {
                uri
              }
              field_site_youtube {
                uri
              }
              field_site_primary_cta {
                uri
                title
              }
              field_site_secondary_cta {
                uri
                title
              }
              field_site_cta_description
              field_site_cta_heading
              relationships {
                field_site_homepage {
                  id
                }
              }
            }
          }
        }
      }
    `
  )
  return allBlockContentSiteInformation.edges
}
