import { useStaticQuery, graphql } from "gatsby"

export const useMenuData = () => {
  const { allMenuLinkContentMenuLinkContent } =  useStaticQuery(
    graphql`
      query SiteMenuQuery {
        allMenuLinkContentMenuLinkContent(sort: {order: ASC, fields: weight}) {
          edges {
            node {
              enabled
              title
              expanded
              external
              langcode
              weight
              link {
                uri
                uri_alias
              }
              drupal_parent_menu_item
              bundle
              drupal_id
              menu_name
            }
          }
        }
      }
    `
  )
  return allMenuLinkContentMenuLinkContent.edges;
}