import React from 'react';
import LayoutBase from '../components/Organisms/Layout/LayoutBase';
import { graphql } from 'gatsby';
export default ({ data, pageContext }) => (
<LayoutBase headerTitle='title'>
    <h3>{data.scholarsProfile.title}</h3>
    {JSON.stringify(data)}
</LayoutBase>
);
  

export const scholarsProfileQuery = graphql`
  query($ID: String!) {
    scholarsProfile: dukeScholarsProfileDukeScholarsProfile(id: { eq: $ID}) {
      title
      id
      drupal_internal__id
    }
  }`