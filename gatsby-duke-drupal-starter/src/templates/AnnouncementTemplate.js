import React from 'react';
import LayoutBase from '../components/Organisms/Layout/LayoutBase';
import CKEditorContent from '../components/Atoms/CKEditor/DrupalCKEditor';
import { graphql } from 'gatsby';

export const AnnouncementTemplate = ({ data }) => {
    const sorted = data.allNodeAnnouncement.nodes.sort(function(a, b) {
        var date1 = new Date(a.changed)
        var date2 = new Date(b.changed)
        return date2 - date1
    })
  
    return (
        <LayoutBase>
            <h3>{sorted[0].title}</h3>
            <CKEditorContent content={sorted[0].body.processed} />
            <p>{sorted[0].relationships.field_priority.name}</p>
        </LayoutBase>
    );
  };

export default AnnouncementTemplate

export const announcementQuery = graphql`
query {
    allNodeAnnouncement(filter: {status: {eq: true}}) {
      nodes {
        id
        title
        body {
          processed
        }
        path {
          alias
        }
        relationships {
          field_priority {
            name
          }
        }
        status
        changed
      }
    }
  }
`