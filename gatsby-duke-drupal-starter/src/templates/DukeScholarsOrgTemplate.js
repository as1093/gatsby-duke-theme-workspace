import React from 'react';
import LayoutBase from '../components/Organisms/Layout/LayoutBase';
export default ({ pageContext: { node }}) => (
<LayoutBase headerTitle='title'>
    <h3>Duke Scholars Org Template</h3>
    {JSON.stringify(node)}
</LayoutBase>
);
  