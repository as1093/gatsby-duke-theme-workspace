/**
 *  Renders Page content type template
 */
import { graphql } from "gatsby"
import React from "react"
import PropTypes from "prop-types"
import LayoutPage from "../components/Organisms/Layout/LayoutPage"
import { getParagraph } from "../components/Molecules/Paragraphs/ParagraphsHelper.js"

export const PageTemplate = ({ data }) => {

  const hero = (data.page.relationships.hero) ? getParagraph(data.page.relationships.hero) : null;
  const paragraphs = data.page.relationships.paragraphs?.map(getParagraph)

  // check if this page is in menu
  const pageInMenu = Boolean(data.menuLink);

  return (
    <LayoutPage
      hasSidebar={pageInMenu}
      title={data.page.title}
      hero={hero ? hero : null}
    >
      {paragraphs}
    </LayoutPage>
  )
}

PageTemplate.propTypes = {
  children: PropTypes.node,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default PageTemplate

export const pageQuery = graphql`
  query($ID: String!, $uri: String!) {
    page: nodePage(id: { eq: $ID }) {
      id
      title
      relationships {
        hero: field_hero {
          type: __typename
          ...ParagraphCarousel
          ...ParagraphHeroImage
          ...ParagraphHeroImageText
          ...ParagraphHeroVideoText
        }
        paragraphs: field_content_blocks {
          type: __typename
          ...ParagraphAccordionSection
          ...ParagraphBlogList
          ...ParagraphButton
          ...ParagraphCallout
          ...ParagraphContent
          ...ParagraphContentReference
          ...ParagraphEventList
          ...ParagraphFaqsList
          ...ParagraphIconRow
          ...ParagraphIframe
          ...ParagraphImageCardRow
          ...ParagraphImageText
          ...ParagraphMedia
          ...ParagraphNewsList
          ...ParagraphPullQuote
          ...ParagraphScholarsList
          ...ParagraphSlideshow
          ...ParagraphSocialMedia
          ...ParagraphSpecialText
          ...ParagraphStoryList
          ...ParagraphTwoColumn
          ...ParagraphThreeColumn
        }
      }
    }
    menuLink: menuLinkContentMenuLinkContent(link: {uri_alias: {eq: $uri}}) {
      menu_name
    }
  }
`
