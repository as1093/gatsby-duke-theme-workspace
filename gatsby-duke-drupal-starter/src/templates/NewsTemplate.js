import React from "react"
import LayoutNews from "../components/Organisms/Layout/LayoutNews"
import News from "../components/Templates/News/News"
import RelatedNews from "../components/Templates/News/RelatedNews.js"
import { getParagraph } from "../components/Molecules/Paragraphs/ParagraphsHelper.js"
import { graphql } from "gatsby"

export const NewsTemplate = ({ data }) => {

  let slideshow
  if (data.news.relationships.paragraphs !== null) {
    slideshow = getParagraph(data.news.relationships.paragraphs)
  }  

  return (
    <LayoutNews title={data.news.title}>
      <News
        key={data.news.id}
        title={
          data.news.field_title_override
            ? data.news.field_title_override
            : data.news.title
        }
        title_override={data.news.field_title_override}
        subtitle={data.news.field_subtitle}
        featured_image={data.news.relationships.field_featured_media}
        date={data.news.field_date}
        authors={data.news.relationships.field_author_reference}
        body={data.news.body}
        categories={data.news.relationships.field_categories}
        feed_image_url={data.news.field_feed_image_url}
        feed_image_alt={data.news.field_feed_image_alt_text}
        slideshow={slideshow}
      />
      <RelatedNews
        id={data.news.id}
        categories={data.news.relationships.field_categories}
      />
    </LayoutNews>
  )
}

export default NewsTemplate

export const newsQuery = graphql`
  query($ID: String!) {
    news: nodeNews(id: { eq: $ID }) {
      title
      id
      path {
        alias
      }
      body {
        summary
        processed
      }
      field_title_override
      field_subtitle
      field_date
      field_feed_image_url {
        uri
      }
      field_feed_image_alt_text
      relationships {
        field_author_reference {
          title
          field_last_name
          path {
            alias
          }
          field_bio {
            summary
          }
          relationships {
            field_image {
              field_media_image {
                alt
              }
              relationships {
                field_media_image {
                  localFile {
                    publicURL
                    childImageSharp {
                      fluid(maxWidth: 330) {
                        ...GatsbyImageSharpFluid
                      }
                    }
                  }
                }
              }
            }
          }
        }
        field_categories {
          name
          id
          relationships {
            node__news {
              id
              title
              field_date
              relationships {
                field_categories {
                  id
                  name
                }
              }
              path {
                alias
              }
              body {
                summary
                value
              }
            }
          }
        }
        field_featured_media {
          field_media_image {
            alt
          }
          relationships {
            field_media_image {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1025) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
        paragraphs: field_slideshow {
          type: __typename
          ...ParagraphSlideshow
        }
      }
    }
  }
`
