import React from 'react'
import LayoutEvent from "../components/Organisms/Layout/LayoutEvent"
import Event from '../components/Templates/Event/Event';
import { graphql } from 'gatsby';

export default ({ data }) => (
  <LayoutEvent title={data.event.title}>
    <Event
      title={data.event.field_title_override ? data.event.field_title_override : data.event.title}
      title_override={data.event.field_title_override}
      featured_image={data.event.relationships.field_featured_media}
      additional_media={data.event.relationships.field_additional_media}
      event_date={data.event.date}
      location_link={data.event.field_location_link}
      location_text={data.event.field_location_text}
      presenter={data.event.field_presenter_speaker}
      cost={data.event.field_cost}
      summary={data.event.body.summary}
      additional_links={data.event.field_additional_links}
      contact_name={data.event.field_contact_name}
      contact_email={data.event.field_contact_email}
      contact_phone={data.event.field_contact_phone}
      more_event_info={data.event.field_more_event_info}
      categories={data.event.relationships.field_categories}
      event_series={data.event.relationships.field_series}
      event_sponsors={data.event.relationships.field_sponsors}
      event_co_sponsors={data.event.relationships.field_co_sponsors}
      event_source_url={data.event.field_event_source_url && data.event.field_event_source_url.uri}
      body={data.event.body}
      webcast_url={data.event.field_webcast_url}
      feed_image_url={data.event.field_feed_image_url}
      feed_image_alt={data.event.field_feed_image_alt_text}
      event_status={data.event.relationships.field_status}
    />
  </LayoutEvent>
);

export const eventQuery = graphql`
query($ID: String!) {
  event: nodeEvent(id: {eq: $ID}) {
    title
    id
    path {
      alias
    }
    field_title_override
    field_alternate_link {
      uri
    }
    date: field_event_date {
      start: value
      end: end_value
    }
    field_location_text
    field_location_link {
      uri
    }
    field_additional_location_inform {
      processed
    }
    field_presenter_speaker
    field_cost
    body {
      summary
      processed
    }
    field_additional_links {
      uri
      title
    }
    field_contact_name 
    field_contact_email
    field_contact_phone
    field_more_event_info {
      uri
      title
    }
    field_webcast_url {
      uri
    }
    field_feed_image_url {
      uri
    }
    field_feed_image_alt_text
    relationships {
      field_featured_media {
        field_media_image {
          alt
        }
        relationships {
          field_media_image {
            localFile {
              childImageSharp {
                fluid(maxWidth: 1025) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
      field_categories {
        name
        id
        path {
          alias
        }
      }
      field_series {
        name
        id
        path {
          alias
        }
      }
      field_sponsors {
        name
        id
        path {
          alias
        }
      }
      field_co_sponsors {
        name
        id
        path {
          alias
        }
      }
      field_status {
        id
        name
      }
    }
    field_event_source_url {
      uri
    }
  }
}`
