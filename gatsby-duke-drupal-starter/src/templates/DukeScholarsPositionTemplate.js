import React from 'react';
import LayoutBase from '../components/Organisms/Layout/LayoutBase';
import { graphql } from 'gatsby';
export default ({ pageContext: { node }}) => (
<LayoutBase headerTitle='title'>
    <h3>duke scholars position Template</h3>
    {JSON.stringify(node)}
</LayoutBase>
);
