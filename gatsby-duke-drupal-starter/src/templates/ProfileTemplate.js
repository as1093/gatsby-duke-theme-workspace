import React from "react"
import LayoutProfile from "../components/Organisms/Layout/LayoutProfile"
import Profile from "../components/Templates/Profile/Profile"
import { graphql } from "gatsby"

export const ProfileTemplate = ({ data }) => {
  let relatedContent = []
  if (data.profile.relationships.node__blog_post) {
    relatedContent = relatedContent.concat(
      data.profile.relationships.node__blog_post
    )
  }
  if (data.profile.relationships.node__news) {
    relatedContent = relatedContent.concat(
      data.profile.relationships.node__news
    )
  }
  if (data.profile.relationships.node__story) {
    relatedContent = relatedContent.concat(
      data.profile.relationships.node__story
    )
  }

  return (
    <LayoutProfile title={data.profile.title}>
      <Profile
        title={data.profile.title}
        roles={data.profile.field_role && data.profile.field_role}
        bio={data.profile.field_bio?.processed}
        email_address={data.profile.field_email_address}
        phone_number={
          data.profile.field_phone_number && data.profile.field_phone_number
        }
        address_1={
          data.profile.field_address_line_1 && data.profile.field_address_line_1
        }
        address_2={
          data.profile.field_address_line_2 && data.profile.field_address_line_2
        }
        address_3={
          data.profile.field_address_line_3 && data.profile.field_address_line_3
        }
        website_url={data.profile.field_personal_website?.uri}
        photo={
          data.profile.relationships.field_image
            ? data.profile.relationships.field_image
            : null
        }
        related_content={relatedContent}
      />
    </LayoutProfile>
  )
}

export default ProfileTemplate

export const ProfileQuery = graphql`
  query($ID: String!) {
    profile: nodeProfile(id: { eq: $ID }) {
      title
      id
      path {
        alias
      }
      field_last_name
      field_address_line_1
      field_address_line_2
      field_address_line_3
      field_email_address
      field_phone_number
      field_role
      field_bio {
        processed
      }
      field_personal_website {
        uri
        title
      }
      relationships {
        field_image {
          field_media_image {
            alt
          }
          relationships {
            field_media_image {
              localFile {
                publicURL
                childImageSharp {
                  fixed(width: 300, height: 360) {
                    ...GatsbyImageSharpFixed
                  }
                }
              }
            }
          }
        }
        node__blog_post {
          type: __typename
          title
          field_date
          path {
            alias
          }
        }
        node__story {
          type: __typename
          title
          field_date
          path {
            alias
          }
        }
        node__news {
          type: __typename
          title
          field_date
          path {
            alias
          }
        }
      }
    }
  }
`
