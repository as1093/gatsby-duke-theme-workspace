import React from 'react';
import LayoutBase from '../components/Organisms/Layout/LayoutBase';
import { graphql } from 'gatsby';
export default ({ data, pageContext }) => (
<LayoutBase headerTitle='title'>
    <h3>{data.faq.title}</h3>
    {JSON.stringify(data)}
</LayoutBase>
);

export const faqQuery = graphql`
query($ID: String!) {
    faq: nodeFaq(id: {eq: $ID}) {
        title
        id
        path {
            alias
        }
        field_question
        field_answer {
            processed
        }
        relationships {
            field_categories {
                id
                name
                path {
                    alias
                }
            }
        }
    }
}`