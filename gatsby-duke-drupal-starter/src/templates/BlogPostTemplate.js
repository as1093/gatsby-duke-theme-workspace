import React from "react"
import { graphql } from "gatsby"
import LayoutBlogPost from "../components/Organisms/Layout/LayoutBlogPost"
import BlogPost from "../components/Templates/BlogPost/BlogPost"
import { getParagraph } from "../components/Molecules/Paragraphs/ParagraphsHelper.js"

export const BlogPostTemplate = ({ data }) => {

  let slideshow
  if (data.blogPost.relationships.paragraphs !== null) {
    slideshow = getParagraph(data.blogPost.relationships.paragraphs)
  }  

  return (
    <LayoutBlogPost
      title={data.blogPost.title}
    >
      <BlogPost
        title={data.blogPost.title}
        body={data.blogPost.body?.processed}
        date={data.blogPost.field_date && data.blogPost.field_date}
        featured_media={
          data.blogPost.relationships.field_featured_media
            ? data.blogPost.relationships.field_featured_media
            : null
        }
        categories={data.blogPost.relationships.field_categories}
        authors={data.blogPost.relationships.field_author_reference}
        slideshow={slideshow}
      />
    </LayoutBlogPost>
  )
}

export default BlogPostTemplate

// @TODO: field_media_image needs to be a fragment
// @TODO: field_categories needs to be a fragment

export const blogPostQuery = graphql`
  query($ID: String!) {
    blogPost: nodeBlogPost(id: { eq: $ID }) {
      id
      title
      field_date(formatString: "MMMM D, YYYY")
      body {
        processed
      }
      path {
        alias
      }
      relationships {
        field_categories {
          id
          name
          path {
            alias
          }
        }
        field_author_reference {
          title
          field_last_name
          path {
            alias
          }
          field_bio {
            summary
          }
          relationships {
            field_image {
              field_media_image {
                alt
              }
              relationships {
                field_media_image {
                  localFile {
                    publicURL
                    childImageSharp {
                      fluid(maxWidth: 330) {
                        ...GatsbyImageSharpFluid
                      }
                    }
                  }
                }
              }
            }
          }
        }
        field_featured_media {
          field_media_image {
            alt
          }
          relationships {
            field_media_image {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1025) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
        paragraphs: field_slideshow {
          type: __typename
          ...ParagraphSlideshow
        }
      }
    }
  }
`
