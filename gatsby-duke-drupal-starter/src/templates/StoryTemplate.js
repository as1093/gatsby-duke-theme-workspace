import React from "react"
import { graphql } from "gatsby"
import LayoutStory from "../components/Organisms/Layout/LayoutStory"
import Story from "../components/Templates/Story/Story"
import { getParagraph } from "../components/Molecules/Paragraphs/ParagraphsHelper.js"

export const StoryTemplate = ({ data }) => {
  // Authors for the header of the Story (names linked to profile or URL/email)
  // const authors = data.story.relationships.authors.map(getParagraph)

  // Extended Authors info for the footer of the Story (names linked to profile or URL/email, profile photo, short bio (summary))
  // const authorsFull = data.story.relationships.field_author_reference.map((author, index) => {
  //   return author.type === "paragraph__existing_profile" ? (
  //     <Author node={author} key={index} />
  // })

  const paragraphs = data.story.relationships.paragraphs.map(getParagraph)

  return (
    <LayoutStory
      title={data.story.title}
    >
      <Story
        title={data.story.title}
        subtitle={data.story.field_subtitle}
        summary={data.story.field_summary}
        date={data.story.field_date}
        featured_media={
          data.story.relationships.field_featured_media
            ? data.story.relationships.field_featured_media
            : null
        }
        categories={data.story.relationships.field_categories}
        authors={data.story.relationships.field_author_reference}
        paragraphs={paragraphs}
      />
    </LayoutStory>
  )
}

export default StoryTemplate

// @TODO: field_media_image needs to be a fragment
// @TODO: field_categories needs to be a fragment

export const pageQuery = graphql`
  query($ID: String!) {
    story: nodeStory(id: { eq: $ID }) {
      id
      title
      field_subtitle
      field_date(formatString: "MMMM D, YYYY")
      field_summary
      relationships {
        field_categories {
          id
          name
          path {
            alias
          }
        }
        field_author_reference {
          title
          field_last_name
          path {
            alias
          }
          field_bio {
            summary
            processed
          }
          relationships {
            field_image {
              field_media_image {
                alt
              }
              relationships {
                field_media_image {
                  localFile {
                    publicURL
                    childImageSharp {
                      fluid(maxWidth: 330) {
                        ...GatsbyImageSharpFluid
                      }
                    }
                  }
                }
              }
            }
          }
        }
        field_featured_media {
          field_media_image {
            alt
          }
          relationships {
            field_media_image {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1025) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
        paragraphs: field_content_blocks {
          type: __typename
          ...ParagraphAccordionSection
          ...ParagraphButton
          ...ParagraphCallout
          ...ParagraphContent
          ...ParagraphIconRow
          ...ParagraphIframe
          ...ParagraphImageCardRow
          ...ParagraphImageText
          ...ParagraphMedia
          ...ParagraphPullQuote
          ...ParagraphSlideshow
          ...ParagraphSocialMedia
          ...ParagraphSpecialText
        }
      }
    }
  }
`
