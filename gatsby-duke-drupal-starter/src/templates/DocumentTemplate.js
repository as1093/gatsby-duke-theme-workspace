import React from "react"
import LayoutBase from "../components/Organisms/Layout/LayoutBase"
import DocumentTeaser from "../components/Templates/Document/DocumentTeaser"
import { graphql } from "gatsby"

export const DocumentTemplate = ({ data }) => {
  return (
    <LayoutBase headerTitle={data.document.title}>
      <DocumentTeaser
        title={data.document.title}
        description={data.document.body ? data.document.body.processed : ""}
        categories={
          data.document.relationships.field_categories
            ? data.document.relationships.field_categories
            : []
        }
        file={
          data.document.relationships.field_document
            ? data.document.relationships.field_document
            : null
        }
        date={
          data.document.field_last_modified_date
            ? data.document.field_last_modified_date
            : ""
        }
      />
    </LayoutBase>
  )
}

export default DocumentTemplate

export const DocumentQuery = graphql`
  query($ID: String!) {
    document: nodeDocument(id: { eq: $ID }) {
      title
      id
      path {
        alias
      }
      body {
        processed
      }
      field_last_modified_date(formatString: "M/D/YY")
      relationships {
        field_categories {
          name
        }
        field_document {
          ...ParagraphDocumentUpload
          ...ParagraphURL
        }
      }
    }
  }
`
