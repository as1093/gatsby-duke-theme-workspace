require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    title: `BB8 gatsby starter`,
    description: `Base Drupal-wired Gatsby theme`,
    author: `Duke Web Services`,
    siteUrl: process.env.SITE_URL,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: "gatsby-plugin-sass",
      options: {
        data: `@import "${__dirname}/src/config.scss";`,
        useResolveUrlLoader: {
          options: {
            sourceMap: true,
          },
        },
      },
    },
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Open+Sans`,
            variants: [`300`, `400`, `600`, `700`],
          },
          {
            family: `Merriweather`,
            variants: [`300`, `600`],
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: "gatsby-plugin-page-creator",
      options: { path: `${__dirname}/src/pages` },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-drupal`,
      options: {
        baseUrl: process.env.DRUPAL_BASE_URL,
        apiBase: process.env.API_BASE || `jsonapi`, // optional, defaults to `jsonapi`
        concurrentFileRequests: 1,
        preview: true,
        secret: process.env.DRUPAL_SECRET_KEY,
        // basicAuth: {
        //   username: process.env.BASIC_AUTH_USERNAME,
        //   password: process.env.BASIC_AUTH_PASSWORD
        // },
        fastBuilds: false,
        disallowedLinkTypes: [
          `self`,
          `describedby`,
          `action--action`,
          `editor--editor`,
          `block--block`,
          `entity_browser--entity_browser`,
          `entity_embed_fake_entity--entity_embed_fake_entity`,
          `entity_form_display--entity_form_display`,
          `entity_form_mode--entity_form_mode`,
          `entity_view_display--entity_view_display`,
          `entity_view_mode--entity_view_mode`,
          `features_bundle--features_bundle`,
          `field_config--field_config`,
          `field_storage_config--field_storage_config`,
          `filter_format--filter_format`,
          `jsonapi_resource_config--jsonapi_resource_config`,
          `linkit_profile--linkit_profile`,
          `pathauto_pattern--pathauto_pattern`,
          `user_role--user_role`,
        ],
      },
    },
    {
      resolve: `gatsby-plugin-schema-snapshot`,
      options: {
        path: `schema.gql`,
        exclude: {
          plugins: [],
        },
        update: false,
      },
    },
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
              }
            }
          }
        `,
        feeds: [
          {
            serialize: ({ query: { site, allNodeNews } }) => {
              var $description = ""
              var $url = ""

              return allNodeNews.edges.map(edge => {
                $url =
                  edge.node.field_alternate_link &&
                  edge.node.field_alternate_link.uri
                    ? edge.node.field_alternate_link.uri
                    : site.siteMetadata.siteUrl + edge.node.path.alias

                if (edge.node.body)
                  $description = edge.node.body.summary
                    ? edge.node.body.summary
                    : edge.node.body.value.substring(0, 300)

                return Object.assign(
                  {},
                  {
                    title: edge.node.title,
                    description: $description,
                    date: edge.node.field_date,
                    author:
                      edge.node.relationships.field_author_reference ? edge.node.relationships.field_author_reference.title +
                      edge.node.relationships.field_author_reference
                        .field_last_name : '',
                    url: $url,
                    guid: edge.node.drupal_id,
                  }
                )
              })
            },
            query: `
              {
                allNodeNews(
                  sort: { order: DESC, fields: [field_date] },
                ) {
                  edges {
                    node {
                      title
                      field_date
                      drupal_id
                      path {
                        alias
                      }
                      field_alternate_link {
                        uri
                      }
                      body {
                        summary
                        value
                      }
                      relationships {
                        field_author_reference {
                          title
                          field_last_name
                        }
                      }
                    }
                  }
                }
              }
            `,
            output: "/news/rss.xml",
            title: "News RSS Feed",
          },
        ],
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
