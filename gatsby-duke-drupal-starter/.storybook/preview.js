import { action } from "@storybook/addon-actions"
import { addDecorator, addParameters } from "@storybook/react";
import { withA11y } from "@storybook/addon-a11y";
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';

import "../src/custom.scss"

// system wide decorator that all of the stories will use
addParameters({
  options: { panelPosition: 'bottom' },
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
});


// Gatsby's Link overrides:
// Gatsby Link calls the `enqueue` & `hovering` methods on the global variable ___loader.
// This global object isn't set in storybook context, requiring you to override it to empty functions (no-op),
// so Gatsby Link doesn't throw any errors.
global.___loader = {
  enqueue: () => {},
  hovering: () => {},
}

// __PATH_PREFIX__ is used inside gatsby-link an other various places. For storybook not to crash, you need to set it as well.
global.__PATH_PREFIX__ = "";
global.__BASE_PATH__ = "";

// Navigating through a gatsby app using gatsby-link or any other gatsby component will use the `___navigate` method.
// In Storybook it makes more sense to log an action than doing an actual navigate. Checkout the actions addon docs for more info: https://github.com/storybookjs/storybook/tree/master/addons/actions.

if (typeof window !== `undefined`) {
  window.___navigate = pathname => {
    action("NavigateTo:")(pathname)
  };
}

// system wide decorator to allow the addon to be used
addDecorator(withA11y);
