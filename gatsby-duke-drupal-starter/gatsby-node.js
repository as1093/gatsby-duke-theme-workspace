
exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  function snakeToPascal(str) {
    str += '';
    str = str.split('_');
    for (var i = 0; i < str.length; i++) {
      str[i] =
        str[i].slice(0, 1).toUpperCase() + str[i].slice(1, str[i].length);
    }
    return str.join('');
  }

  const path = require("path")

  const nodeTypes = await graphql(`
    {
      allNodeTypeNodeType {
        nodes {
          name
          drupal_internal__type
        }
      }
    }
  `);
  
  await Promise.all(

    // we need to make sure the allNodeTypeNodeType object exists

    nodeTypes.data.allNodeTypeNodeType.nodes.map(async node => {

      const nodeName = snakeToPascal(node.drupal_internal__type);

      const query = `{
        allNode${nodeName}(filter: {status: {eq: true}}) {
          nodes {
            id
            path {
              alias
            }
          }
        }
      }`

      const queryScholars = `{
        allDukeScholarsProfileDukeScholarsProfile(filter: {status: {eq: true}}) {
          nodes {
            id
            drupal_internal__id
          }
        }
      }`

      const nodes = await graphql(query)
      const nodesScholars = await graphql(queryScholars)

      nodes.data[`allNode${nodeName}`].nodes.forEach(node => 
        createPage({
          path: node.path.alias,
          component: path.resolve(`./src/templates/${nodeName}Template.js`),
          context: {
            ID: node.id,
            uri: node.path.alias
          }
        })  
      )

      if (nodesScholars.data !== undefined) {
        nodesScholars.data.allDukeScholarsProfileDukeScholarsProfile.nodes.forEach(node => 
          createPage({
            path: `/duke-scholars-profile/${node.drupal_internal__id}`,
            component: path.resolve(`./src/templates/ScholarsTemplate.js`),
            context: {
              ID: node.id,
              uri: `/duke-scholars-profile/${node.drupal_internal__id}`
            }
          })  
        ) 
      }
           
    }
))};
