import React from 'react';
import Header from '../../src/components/Organisms/Header/Header';
import mainmenu from "../2-molecules/2-MainMenu.stories"

/**
 * Storybook Definition.
 */
export default { title: 'Organisms' };

export const header = () => (
  <Header siteTitle="Sanford School of Public Policy"  />
);