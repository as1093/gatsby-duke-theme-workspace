import React from 'react';
import PageTitle from '../../src/components/Atoms/PageTitle/PageTitle';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms' };

export const pageTitle = () => <PageTitle>This is the page title</PageTitle>;