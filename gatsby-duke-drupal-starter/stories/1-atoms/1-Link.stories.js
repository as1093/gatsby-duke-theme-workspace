import React from 'react';
import Link from '../../src/components/Atoms/Link/Link';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms' };

export const link = () => (
  <>
    <Link
      url="https://gitlab.oit.duke.edu/dws/drupal/gatsby/gatsby-drupal-starter"
      target="_blank"
      title="This is the link one"
    >This link will open in a new window.</Link>
    
    <br />

    <Link
      url="https://gitlab.oit.duke.edu/dws/drupal/gatsby/gatsby-drupal-starter"
      title="This is the link one"
    >This link will open in the same window.</Link>
  </>
);