import React from 'react'
import Heading from '../../src/components/Atoms/Heading/Heading'
import CKEditorContent from "../../src/components/Atoms/CKEditor/DrupalCKEditor"

/**
 * Storybook Definition.
 */
export default { title: 'Atoms' };

const Headings = () => (
  <>
    {[1, 2, 3, 4, 5, 6].map((level) => (
      <Heading level={level}>This is a Header {level} example</Heading>
    ))}
  </>
);

export const text = () => <CKEditorContent content="Here's a test paragraph." />;
export const headings = () => <Headings />;