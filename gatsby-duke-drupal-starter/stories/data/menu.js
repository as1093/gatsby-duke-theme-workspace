const items = [
  {
    node: {
      enabled: true,
      title: "Home",
      expanded: false,
      external: false,
      langcode: "en",
      weight: -50,
      link: { uri: "internal:/", uri_alias: null },
      drupal_parent_menu_item: null,
      bundle: "menu_link_content",
      drupal_id: "01df17d9-0fac-45e7-b00a-9952637c4646",
      menu_name: "main",
      children: [],
    },
  },
  {
    node: {
      enabled: true,
      title: "Academics",
      expanded: false,
      external: false,
      langcode: "en",
      weight: -49,
      link: { uri: "entity:node/17", uri_alias: "/admissions" },
      drupal_parent_menu_item: null,
      bundle: "menu_link_content",
      drupal_id: "a6ca7639-95a4-46e1-88f9-02a46ea000f4",
      menu_name: "main",
      children: [
        {
          enabled: true,
          title: "Page from Academics",
          expanded: false,
          external: false,
          langcode: "en",
          weight: 0,
          link: {
            uri: "entity:node/20",
            uri_alias: "/admissions/page-admissions",
          },
          drupal_parent_menu_item: "a6ca7639-95a4-46e1-88f9-02a46ea000f4",
          bundle: "menu_link_content",
          drupal_id: "e9c8152b-cd23-4143-ae6d-34b75a66f745",
          menu_name: "main",
          children: [],
        },
      ],
    },
  },
  {
    node: {
      enabled: true,
      title: "Admissions",
      expanded: false,
      external: false,
      langcode: "en",
      weight: -48,
      link: {
        uri: "entity:node/9",
        uri_alias: "/master-public-policy-page-title",
      },
      drupal_parent_menu_item: null,
      bundle: "menu_link_content",
      drupal_id: "3b0c4ef9-cc20-45fb-bb04-7f6c987a559e",
      menu_name: "main",
      children: [
        {
          enabled: true,
          title: "test page 2",
          expanded: false,
          external: false,
          langcode: "en",
          weight: -31,
          link: {
            uri: "entity:node/10",
            uri_alias: "/test-page-1/test-page-2",
          },
          drupal_parent_menu_item: "3b0c4ef9-cc20-45fb-bb04-7f6c987a559e",
          bundle: "menu_link_content",
          drupal_id: "014f622d-c771-4128-ba91-19a68d70b16a",
          menu_name: "main",
          children: [
            {
              enabled: true,
              title: "Level 3 page",
              expanded: false,
              external: false,
              langcode: "en",
              weight: 0,
              link: {
                uri: "entity:node/18",
                uri_alias: "/test-page-1/test-page-2/level-3-page",
              },
              drupal_parent_menu_item: "014f622d-c771-4128-ba91-19a68d70b16a",
              bundle: "menu_link_content",
              drupal_id: "e9754982-511a-48c3-ba3a-225de42e87f8",
              menu_name: "main",
              children: [],
            },
            {
              enabled: true,
              title: "Another level 3 page",
              expanded: false,
              external: false,
              langcode: "en",
              weight: 0,
              link: {
                uri: "entity:node/19",
                uri_alias: "/test-page-1/test-page-2/another-level-3-page",
              },
              drupal_parent_menu_item: "014f622d-c771-4128-ba91-19a68d70b16a",
              bundle: "menu_link_content",
              drupal_id: "d2d40a3a-b28b-4e7f-bfbe-0826afcd9e4f",
              menu_name: "main",
              children: [],
            },
          ],
        },
      ],
    },
  },
  {
    node: {
      enabled: true,
      title: "Faculty & Research",
      expanded: false,
      external: false,
      langcode: "en",
      weight: -31,
      link: { uri: "entity:node/10", uri_alias: "/test-page-1/test-page-2" },
      drupal_parent_menu_item: "3b0c4ef9-cc20-45fb-bb04-7f6c987a559e",
      bundle: "menu_link_content",
      drupal_id: "014f622d-c771-4128-ba91-19a68d70b16a",
      menu_name: "main",
      children: [
        {
          enabled: true,
          title: "Level 3 page",
          expanded: false,
          external: false,
          langcode: "en",
          weight: 0,
          link: {
            uri: "entity:node/18",
            uri_alias: "/test-page-1/test-page-2/level-3-page",
          },
          drupal_parent_menu_item: "014f622d-c771-4128-ba91-19a68d70b16a",
          bundle: "menu_link_content",
          drupal_id: "e9754982-511a-48c3-ba3a-225de42e87f8",
          menu_name: "main",
          children: [],
        },
        {
          enabled: true,
          title: "Another level 3 page",
          expanded: false,
          external: false,
          langcode: "en",
          weight: 0,
          link: {
            uri: "entity:node/19",
            uri_alias: "/test-page-1/test-page-2/another-level-3-page",
          },
          drupal_parent_menu_item: "014f622d-c771-4128-ba91-19a68d70b16a",
          bundle: "menu_link_content",
          drupal_id: "d2d40a3a-b28b-4e7f-bfbe-0826afcd9e4f",
          menu_name: "main",
          children: [],
        },
      ],
    },
  },
  {
    node: {
      enabled: true,
      title: "Level 3 page",
      expanded: false,
      external: false,
      langcode: "en",
      weight: 0,
      link: {
        uri: "entity:node/18",
        uri_alias: "/test-page-1/test-page-2/level-3-page",
      },
      drupal_parent_menu_item: "014f622d-c771-4128-ba91-19a68d70b16a",
      bundle: "menu_link_content",
      drupal_id: "e9754982-511a-48c3-ba3a-225de42e87f8",
      menu_name: "main",
      children: [],
    },
  },
  {
    node: {
      enabled: true,
      title: "Another level 3 page",
      expanded: false,
      external: false,
      langcode: "en",
      weight: 0,
      link: {
        uri: "entity:node/19",
        uri_alias: "/test-page-1/test-page-2/another-level-3-page",
      },
      drupal_parent_menu_item: "014f622d-c771-4128-ba91-19a68d70b16a",
      bundle: "menu_link_content",
      drupal_id: "d2d40a3a-b28b-4e7f-bfbe-0826afcd9e4f",
      menu_name: "main",
      children: [],
    },
  },
  {
    node: {
      enabled: true,
      title: "Page from Admissions",
      expanded: false,
      external: false,
      langcode: "en",
      weight: 0,
      link: { uri: "entity:node/20", uri_alias: "/admissions/page-admissions" },
      drupal_parent_menu_item: "a6ca7639-95a4-46e1-88f9-02a46ea000f4",
      bundle: "menu_link_content",
      drupal_id: "e9c8152b-cd23-4143-ae6d-34b75a66f745",
      menu_name: "main",
      children: [],
    },
  },
  {
    node: {
      enabled: true,
      title: "Footer link 1",
      expanded: false,
      external: false,
      langcode: "en",
      weight: 0,
      link: { uri: "entity:node/10", uri_alias: "/test-page-1/test-page-2" },
      drupal_parent_menu_item: null,
      bundle: "menu_link_content",
      drupal_id: "ff7ea851-0982-43f7-af5d-540c957feecf",
      menu_name: "footer",
    },
  },
  {
    node: {
      enabled: true,
      title: "News & Events",
      expanded: false,
      external: false,
      langcode: "en",
      weight: 1,
      link: { uri: "entity:node/7", uri_alias: "/news/test-news" },
      drupal_parent_menu_item: null,
      bundle: "menu_link_content",
      drupal_id: "8fb74731-4e7e-4f63-8217-1332ba23424c",
      menu_name: "utility",
      children: [],
    },
  },
  {
    node: {
      enabled: true,
      title: "Admissions",
      expanded: false,
      external: false,
      langcode: "en",
      weight: 2,
      link: { uri: "entity:node/17", uri_alias: "/admissions" },
      drupal_parent_menu_item: null,
      bundle: "menu_link_content",
      drupal_id: "c7153515-2e04-4ee6-bc93-85941b99db23",
      menu_name: "footer",
    },
  },
  {
    node: {
      enabled: true,
      title: "Apply",
      expanded: false,
      external: false,
      langcode: "en",
      weight: 2,
      link: { uri: "entity:node/10", uri_alias: "/test-page-1/test-page-2" },
      drupal_parent_menu_item: null,
      bundle: "menu_link_content",
      drupal_id: "b9ca7980-74ea-4f9f-a975-b32d6f815ca4",
      menu_name: "utility",
      children: [],
    },
  },
  {
    node: {
      enabled: true,
      title: "Directory",
      expanded: false,
      external: false,
      langcode: "en",
      weight: 3,
      link: { uri: "entity:node/20", uri_alias: "/admissions/page-admissions" },
      drupal_parent_menu_item: null,
      bundle: "menu_link_content",
      drupal_id: "85186f93-2f83-4cb8-a58d-64403d7e41d3",
      menu_name: "utility",
      children: [],
    },
  },
]

export default items