import React from 'react';
import CustomBreadcrumbs from '../../src/components/Molecules/Breadcrumbs/Breadcrumbs';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules' };

let items = 
  [{"pathname":"/","crumbLabel":"Home"},
  {"pathname":"/first-page/","crumbLabel":"first-page"}]

export const breadcrumbs = () => <CustomBreadcrumbs trail={items} pageTitle="Title" />;
