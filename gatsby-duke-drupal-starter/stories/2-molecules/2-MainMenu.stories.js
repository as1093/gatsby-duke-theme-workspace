import React from "react"
import Menu from "../../src/components/Molecules/Menu/MenuWalker"
import items from "../data/menu"

export default { title: "Molecules" }

export const mainmenu = () => (
  <Menu storyItems={items} menuName={`main`} startLevel={1} /> 
)
